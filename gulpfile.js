
var _           = require('lodash');
var gulp        = require('gulp');
var prefixer    = require('gulp-autoprefixer');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var through2    = require('through2')
var rename      = require('gulp-rename');
var uglify      = require('gulp-uglify');
var gutil       = require('gulp-util');
var plumber     = require('gulp-plumber');
var postcss     = require('gulp-postcss');
var scssLint    = require('gulp-scss-lint');
var watch       = require('gulp-watch');
var mergeRules  = require('postcss-merge-rules');
var mqPacker    = require('css-mqpacker');
var runSequence = require('gulp4-run-sequence');
var fs          = require('fs');
var config      = JSON.parse(fs.readFileSync('./config/config.json'));
var args        = require('args');
var browser     = require('browser-sync');

args.option('dev', 'Enable dev mode. CSS and JavaScript will not be compressed.');

//change PROXY based on local env. 
const PORT = 8000;
const PROXY = 'http://web-comsandbox.local/'

const flags = args.parse(process.argv);

function errorHandler(error) {

    gutil.log(gutil.colors.red(error.message));
    this.emit('end');

}

function lintReporter(file) {

    if (! file.scsslint.success) {

        gutil.log(file.scsslint.issues.length + ' issues found in ' + file.path);

    }
    
}

function parseConfig() {

    _.each(config.themes, function(theme) {

        theme.sass    = parseThemeSlug(theme, theme.sass);
        theme.scripts = parseThemeScripts(theme);

    });

}

function parseThemeScripts(theme) {

  return _.reduce(theme.scripts, function(object, item) {
    
    var name = _.isString(item) ? 'scripts.js' : item.name;

    object[name] = object[name] || [];
    
    if(_.isString(item)) {

        object[name].push(parseThemeSlug(theme, item));

    } else {

        object[name] = _.map(item.source, function(script) { 
            return parseThemeSlug(theme, script);
        });

    }
    
    return object;
    
  }, {});

}

function parseThemeSlug(theme, value) {

    return value.split('{{slug}}').join(theme.slug);

}

gulp.task('compileSass', function() {

    var processors = [
        mergeRules,
        mqPacker
    ];

    var outputStyle = flags.dev ? 'expanded' : 'compressed';
    var dest, stream;

    _.each(config.themes, function(theme) {

        dest   = './public/wp-content/themes/' + theme.slug;
        stream = gulp
            .src('./source/uhs-theme-1/sass/app.scss')
            .pipe(plumber(errorHandler))
            .pipe(sass({
                    includePaths:['./source/'],
                    omitSourceMapUrl:true, 
                    outputStyle:outputStyle
                }))
            .pipe(prefixer({browsers:['last 3 versions', 'ie >= 9']}))
            .pipe(postcss(processors))
            .pipe(rename('style.css'))
            .pipe( through2.obj( function( file, enc, cb ) {
                var date = new Date();
                file.stat.atime = date;
                file.stat.mtime = date;
                cb( null, file );
            }) )
            .pipe(gulp.dest(dest));

    });

    return stream;

});

gulp.task('compileScripts', function() {

    var dest, stream;

    _.each(config.themes, function(theme) {

        dest = './public/wp-content/themes/' + theme.slug + '/assets/js';

        _.each(theme.scripts, function(source, name) {

            if(source.length) {

                if(flags.dev) {

                    stream = gulp.src(source)
                        .pipe(plumber(errorHandler))
                        .pipe(concat(name))
                        .pipe(gulp.dest(dest));

                } else {

                    stream = gulp.src(source)
                        .pipe(plumber(errorHandler))
                        .pipe(concat(name))
                        .pipe(uglify())
                        .pipe(gulp.dest(dest));

                }

            }

        });

    });

    return stream;

});

// Browsersync
function server(done) {
    browser.init({ port: PORT, proxy: PROXY })
    done()
  }
  
function reload(done) {
    browser.reload()
    done()
}

gulp.task('watchSass', function() {

    gulp.watch('./source/**/*.scss', function(callback) {

        runSequence('compileSass', reload, callback);

    });

});

gulp.task('watchScripts', function() {

    gulp.watch('./source/**/*.js', function(callback) {

        runSequence('compileScripts', reload, callback);

    });

});

gulp.task('default', function() {

    parseConfig();

    runSequence(['watchSass', 'watchScripts', server]);

});
