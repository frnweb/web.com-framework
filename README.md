# uhsinc.com
## Compile Instructions 

1. Install Node.js and gulp https://gulpjs.com/docs/en/getting-started/quick-start
2. Run `npm install`
3. Run `gulp` to compile. Any time a SCSS/JS file is saved it will compile automatically.
4. For BrowserSync to work. Make sure the `PROXY` variable in the gulpfile is set up to match the local URL instance. 

To use the theme both uhs-theme-1 and uhs-modular-framework parent theme are required, found in the repo directory. 


