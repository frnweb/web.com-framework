<?php if (! defined('ABSPATH')) die('No direct access allowed'); 
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */
?>
			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-2">
							<div class="social-icons">
								<?php if( ! empty( get_theme_mod('sc_facebook_url') ) ): ?>
									<a href="<?php echo esc_url( get_theme_mod('sc_facebook_url') ); ?>" title="Facebook" target="_blank">
										Facebook
										<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 43.7 43.7" version="1.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 43.7 43.7">
											<circle r="21" cy="21.8" cx="21.8" stroke-miterlimit="10" stroke-width="1.7" fill="none"/>
											<path id="Facebook_2_" d="m18.5 14.2v4h-3.6v4.1h3.6v12.9h4.4v-12.8h4.5s0.3-1.7 0.5-4.1h-5s0.1-3.9 0.1-4.4c0-0.5 0.3-1.2 0.9-1.2h4v-4c-1.8-0.1-3.2-0.3-3.9-0.3-5.7 0-5.5 5.1-5.5 5.8z" />
										</svg>
									</a>
								<?php endif; ?>
								<?php if( ! empty( get_theme_mod('sc_twitter_url') ) ): ?>
									<a href="<?php echo esc_url( get_theme_mod('sc_twitter_url') ); ?>" title="Twitter" target="_blank">
										Twitter
										<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 43.7 43.7" version="1.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 43.7 43.7">
											<circle r="21" cy="21.8" cx="21.8" stroke-miterlimit="10" stroke-width="1.7" fill="none"/>
											<path d="m33.4 15.4c-0.8 0.4-1.7 0.6-2.7 0.7 1-0.6 1.7-1.5 2-2.6-0.9 0.5-1.9 0.9-3 1.1-0.8-0.9-2.1-1.5-3.4-1.5-2.6 0-4.7 2.1-4.7 4.7 0 0.4 0 0.7 0.1 1.1-3.9-0.2-7.3-2-9.6-4.9-0.4 0.7-0.6 1.5-0.6 2.3 0 1.6 0.8 3 2.1 3.9-0.8 0-1.5-0.2-2.1-0.6v0.1c0 2.3 1.6 4.1 3.7 4.6-0.4 0.1-0.8 0.2-1.2 0.2-0.3 0-0.6 0-0.9-0.1 0.6 1.8 2.3 3.2 4.3 3.2-1.6 1.2-3.6 2-5.8 2-0.4 0-0.7 0-1.1-0.1 2.1 1.3 4.5 2.1 7.1 2.1 8.6 0 13.2-7.1 13.2-13.2v-0.6c1.2-0.7 2-1.5 2.6-2.4z" />
										</svg>
									</a>
								<?php endif; ?>
								<?php if( ! empty( get_theme_mod('sc_googleplus_url') ) ): ?>
									<a href="<?php echo esc_url( get_theme_mod('sc_googleplus_url') ); ?>" title="Google+" target="_blank">
										Google+
										<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 43.7 43.7" version="1.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 43.7 43.7">
											<circle r="21" cy="21.8" cx="21.8" stroke-miterlimit="10" stroke-width="1.7" fill="none"/>
											<g>
												<polygon points="29.9 20.7 29.9 18.2 28.9 18.2 28.9 20.7 26.4 20.7 26.4 21.8 28.9 21.8 28.9 24.3 29.9 24.3 29.9 21.8 32.4 21.8 32.4 20.7"/>
												<path d="m25.1 25c-0.2-0.3-0.5-0.6-0.7-0.8-0.3-0.3-0.6-0.5-0.9-0.8l-1-0.8-0.5-0.5c-0.2-0.2-0.2-0.4-0.2-0.7s0.1-0.6 0.2-0.8c0.2-0.2 0.3-0.4 0.5-0.6 0.3-0.2 0.6-0.5 0.9-0.7s0.5-0.5 0.7-0.8 0.4-0.7 0.5-1c0.1-0.4 0.2-0.8 0.2-1.4 0-0.5-0.1-1-0.2-1.4s-0.3-0.8-0.5-1.1-0.4-0.6-0.6-0.8-0.4-0.4-0.6-0.5h1.7l1.8-1h-5.7c-0.8 0-1.6 0.1-2.5 0.3s-1.7 0.6-2.5 1.2c-0.6 0.5-1 1.1-1.3 1.8s-0.4 1.3-0.4 2c0 0.5 0.1 1.1 0.3 1.6s0.5 1 0.9 1.4 0.9 0.7 1.4 1c0.6 0.3 1.2 0.4 2 0.4h0.4 0.4c-0.1 0.2-0.1 0.3-0.2 0.5s-0.1 0.4-0.1 0.6c0 0.4 0.1 0.8 0.3 1.1s0.4 0.6 0.6 0.8c-0.3 0-0.7 0-1.1 0.1-0.4 0-0.9 0.1-1.4 0.2s-1 0.2-1.5 0.4-0.9 0.4-1.4 0.6c-0.8 0.5-1.4 1-1.7 1.7-0.3 0.6-0.5 1.2-0.5 1.7s0.1 1 0.4 1.5c0.2 0.5 0.6 0.9 1.1 1.3s1.1 0.7 1.9 0.9c0.7 0.2 1.6 0.3 2.6 0.3 1.2 0 2.2-0.2 3.1-0.5s1.7-0.7 2.3-1.2 1.1-1 1.4-1.7c0.3-0.6 0.5-1.3 0.5-1.9 0-0.5-0.1-0.9-0.2-1.3 0-0.5-0.2-0.8-0.4-1.1zm-5.3-4.8c-0.6 0-1.1-0.2-1.5-0.5s-0.8-0.8-1.1-1.3-0.5-1-0.6-1.6-0.2-1.1-0.2-1.6c0-0.4 0-0.7 0.1-1.1s0.2-0.7 0.5-1c0.2-0.3 0.5-0.5 0.9-0.7s0.7-0.2 1.1-0.2c0.6 0 1.1 0.2 1.5 0.5s0.8 0.8 1.1 1.3 0.5 1.1 0.7 1.7c0.1 0.6 0.2 1.1 0.2 1.6 0 0.3 0 0.6-0.1 1s-0.3 0.7-0.6 1.1c-0.2 0.2-0.5 0.4-0.8 0.6-0.5 0.1-0.8 0.2-1.2 0.2zm3.1 10.3c-0.7 0.6-1.7 0.8-3 0.8-1.5 0-2.7-0.3-3.6-0.9s-1.3-1.4-1.3-2.4c0-0.5 0.1-0.9 0.3-1.2s0.4-0.6 0.7-0.8 0.5-0.4 0.8-0.5 0.5-0.2 0.6-0.3c0.3-0.1 0.6-0.2 0.9-0.2 0.3-0.1 0.6-0.1 0.9-0.1s0.5 0 0.7-0.1h0.4 0.3 0.3c0.5 0.4 1 0.7 1.4 1s0.7 0.6 0.9 0.9 0.4 0.6 0.5 0.8c0.1 0.3 0.2 0.6 0.2 1 0.1 0.7-0.3 1.5-1 2z"/>
											</g>
										</svg>
									</a>
								<?php endif; ?>
								<?php if( ! empty( get_theme_mod('sc_linkedin_url') ) ): ?>
									<a href="<?php echo esc_url( get_theme_mod('sc_linkedin_url') ); ?>" title="LinkedIn" target="_blank">
										LinkedIn
										<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 43.7 43.7" version="1.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 43.7 43.7">
											<circle r="21" cy="21.8" stroke="#0A81B4" cx="21.8" stroke-miterlimit="10" stroke-width="1.7" fill="none"/>
											<path d="m16.6 29.7h-3.7v-11.7h3.7v11.7zm-1.9-13.7c-1.4 0-2.2-0.9-2.2-2 0-1.2 0.9-2 2.2-2s2.2 0.9 2.2 2-0.8 2-2.2 2zm17.5 13.7h-4.1v-6.1c0-1.6-0.6-2.7-2.1-2.7-1.1 0-1.7 0.7-2 1.4-0.1 0.3-0.1 0.6-0.1 1v6.3h-4.1s0.1-10.7 0-11.7h4.1v1.8c0.2-0.8 1.6-2 3.7-2 2.6 0 4.6 1.7 4.6 5.3v6.7z" />
										</svg>
									</a>
								<?php endif; ?>
								<?php if( ! empty( get_theme_mod('sc_instagram_url') ) ): ?>
									<a href="<?php echo esc_url( get_theme_mod('sc_instagram_url') ); ?>" target="_blank">
										Instagram
										<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 43.7 43.7" version="1.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 43.7 43.7">
											<g stroke="#221F20" stroke-miterlimit="10">
												<circle r="21" cy="21.8" cx="21.8" stroke-width="1.7" fill="none"/>
												<path stroke-width=".25" d="m26.2 11.7h-8.8c-3.2 0-5.7 2.6-5.7 5.7v8.9c0 3.2 2.6 5.7 5.7 5.7h8.8c3.2 0 5.7-2.6 5.7-5.7v-8.9c0-3.2-2.5-5.7-5.7-5.7zm4.7 14.6c0 2.6-2.1 4.7-4.7 4.7h-8.8c-2.6 0-4.7-2.1-4.7-4.7v-8.9c0-2.6 2.1-4.7 4.7-4.7h8.8c2.6 0 4.7 2.1 4.7 4.7v8.9zm-3.9-10.3c-0.4 0-0.8 0.3-0.8 0.8 0 0.4 0.3 0.8 0.8 0.8 0.4 0 0.8-0.3 0.8-0.8s-0.4-0.8-0.8-0.8zm-5.2 1.4c-2.5 0-4.6 2-4.6 4.6s2 4.6 4.6 4.6c2.5 0 4.6-2 4.6-4.6s-2.1-4.6-4.6-4.6zm0 8c-1.9 0-3.5-1.6-3.5-3.5s1.6-3.5 3.5-3.5 3.5 1.6 3.5 3.5-1.5 3.5-3.5 3.5z" />
											</g>
										</svg>
									</a>
								<?php endif; ?>
								<?php if( ! empty( get_theme_mod('sc_youtube_url') ) ): ?>
									<a href="<?php echo esc_url( get_theme_mod('sc_youtube_url') ); ?>" title="YouTube" target="_blank">
										YouTube
										<svg style="enable-background:new 0 0 43.7 43.7" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 43.7 43.7" version="1.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink">
											<circle r="21" cy="21.8" cx="21.8" stroke-width="1.7" fill="none"/>
											<path d="m32.9 16.3c-0.3-1-1-1.8-2-2-1.8-0.5-9-0.5-9-0.5s-7.2 0-9 0.5c-1 0.3-1.8 1-2 2-0.5 1.8-0.5 5.6-0.5 5.6s0 3.8 0.5 5.6c0.3 1 1 1.8 2 2 1.8 0.5 9 0.5 9 0.5s7.2 0 9-0.5c1-0.3 1.8-1 2-2 0.5-1.8 0.5-5.6 0.5-5.6s0-3.8-0.5-5.6zm-13.4 9v-6.9l6 3.5-6 3.4z"/>
										</svg>
									</a>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-xs-12 col-md-10">
							<p class="disclaimer"><?php echo esc_html( get_theme_mod( 'sc_disclaimer' ) ); ?></p>
							<div class="navigation">
								<?php 
									wp_nav_menu(array(
										'theme_location' => 'footer_menu',
										'container'      => 'div',
										'depth'          => '1'
									));
								?>
							</div>
							<p class="copyright"><?php echo sc_dynamic_year( esc_html( get_theme_mod( 'sc_copyright' ) ) ); ?></p>
						</div>
					</div>
				</div><!-- /.container -->
			</footer><!-- /#footer -->
		</div><!-- /#wrapper -->
		<?php 

			$constant_contact_universal_code = get_theme_mod( 'sc_constant_contact_universal_code' );

			if( ! empty( $constant_contact_universal_code ) ) {

				echo $constant_contact_universal_code;

			}

			$sharethis_code = get_theme_mod( 'sc_sharethis_code' );

			if( ! empty( $sharethis_code ) ) {

				echo $sharethis_code;

			}

			wp_footer(); 
		
		?>
	</body>
</html>