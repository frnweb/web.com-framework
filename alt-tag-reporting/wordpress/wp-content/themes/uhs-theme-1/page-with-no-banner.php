<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 *
 * Template Name: No Banner 
 */
get_header();

if ( have_posts() ) {

	while ( have_posts() ) {

		the_post();

		sc_render_page_content();

	}

}

get_footer();