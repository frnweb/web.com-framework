<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and
 * make your changes there. Failure to do this will result in changes being
 * overwritten by an automatic update in the future.
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="format-detection" content="telephone=no">
    <!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<?php if( sc_google_tag_manager_id() ): ?>
			<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','<?php echo esc_js( sc_google_tag_manager_id() ); ?>');</script>
			<!-- End Google Tag Manager -->
		<?php endif; ?>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class( ( ! is_front_page() ? array('interior') : array('home') ) ); ?>>
		<?php if( sc_google_tag_manager_id() ): ?>
			<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo esc_js( sc_google_tag_manager_id() ); ?>"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) -->
		<?php endif; ?>
		<?php  sc_yotrack_swap(); ?>
		<a href="#skip-to-content-anchor" id="skip-to-content-link">[Skip to Content]</a>
		<div id="wrapper">
			<div id="mobile-navigation">
				<?php if( sc_use_desktop_nav_always() === true ): ?>
					<?php
						wp_nav_menu(array(
							'theme_location' => 'header_menu',
							'container'      => 'div',
							'container_class'=> 'desktop-menu-mobile',
							'depth'          => '4'
						));
					?>
				<?php else: ?>
					<ul>
						<?php wp_list_pages( array( 'depth' => 0, 'title_li' => null, 'sort_column' => 'menu_order, post_title' ) ); ?>
					</ul>
				<?php endif; ?>
			</div><!-- /#mobile-navigation -->
			<header id="header">
				<div class="header-top">
					<div class="container">
						<div class="row">
							<div class="col-xs-7 col-md-4">
								<div class="header-col header-col-left">
									<a href="<?php echo esc_url( get_bloginfo('url') ); ?>" class="logo">
										<?php sc_logo(); ?>
									</a>
								</div>
							</div>
							<div class="col-xs-5 col-md-8">
								<div class="header-col header-col-right">
									<div class="small-navigation clearfix">
										<?php
											wp_nav_menu(array(
												'theme_location' => 'header_menu_small',
												'container'      => 'div',
												'depth'          => '1'
											));
										?>
									</div>
									<a href="<?php echo site_url('?s') ?>" class="search-link">
										Search Link
										<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 42.2 40.1" version="1.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 42.2 40.1">
											<path d="m41.2 34.7l-8.5-7.4c2-3 2.9-6.6 2.8-10.2-0.4-9.6-8.1-17.1-17.7-17.1h-0.6c-9.8 0.4-17.5 8.6-17.2 18.4s8.5 17.4 18.4 17.1c3.6-0.1 7.2-1.4 10.1-3.6l8.6 7.4c0.6 0.5 1.3 0.8 2.1 0.8h0.1c0.8 0 1.6-0.4 2.2-1 1.1-1.2 1-3.2-0.3-4.4zm-35-16.6c-0.2-6.3 4.8-11.7 11.1-11.9h0.4c6.2 0 11.3 4.9 11.5 11.1 0.2 6.4-4.8 11.7-11.1 11.9-6.4 0.3-11.7-4.7-11.9-11.1z"/>
										</svg>
									</a>
									<a href="javascript:void(0);" class="mobile-menu-button hidden-md hidden-lg" id="mobile-menu-button">
										Open Navigation Menu
										<span></span>
										<span></span>
										<span></span>
									</a>
									<script type="text/template" id="header-search-template">
										<div id="header-search-form">
											<form method="get" class="header-search-form" action="<?php bloginfo( 'url' ); ?>/">
												<label for="header-search-input" class="hidden">Search Website</label>
												<input id="header-search-input" type="text" class="header-search-input" name="s" placeholder="Search Website" />
												<label for="header-search-submit" class="hidden">Submit Search</label>
												<button id="header-search-submit" type="submit" class="header-search-button" value="Search">
													<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 42.2 40.1" version="1.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 42.2 40.1">
														<path d="m41.2 34.7l-8.5-7.4c2-3 2.9-6.6 2.8-10.2-0.4-9.6-8.1-17.1-17.7-17.1h-0.6c-9.8 0.4-17.5 8.6-17.2 18.4s8.5 17.4 18.4 17.1c3.6-0.1 7.2-1.4 10.1-3.6l8.6 7.4c0.6 0.5 1.3 0.8 2.1 0.8h0.1c0.8 0 1.6-0.4 2.2-1 1.1-1.2 1-3.2-0.3-4.4zm-35-16.6c-0.2-6.3 4.8-11.7 11.1-11.9h0.4c6.2 0 11.3 4.9 11.5 11.1 0.2 6.4-4.8 11.7-11.1 11.9-6.4 0.3-11.7-4.7-11.9-11.1z"/>
													</svg>
												</button>
											</form>
										</div>
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header-bottom">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-10 hidden-xs hidden-sm">
								<div class="navigation clearfix" id="site-navigation">
									<?php
										wp_nav_menu(array(
											'theme_location' => 'header_menu',
											'container'      => 'div',
											'depth'          => '5'
										));
									?>
								</div>
							</div>
							<div class="col-xs-12 col-md-2 phone-link-container">
								<?php

								if( get_theme_mod( 'sc_header_phone_number',  false ) ) {

									printf(
										'<a href="tel:%s" class="phone-link"><i class="fa fa-phone" aria-hidden="true"></i> %s</a>',
										esc_attr( get_theme_mod( 'sc_header_phone_number',  false ) ),
										esc_html( get_theme_mod( 'sc_header_phone_number',  false ) )
									);

								}

								?>
							</div>
						</div>
					</div>
				</div>
			</header><!-- /#header -->
			<a name="skip-to-content-anchor" aria-hidden="true"></a>
