<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

if ( ! class_exists( 'SC_Widget_Sidebar_Nav_Page_Based' ) ) {

	class SC_Widget_Sidebar_Nav_Page_Based {

		public $pages_to_expand;

		function __construct() {

			$this->pages_to_expand = $this->get_pages_to_expand();

		}

		public function render() {

			echo $this->get_title_markup( $this->pages_to_expand[0] );
			echo '<ul class="menu">' . $this->get_nav_markup( 0 ) . '</ul>';

		}

		public function get_title_markup( $post_id ) {
	
			return sprintf(
				'<h4 class="sc-widget-title">%s</h4>',
				esc_html( get_the_title( $post_id ) )
			);
	
		}

		public function get_nav_markup( $depth ) {
	
			global $post;
	
			$page   = get_post( $this->pages_to_expand[$depth] );
			$pages  = $this->get_child_pages( $page->ID );
			$markup = '';
	
			$depth++;
	
			foreach ( $pages as $page ) {
	
				$sub_nav_markup = '';
				$classes = array( 'menu-item' );
	
				if ( $page->ID == $this->pages_to_expand[$depth] ) {
	
					$sub_nav_markup = $this->get_nav_markup( $depth );
				
				}
	
				if ( count( $this->get_child_pages( $page->ID ) ) ) {
	
					$classes[] = 'has-children';
				
				}
	
				if ( $sub_nav_markup != '' ) {
	
					$classes[] = 'expanded-menu-item';
	
				}
	
				if ( $page->ID == $post->ID ) {
	
					$classes[] = 'current-menu-item';
	
				}
	
				$markup .= sprintf('
					<li class="%s" data-level="%d">
						<a href="%s">%s</a>
						<ul class="sub-menu">%s</ul>
					</li>',
					esc_attr( implode( ' ', $classes) ),
					esc_attr( $depth + 1 ),
					get_permalink( $page->ID ),
					esc_html( $page->post_title ),
					$sub_nav_markup
				);
	
			}
	
			return $markup;
	
		}
	
		public function get_child_pages( $post_id ) {
	
			global $post;
	
			$page_query = new WP_Query(array(
				'post_type'      => 'page',
				'post_parent'    => $post_id,
				'post_status'    => 'publish',
				'orderby'        => 'menu_order',
				'order'          => 'asc',
				'posts_per_page' => -1
			));
	
			$pages = array();
	
			if ( $page_query->have_posts() ) {
	
				while ( $page_query->have_posts() ) {
	
					$page_query->the_post();
	
					$pages[] = $post;
				
				}
	
				wp_reset_postdata();
	
			}
	
			return $pages;
	
		}

		public function get_pages_to_expand() {
	
			global $post;
	
			$post_id         = $post->ID;
			$pages_to_expand = array_reverse( get_post_ancestors( $post_id ) );
			
			if( 0 === count( $pages_to_expand ) ) {
	
				$pages_to_expand[] = $post_id;
	
			} else {
	
				$pages_to_expand[] = $post_id;
	
			}
	
			return $pages_to_expand;
	
		}

	}

}
