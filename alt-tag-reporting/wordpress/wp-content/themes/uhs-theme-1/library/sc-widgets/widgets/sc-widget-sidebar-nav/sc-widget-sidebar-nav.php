<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

require_once( 'sc-widget-sidebar-nav-page-based.php' );
require_once( 'sc-widget-sidebar-nav-menu-based.php' );

if ( ! class_exists( 'SC_Widget_Sidebar_Nav' ) ) {

	class SC_Widget_Sidebar_Nav extends WP_Widget {
	
		/**
		 * Sets up the widgets name etc
		 */
		function __construct()
		{
			parent::__construct(
				'sc-widget-sidebar-nav',
				'Sidebar Nav',
				array('description' => 'Context sensitive sidebar navigation.')
			);
		}
	
		/**
		 * Outputs the content of the widget
		 *
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {
	
			if( ! is_page() ) {
	
				return false;
	
			}
	
			echo str_replace( '_', '-', $args['before_widget'] );

			if( get_theme_mod( 'sc_use_header_menu_based_sidebar_nav' ) ) {

				$builder = new SC_Widget_Sidebar_Nav_Menu_Based;
				
			} else {
				
				$builder = new SC_Widget_Sidebar_Nav_Page_Based;

			}

			$builder->render();
	
			echo $args['after_widget'];
	
		}
	
		/**
		 * Outputs the options form on admin
		 *
		 * @param array $instance The widget options
		 */
		public function form( $instance ) {
	
			echo '<br /><br />';
	
		}
	
		/**
		 * Processing widget options on save
		 *
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 */
		public function update( $new_instance, $old_instance ) {
	
			return $new_instance;
			
		}
	
	}

}
