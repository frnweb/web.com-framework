<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

$fluid = $module['size'] == 12 ? true : false;
$class = array(
	'sc-module',
	'sc-module-banner'
);

if( $fluid ) {

	$class[] = 'sc-force-fluid';

}

if( ! empty( $module['lg_image'] ) ) {

	$class[] = 'lg-image';

}

if( ! empty( $module['md_image'] ) ) {

	$class[] = 'md-image';

}

if( ! empty( $module['sm_image'] ) ) {

	$class[] = 'sm-image';

}

?>
<section class="<?php echo implode(' ', $class); ?>" id="<?php echo esc_attr( $module['module_id'] ); ?>" data-lg="<?php echo $module['lg_image']['url']; ?>" data-md="<?php echo $module['md_image']['url']; ?>" data-sm="<?php echo $module['sm_image']['url']; ?>">
  
	<?php if($fluid): ?>
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 banner-content-wrapper">
  <?php else: ?>
		<div class="banner-content-wrapper">
  <?php endif; ?>
	
	<?php if( ! empty( $module['icon'] ) ): ?>
		<div class="banner-icon">
			<?php echo '<img src="' . esc_url( $module['icon']['url'] ) . '" alt="' . esc_attr( $module['icon']['alt'] ) . '">'; ?>
		</div>
	<?php endif; ?>
	
	<?php if( ! empty( $module['content'] ) ): ?>
		<div class="banner-content">
			<h4><?php echo esc_html( $module['headline'] ); ?></h4>
			<p><?php echo esc_html( $module['content'] ); ?></p>
		</div>
	<?php endif; ?>
	
	<?php if( ! empty( $module['button_link'] ) && ! empty( $module['button_text'] ) ): ?>
		<?php

			$button_stroke = '';

			if( true === $module['add_stroke'] ) {

				$button_stroke = 'add-stroke';

			}

			$button_target = '';

			if( isset( $module['button_target'][0] ) ) {

				$button_target = $module['button_target'][0];

			}


		?>
		<div class="banner-cta">
			<a class="button <?php echo esc_attr( $button_stroke ); ?>" href="<?php echo esc_url( $module['button_link'] ); ?>" target="<?php echo esc_attr( $button_target ); ?>"><?php echo esc_html( $module['button_text'] ); ?></a>
		</div>
	<?php endif; ?>
  
  <?php if($fluid): ?>
			</div>
		</div>
  <?php else: ?>
		</div>
  <?php endif; ?>

</section>
<script type="text/javascript">

  (function($) {

    var module = $('#<?php echo esc_js( $module['module_id'] ); ?>');

    $(document).ready(function() {

      bannerImgSwap();

      $(window).resize( _.throttle( onResize, 250 ) );

    });

    $(window).load(function() {

      //

    });

    function onResize() {

      bannerImgSwap();
		
    }
	
	function bannerImgSwap() {

	  if(sc.mediaQueries.match('md_up')) {

			$('#<?php echo esc_js( $module['module_id'] ); ?>.lg-image').css('background-image', 'url("' + module.data('lg') + '")');
	  
		} else if(sc.mediaQueries.match('sm_up')) {

			$('#<?php echo esc_js( $module['module_id'] ); ?>.md-image').css('background-image', 'url("' + module.data('md') + '")');

	  } else {

			$('#<?php echo esc_js( $module['module_id'] ); ?>.sm-image').css('background-image', 'url("' + module.data('sm') + '")');
	  }

	}

  })(jQuery);

</script>