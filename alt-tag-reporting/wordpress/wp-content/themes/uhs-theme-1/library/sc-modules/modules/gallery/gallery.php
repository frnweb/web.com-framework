<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

?>
<section class="sc-module sc-module-gallery clearfix" id="<?php echo esc_attr( $module['module_id'] ); ?>">
  <?php if (! empty( $module['title'] ) ): ?>
  	<h4><?php echo esc_html( $module['title'] ); ?></h4>
  <?php endif; ?>
  <div class="gallery-slider">
		<?php foreach($module['gallery'] as $img) : ?>
			<div class="gallery-slide">
				<div class="content">
						<img src="<?php echo esc_url( $img['sizes']['gallery-display'] ); ?>" alt="<?php echo esc_attr( $img['alt'] ); ?>" class="image" />
				</div>
			</div>
		<?php endforeach; ?>
  </div>
  <div class="thumbnails">
		<?php foreach($module['gallery'] as $img) : ?>
			<div>
				<img src="<?php echo esc_url( $img['sizes']['gallery-thumb'] ); ?>" alt="<?php echo esc_attr( $img['alt'] ); ?>">
			</div>
		<?php endforeach; ?>
  </div>
  <div class="gallery-footer">
	  <div class="caption-slider">
			<?php foreach($module['gallery'] as $img) : ?>
				<div>
					<h5><?php echo esc_html( $img['title'] ); ?></h5>
					<p><?php echo wp_trim_words( esc_html( $img['caption'] ), 40 ); ?></p>
				</div>
			<?php endforeach; ?>
	  </div>
	  <div class="arrows"></div>
  </div>
</section>
<script type="text/javascript">

  (function($) {

    var module = $('#<?php echo esc_js( $module['module_id'] ); ?>');

    $(document).ready(function() {

			$('#<?php echo esc_js( $module['module_id'] ); ?> .gallery-slider').slick({
				accessibility: true,
				dots: false,
				arrows: true,
				appendArrows: '#<?php echo esc_js( $module['module_id'] ); ?> .gallery-footer > .arrows',
				prevArrow: '<button type="button" class="fa fa-chevron-left">Previous Image</button>',
				nextArrow: '<button type="button" class="fa fa-chevron-right">Next Image</button>',
				asNavFor: '#<?php echo esc_js( $module['module_id'] ); ?> .caption-slider, #<?php echo esc_js( $module['module_id'] ); ?> .thumbnails'
			});
	  
			$('#<?php echo esc_js( $module['module_id'] ); ?> .caption-slider').slick({
				accessibility: true,
				dots: false,
				arrows: false,
				asNavFor: '#<?php echo esc_js( $module['module_id'] ); ?> .thumbnails, #<?php echo esc_js( $module['module_id'] ); ?> .gallery-slider'
			});
	  
			$('#<?php echo esc_js( $module['module_id'] ); ?> .thumbnails').slick({
				accessibility: true,
				dots: false,
				arrows: false,
				asNavFor: '#<?php echo esc_js( $module['module_id'] ); ?> .gallery-slider, #<?php echo esc_js( $module['module_id'] ); ?> .caption-slider',
				slidesToShow: 5,
				slidesToScroll: 1,
				focusOnSelect: true,
				responsive: [{
					breakpoint: 750,
					settings: {
						dots: false,
						arrows: false,
						slidesToShow: 4,
						slidesToScroll: 1
					}
				}]
			});

    });

  })(jQuery);

</script>