<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

$title   = $module['title'];
$stories = [];

if( is_array( $module['story'] ) ) {

	foreach($module['story'] as $story) {

		$original_image = isset( $story['image']['url'] ) ? $story['image']['url'] : false;
		$desktop_image  = false;
		$mobile_image   = false;

		if( isset( $story['image']['sizes']['story-md-up'] ) ) {

			$desktop_image = $story['image']['sizes']['story-md-up'];

		}

		if( isset( $story['image']['sizes']['story-xs-up-recommended'] ) ) {

			if( $story['image']['sizes']['story-xs-up-recommended'] !== $original_image ) {

				$mobile_image = $story['image']['sizes']['story-xs-up-recommended'];

			}

		}
		
		if( isset( $story['image']['sizes']['story-xs-up'] ) && false === $mobile_image ) {

			$mobile_image = $story['image']['sizes']['story-xs-up'];

		}

		$stories[] = sprintf(
			'<div class="story">
				<div class="story-image" data-sm="%s" data-lg="%s">
					<div class="content"></div>
				</div>
				<div class="story-info">
					<div class="story-headline">%s</div>
					<div class="story-content">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.69 20.51" class="story-icon">
							<path class="cls-1" d="M8,7.77c0,1.88,3.91,2.18,3.91,6.8A5.71,5.71,0,0,1,6,20.51c-3.66,0-6-3.25-6-6.7C0,7.87,5.58,0,9,0c.86,0,2.44.61,2.44,1.68S8,4.57,8,7.77Zm13.81,0c0,1.88,3.91,2.18,3.91,6.8a5.71,5.71,0,0,1-5.84,5.94c-3.65,0-6-3.25-6-6.7,0-5.94,5.58-13.81,9-13.81.86,0,2.44.61,2.44,1.68S21.78,4.57,21.78,7.77Z"/>
						</svg>
						<p>%s</p>
						<h5 class="story-name">%s</h5>
						<div class="story-relation">%s</div>
					</div>
				</div>
			</div>',
			$mobile_image ? $mobile_image : get_stylesheet_directory_uri() . '/assets/images/story-default.jpg',
			$desktop_image ? $desktop_image : get_stylesheet_directory_uri() . '/assets/images/story-default-lg.jpg',
			$story['headline'],
			$story['content'],
			$story['name'],
			$story['relation']
		);
		
	}

}

?>
<section class="sc-module sc-module-stories" id="<?php echo esc_attr( $module['module_id'] ); ?>" data-align="<?php echo $module['image_alignment']; ?>">
	<h4><?php echo esc_html($module['title']); ?></h4>
	<div class="sc-stories">
		<?php echo implode('', $stories); ?>
	</div>
	<div class="story-nav">
		<div class="dots"></div>
		<div class="arrows"></div>
	</div>
</section>
<script type="text/javascript">

	(function($){

		function imgSwap() {

			if( sc.mediaQueries.match('md_up') ) {

				$('#<?php echo $module['module_id']; ?> .story-image').each(function() {
					$(this).css('background-image', 'url("' + $(this).data('lg') + '")');
				});

			} else {

				$('#<?php echo $module['module_id']; ?> .story-image').each(function() {
					$(this).css('background-image', 'url("' + $(this).data('sm') + '")');
				});

			}


			// if(! sc.mediaQueries.match( 'md_up') && ! sc.mediaQueries.match('xxs_only')) {
			
			// 	$('#<?php echo $module['module_id']; ?> .story-image').each(function() {
			// 		$(this).css('background-image', 'url("' + $(this).data('sm') + '")');
			// 	});

			// } else {
			
			// 	$('#<?php echo $module['module_id']; ?> .story-image').each(function() {
			// 		$(this).css('background-image', 'url("' + $(this).data('lg') + '")');
			// 	});
			
			// }

		}

		$(document).ready(function() {

			$('#<?php echo $module['module_id']; ?> > .sc-stories').slick({
				accessibility: true,
				dots: true,
				mobileFirst: true,
				adaptiveHeight: true,
				fade: true,
				prevArrow: '<button type="button" class="fa fa-chevron-left">Previous Story</button>',
				nextArrow: '<button type="button" class="fa fa-chevron-right">Next Story</button>',
				appendDots: $('#<?php echo $module['module_id']; ?> > .story-nav .dots'),
				appendArrows: $('#<?php echo $module['module_id']; ?> > .story-nav .arrows')
			});

			imgSwap();

		});
		
		$(window).resize(imgSwap);

	})(jQuery);

</script>