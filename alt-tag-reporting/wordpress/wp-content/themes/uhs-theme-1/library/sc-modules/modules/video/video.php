<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

preg_match( '/src="(.+?)"/i', $module['video'], $matches );

if( isset( $matches[1] ) ) {

  $new_source      = add_query_arg( 'rel', '0', $matches[1] );
  $module['video'] = str_replace( $matches[1], $new_source, $module['video'] );

}

$vid_frame = str_replace( 
	'<iframe ', 
	'<iframe title="Video - ' . esc_attr( $module['headline'] ) . '" ', 
	$module['video']
);

?>
<section class="sc-module sc-module-video" id="<?php echo esc_attr( $module['module_id'] ); ?>">
	<h4><?php echo esc_html( $module['title'] ); ?></h4>
	<div class="video">
		<div class="content">
			<?php echo $vid_frame; ?>
		</div>
	</div>
	<div class="content">
		<div class="headline">
			<?php echo esc_html( $module['headline'] ); ?>
		</div>
		<p class="body">
			<?php echo esc_html( $module['description'] ); ?>
		</p>
	</div>
</section>
