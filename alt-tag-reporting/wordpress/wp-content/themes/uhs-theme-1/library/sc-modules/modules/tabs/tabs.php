<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

global $wp;

$defaults = array(
	'tabs'         => array(),
	'display_mode' => 'tabs'
);

$module         = array_merge( $defaults, $module );
$section_links  = array();
$section_panels = array();
$display_mode   = $module['display_mode'];
$classes        = array('sc-module', 'sc-module-tabs');

if( 'accordion' == $display_mode ) {

	$classes[] = 'sc-module-accordion';

}

if( is_array( $module['tabs'] ) ) {

	$current_url = home_url( $wp->request );
	$active      = 'yes';
	$index       = 1;

	foreach( $module['tabs'] as $tab ) {

		$name    = trim( $tab['tab_label'] );
		$slug    = sanitize_title( $name );
		$content = apply_filters( 'the_content', trim( $tab['tab_content'] ) );
		$url     = $current_url . '#' . $slug;

		if( empty( $name ) || empty( $content ) ) {

			continue;

		}

		$section_labels[] = sprintf('
			<li class="section-label" data-active="%1$s" data-section="%2$s" data-index="%4$s">
				<a href="%5$s">%3$s</a>
			</li>',
			$active,
			$slug,
			$name,
			$index,
			$url
		);

		$section_panels[] = sprintf('
			<div class="section-label" data-active="%1$s" data-section="%2$s" data-index="%4$s">
				<a href="%6$s">%3$s</a>
			</div>
			<div class="section-panel" data-active="%1$s" data-section="%2$s" data-index="%4$s">
				<a name="%2$s" aria-hidden="true"></a>
				%5$s
			</div>',
			$active,
			$slug,
			$name,
			$index,
			$content,
			$url
		);

		$active = 'no';
		$index++;

	}
}

?>
<?php if( count( $section_labels ) ): ?>
	<section 
		class="<?php echo implode( ' ', $classes ); ?>" 
		id="<?php echo esc_attr( $module['module_id'] ); ?>" 
		data-display-mode="<?php echo esc_attr( $module['display_mode'] ); ?>">
		<div class="row">
			<div class="col-xs-12">
				<?php if( ! empty( $module['tabs_layout_headline'] ) ): ?>
					<h4><?php echo esc_html( $module['tabs_layout_headline'] ); ?></h4>
				<?php endif; ?>
				<div class="section-labels">
					<ul>
						<?php echo implode( '', $section_labels ); ?>
					</ul>
				</div>
				<div class="section-panels">
					<?php echo implode( '', $section_panels ); ?>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">

		(function($) {

			var moduleId = '#<?php echo esc_js( $module['module_id'] ); ?>';
			var module;

			$(document).ready(function() {

				module = $(moduleId);

				$(window).resize(_.throttle(onResize, 100));

				initLabelEvents();
				updateDisplayMode();

				sc.responsiveTable.init(moduleId);
				
			});

			$(window).load(function() {

				updateDisplayMode();

			});

			function onResize() {

				updateDisplayMode();

			}

			function initLabelEvents() {

				module.find('.section-label a').click(function(event) {

					event.preventDefault();

					selectTab( $(this).parent().data('index') );

				});

			}

			function updateDisplayMode() {

				var displayMode  = module.data('display-mode');
				var currentLabelTop, 
						lastLabelTop;

				if('tabs' == displayMode) {

					module.find('.section-labels .section-label').each(function(index, sectionLabel) {

						currentLabelTop = $(sectionLabel).position().top;

						if(undefined === lastLabelTop) {

							lastLabelTop = currentLabelTop;

						}

						if(lastLabelTop != currentLabelTop) {

							displayMode = 'accordion';

						}

						lastLabelTop = currentLabelTop;

					});

				}

				switch(displayMode) {

					case 'tabs':

						module.removeClass('sc-module-accordion');
						break;

					case 'accordion':

						module.addClass('sc-module-accordion');
						break;

				}

			}

			function selectTab(index) {
			
				if( $(moduleId + '.sc-module-accordion').find('.section-label[data-index="' + index + '"]').attr('data-active') == 'yes' ) {
					
					$(moduleId + '.sc-module-accordion').find('.section-label[data-index="' + index + '"]').attr('data-active', 'no');
					$(moduleId + '.sc-module-accordion').find('.section-panel[data-index="' + index + '"]').attr('data-active', 'no');
					return;
					
				}

				module.find('.section-label').attr('data-active', 'no');
				module.find('.section-panel').attr('data-active', 'no');

				module.find('.section-label[data-index="' + index + '"]').attr('data-active', 'yes');
				module.find('.section-panel[data-index="' + index + '"]').attr('data-active', 'yes');

			}

		})(jQuery);

	</script>
<?php endif; ?>