<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

if ( ! class_exists( 'SC_Facilities' ) ) {

	class SC_Facilities {

		private static $_data;
		private static $_facilities;

		public static function get_facilities() {

			if ( is_array( self::$_facilities ) ) {

				return self::$_facilities;

			}

			self::$_facilities = array();

			if ( is_array( self::$_data['facilities'] ) ) {

				foreach ( self::$_data['facilities'] as $facility ) {

					if( ! isset( $facility['location']['lat'] ) ) {

						continue;

					}

					if( ! isset( $facility['title'] ) || empty( trim( $facility['title'] ) )  ) {

						continue;

					}

					$parsed_facility = (object) array(
						'location'          	=> $facility['location'],
						'title'             	=> trim( $facility['title'] ),
						'link'              	=> isset( $facility['link'] ) ? trim( $facility['link'] ) : '',
						'phone_numbers'     	=> array(),
						'image'              	=> false,
						'directions_url'        => esc_url( 'https://www.google.com/maps/dir/?api=1&destination=' . $facility['location']['address'] ),
						'tooltip_is_enabled'    => $facility['tooltip_is_enabled'],
						'location_is_enabled'   => $facility['location_is_enabled'],
						'directions_is_enabled' => $facility['directions_is_enabled'],
						'tooltip_cta_text'		=> esc_html( $facility['tooltip_cta_text'] ),
						'tooltip_cta_url'		=> esc_url( $facility['tooltip_cta_url'] )
					);

					if( isset( $facility['image']['sizes']['medium'] ) ) {

						$source = $facility['image']['sizes']['medium'];
						$alt    = trim( $facility['image']['alt'] );

						if( empty( $alt ) ) {

							$alt = trim( $facility['title'] );

						}

						$parsed_facility->image = (object) array(
							'source' => $source,
							'alt'    => $alt
						);

					}

					foreach( $facility['phone_numbers'] as $phone ) {

						$number = isset( $phone['phone_number'] ) ? trim( $phone['phone_number'] ) : '';
						$label  = isset( $phone['phone_label'] ) ? trim( $phone['phone_label'] ) : '';

						if( empty( trim( $number ) ) ) {
							continue;
						}

						$parsed_facility->phone_numbers[] = (object) array(
							'number' => $number,
							'label'  => $label
						);

					}

					self::$_facilities[] = $parsed_facility;

				}
			}

			return self::$_facilities;

		}

		public static function get_facility_classes() {

			$classes = array( 'facility' );

			if( 1 === count( self::get_facilities() ) ) {

				$classes[] = 'single';

			}

			return $classes;

		}

		public static function set_data( $data ) {
			self::$_data       = $data;
			self::$_facilities = null;
		}

	}

}

SC_Facilities::set_data( $module );

$facilities       = SC_Facilities::get_facilities();
$facilities_total = count( $facilities );
$facility_classes = implode( ' ', SC_Facilities::get_facility_classes() );

?>
<?php if( $facilities_total ) : ?>
	<section class="sc-module sc-module-location" id="<?php echo esc_attr( $module['module_id'] ); ?>">
		<div class="map"></div>
			<?php foreach( $facilities as $facility ): ?>
				<?php if( ! $facility->location_is_enabled ) {
					continue;
				} 
				?>
				<div class="<?php echo esc_attr( $facility_classes ); ?>">
					<div class="location-info">
						<div class="details">
							<?php 
			
							if ( false !== $facility->image ) {
			
								sc_image( 
									$facility->image->source, 
									array( 
										'alt'   => $facility->image->alt,
										'class' => 'image'
									)
								);
			
							}
			
							?>
							<div class="body">
								<h4 class="title"><?php echo esc_html( $facility->title ); ?></h4>
								<div class="address">
									<i class="fa fa-map-marker"></i>
									<?php 
									
									echo esc_html( $facility->location['address'] );
			
									if ( ! empty( $facility->link ) ) {
			
										echo '<a href="' . esc_url( $facility->link ) . '">Contact Us</a>';
			
									} 
			
									?>
								</div>
								<?php if ( count( $facility->phone_numbers ) ) : ?>
									<div class="phone_numbers">
										<i class="fa fa-phone"></i>
										<?php 

											foreach($facility->phone_numbers as $phone_number ) {

												printf(
													'<span class="phone_number"><a href="tel:%s">%s %s</a></span>',
													esc_attr( $phone_number->number ),
													esc_html( $phone_number->label ),
													esc_html( $phone_number->number )
												);

											}

										?>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="cta-directions">
							<a href="<?php echo esc_url( 'https://www.google.com/maps/dir/?api=1&destination=' . $facility->location['address'] ); ?>" class="button" target="_blank">Get Directions</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
	</section>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCPCdnxQ6y_RwP1jy7teKfUTZZa5POU7sI"></script>
	<script type="text/javascript">

			var module      = jQuery('#<?php echo esc_js( $module['module_id'] ); ?>');
			var facilites   = <?php echo json_encode( $facilities ); ?>;
			var markerColor = '<?php echo esc_js( isset( $module['map_marker_color'] ) ? $module['map_marker_color'] : 'tertiary' ); ?>';
			var themeColors = {
				'primary'   : '<?php echo esc_js( get_theme_mod( 'sc_primary_color', '#351a52' ) ); ?>',
				'secondary' : '<?php echo esc_js( get_theme_mod( 'sc_secondary_color', '#be3932' ) ); ?>',
				'tertiary'  : '<?php echo esc_js( get_theme_mod( 'sc_tertiary_color', '#f4504c' ) ); ?>',
				'text'      : '<?php echo esc_js( get_theme_mod( 'sc_text_color', '#33333c' ) ); ?>'
			};
      var map, bounds, markers, facility, infoWindows;

      markerColor = themeColors[markerColor];

			function render_map() {

				bounds  = new google.maps.LatLngBounds();
				map     = new google.maps.Map(module.find('.map')[0], {
					center: new google.maps.LatLng(facilites[0].location['lat'], facilites[0].location['lng']),
					zoom:12,
					scrollwheel: false,
					styles: [
						{
							"featureType": "landscape.man_made",
							"elementType": "geometry.fill",
							"stylers": [
								{
									"color": '#<?php echo esc_js( SC_Customizer_Stylesheet::singleton()->color_blend_by_opacity( get_theme_mod( 'sc_primary_color', '351A52' ), 10 ) ); ?>'
								}
							]
						},
						{
							"featureType": "landscape.natural",
							"elementType": "geometry.fill",
							"stylers": [
								{
									"color": '#<?php echo esc_js( SC_Customizer_Stylesheet::singleton()->color_blend_by_opacity( get_theme_mod( 'sc_primary_color', '351A52' ), 5 ) ); ?>'
								}
							]
						},
						{
							"featureType": "poi.business",
							"elementType": "geometry.fill",
							"stylers": [
								{
									"color": '#<?php echo esc_js( SC_Customizer_Stylesheet::singleton()->color_blend_by_opacity( get_theme_mod( 'sc_primary_color', '351A52' ), 30 ) ); ?>'
								}
							]
						},
						{
							"featureType": "poi.park",
							"elementType": "geometry.fill",
							"stylers": [
								{
									"color": "#e1e5e0"
								}
							]
						},
						{
							"featureType": "road.highway",
							"elementType": "geometry.fill",
							"stylers": [
								{
									"color": '#<?php echo esc_js( SC_Customizer_Stylesheet::singleton()->color_blend_by_opacity( get_theme_mod( 'sc_secondary_color', 'F4504C' ), 30 ) ); ?>'
								}
							]
						},
						{
							"featureType": "road.highway",
							"elementType": "geometry.stroke",
							"stylers": [
								{
									"color": '#<?php echo esc_js( SC_Customizer_Stylesheet::singleton()->color_blend_by_opacity( get_theme_mod( 'sc_secondary_color', 'F4504C' ), 30 ) ); ?>'
								}
							]
						},
						{
							"featureType": "water",
							"elementType": "geometry.fill",
							"stylers": [
								{
									"color": "#ccd9ee"
								}
							]
						}
					]
				});

				map.markers     = [];
				var infoWindows = [];
				

				for(var c = 0; c < facilites.length; c++) {

					facility = facilites[c];

					var image = {
						url: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
						// This marker is 20 pixels wide by 32 pixels high.
						size: new google.maps.Size(20, 32),
					};

					var marker = new google.maps.Marker({
							position: new google.maps.LatLng(facility.location['lat'], facility.location['lng']),
							map: map,
							icon: image,
							label: {
								fontFamily: 'Fontawesome',
								text: '\uf041',
								color: markerColor,
								fontSize: '34px'
							},
							zIndex: 100
						});

					if ( facility.tooltip_is_enabled ) {
						var content = "<h4>" + facility.title + "</h4>";
								content += "<p>" + facility.location.address + "</p>";

						if ( facility.phone_numbers.length ) {
							
							content += "<p>"; // begin phone numbers		

							for ( var i = 0; i < facility.phone_numbers.length; i++ ) {
								var number = facility.phone_numbers[i].number;
								var label  = facility.phone_numbers[i].label;

								 
								
								if( typeof phones !== 'undefined' && phones.length > 0) {
									
									for( var j = 0; j < phones.length; j++ ) {
										
										var isInPhoneArray = phones[j].orginal_lines.includes(number);

										if(isInPhoneArray) {

											number = phones[j].swapped_line;

										}

									}

								}
								
								content += "<strong>" + label + "</strong>: <a href=\"tel:" + number + "\">" + number + "</a><br>";
							}

							content += "</p>"; //end phone numbers

						}

						content += '<div class="ctas">';

						<?php if ( $facility->directions_is_enabled ): ?>

							content += "<p><a class=\"arrow-link directions\" href=" + facility.directions_url + " target=\"_blank\">Get Directions</a></p>";

						<?php endif; ?>

						if( facility.tooltip_cta_text && facility.tooltip_cta_url ){

							content += "<p><a class=\"arrow-link cta\" href=" + facility.tooltip_cta_url + " target=\"_blank\">" + facility.tooltip_cta_text + "</a></p>";

						}

						content  += "</div>";

						attachTooltip(facility, marker, content, infoWindows);

						
					}

					map.markers.push(
						marker
					);

					bounds.extend(new google.maps.LatLng(facility.location['lat'], facility.location['lng']));

				}

				if(facilites.length > 1){
					map.fitBounds(bounds);
				}

			}

			function attachTooltip( facility, marker, content, infoWindows ) {
				
				google.maps.event.addListener(marker, 'click', function(e) {
					closeAllInfoWindows(infoWindows);

					infowindow = new google.maps.InfoWindow({
						content: content
					});

					infowindow.open(marker.get('map'), marker);
					infoWindows.push(infowindow);
				});
			}

			function closeAllInfoWindows( infoWindows ) {

				if ( infoWindows.length ) {

					for ( var c = 0; c < infoWindows.length; c++ ) {

						if ( infoWindows[c] ) {
							
							infoWindows[c].close();

						}
	
					}

				}


			}

	</script>
<?php endif; ?>