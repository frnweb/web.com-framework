<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

$classes          = [ 'sc-module', 'sc-module-card' ];
$card_link_text   = 'Learn More';
$card_link_target = '';

if( isset( $module['fixed_height'][0] ) ) {
	$classes['height'] = 'sc-module-card-fixed-height';
}

if( ! empty( $module['image'] ) ) {
	$card_img     = $module['image']['url'];
	$card_img_alt = $module['image']['alt'];
}

if( ! empty( $module['title'] ) ) {
	$card_title = $module['title'];
}

if( ! empty( $module['text'] ) ) {
	$card_text = $module['text'];
}

if( ! empty( $module['link'] ) ) {
	$card_link = $module['link'];
}

if( ! empty( $module['link_text'] ) ) {
	$card_link_text = $module['link_text'];
}

if( isset( $module['link_target'][0] ) ) {
	$card_link_target = $module['link_target'][0];
}

?>
<section class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>" id="<?php echo esc_attr( $module['module_id'] ); ?>">
  <?php if( isset( $card_img ) ) : ?>
		<div class="card-header">
			<div class="card-image" style="background-image:url('<?php echo esc_url( $card_img ); ?>');"></div>
			<?php

				sc_image( 
					$card_img, 
					array (
						'class' => 'card-image-constraint',
						'alt'   => $card_img_alt
					) 
				);

			?>
		</div>
	<?php endif; ?>
  <div class="card-content">
		<?php 

		if( isset( $card_title ) ) {

			echo '<h5>' . esc_html( $card_title ) . '</h5>';

		}

		if( isset( $card_text ) ) {

			echo '<p>' . esc_html( $card_text ) . '</p>';

		}
		
		if( isset( $card_link ) ) {

			printf(
				'<a class="arrow-link" href="%s" target="%s">%s</a>',
				esc_url( $card_link ),
				esc_attr( $card_link_target ),
				esc_html( $card_link_text )
			);

		}

		?>
  </div>
</section>
<script type="text/javascript">

  (function($) {

		var module     = $('#<?php echo esc_js( $module['module_id'] ); ?>');
		var image      = module.find('.card-image');
		var constraint = module.find('.card-image-constraint');

    $(document).ready(function() {

			equalizeCards();

      $(window).resize( _.throttle( onResize, 250 ) );

			module.click(function(e) {

				e.preventDefault();
				e.stopPropagation();

				if( $(this).find('.arrow-link').length ) {

					if($(this).find('.arrow-link').attr('target') === '_blank'){
						window.open($(this).find('a').attr('href'));
					} else {
						window.location = $(this).find('a').attr('href');
					}

				}

			});

			module.hover(onHover, onHoverOut);

    });

    $(window).load(function() {

	  	equalizeCards();

    });

    function onResize() {

	  	equalizeCards();

		}
		
		function onHover(event) {

			image
				.css('width',  constraint.width()  + 16)
				.css('height', constraint.height() + 16);

		}

		function onHoverOut(event) {

			image
				.css('width',  constraint.width())
				.css('height', constraint.height());

		}
	
		function equalizeCards() {

			if( sc.mediaQueries.match( 'md_up' ) ) {

				var h = 0;

				$('.container .sc-module-card').matchHeight();

			} else {

				$('.container .sc-module-card').height( 'auto' );

			}

		}

  })(jQuery);

</script>