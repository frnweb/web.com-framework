<?php if (!defined('ABSPATH')) {
	die('No direct access allowed');
}

if( ! class_exists( 'SC_Social_Feed_Module' ) ) {

	class SC_Social_Feed_Module {

		private static $_data;

		public static function get_section_labels_markup() {

			global $wp;

			$section_labels = [];
			$current_url    = home_url($wp->request);
			$active         = 'yes';
			$index          = 1;

			if (is_array(self::$_data['facebook_enabled'])) {

				$section_labels[] = sprintf(
					'<li class="section-label" data-active="%1$s" data-section="facebook" data-index="%2$s">
            <a href="%3$s">Facebook</a>
          </li>',
					esc_attr($active),
					esc_attr($index),
					esc_url($current_url . '#' . $slug)
				);

				$index++;
				$active = 'no';

			}

			if (is_array(self::$_data['twitter_enabled'])) {

				$section_labels[] = sprintf(
					'<li class="section-label" data-active="%1$s" data-section="twitter" data-index="%2$s">
            <a href="%3$s">Twitter</a>
          </li>',
					esc_attr($active),
					esc_attr($index),
					esc_url($current_url . '#' . $slug)
				);

				$index++;
				$active = 'no';

			}

			if (count($section_labels)) {

				return sprintf(
					'<div class="section-labels">%s</div>',
					implode('', $section_labels)
				);

			}

			return '';

		}

		public static function get_section_panels_markup() {

			global $wp;

			$section_panels = [];
			$active         = 'yes';
			$index          = 1;

			if( is_array( self::$_data['facebook_enabled'] ) ) {

				$section_panels[] = sprintf('
          <div class="section-panel" data-active="%1$s" data-section="facebook" data-index="%2$s">
            <a name="facebook" aria-hidden="true"></a>
            %3$s
          </div>',
					esc_attr( $active ),
					esc_attr( $index ),
					do_shortcode( '[custom-facebook-feed num=2 layout=full]' )
				);

				$index++;
				$active = 'no';

			}

			if( is_array( self::$_data['twitter_enabled'] ) ) {

				$section_panels[] = sprintf('
          <div class="section-panel" data-active="%1$s" data-section="twitter" data-index="%2$s">
            <a name="twitter" aria-hidden="true"></a>
            %3$s
          </div>',
					esc_attr( $active ),
          esc_attr( $index ),
          do_shortcode( '[custom-twitter-feeds num=2]' )
				);

				$index++;
				$active = 'no';

			}

			if( count( $section_panels ) ) {

				return sprintf(
					'<div class="section-panels">%s</div>',
					implode( '', $section_panels )
				);

			}

			return '';

		}

		public static function set_data( $data ) {

			self::$_data = $data;

		}

	}

}

SC_Social_Feed_Module::set_data( $module );

?>
<section class="sc-module sc-module-social-feed" id="<?php echo esc_attr( $module['module_id'] ); ?>">
  <?php if( ! empty( $module['title'] ) ): ?>
    <h4 class="title" data-mh="sc-module-title">
      <?php echo esc_html( $module['title'] ); ?>
    </h4>
    <div class="inner">
      <div class="sections">
        <?php

        echo SC_Social_Feed_Module::get_section_labels_markup();
        echo SC_Social_Feed_Module::get_section_panels_markup();

        ?>
      </div>
    </div>
  <?php endif;?>
</section>
<script type="text/javascript">

  (function($) {

    var _moduleId = '#<?php echo esc_js( $module['module_id'] ); ?>';
    var _module;

    $(document).ready(function() {

      _module = $(_moduleId);

      $(window).resize(_.throttle(onResize, 100));

      updateHeight();
			initTabEvents();
			
			setInterval( updateHeight, 1000 );

    });

    $(window).load(function() {

      updateHeight();

    });

    function onResize() {

      updateHeight();

    }

    function updateHeight() {

			if( ! sc.mediaQueries.match('md_up')) {

				return null;

			}

			var thisHeight    = _module.find('.inner').height();
			var siblingHeight = 0;

			_module.closest('.sc-module-container').find('.sc-module:not(' + _moduleId + ') .inner').each(function(i, sibling) {

				sibling       = $(sibling);
				siblingHeight = sibling.outerHeight();

				if( thisHeight != siblingHeight ) {

					_module.find('.inner').height(siblingHeight - 10);
				
				}


			});

		}

    function initTabEvents() {

      _module.find('.section-label a').click(function(event) {

        event.preventDefault();

        selectTab( $(this).parent().data('index') );

      });

    }

    function selectTab(index) {

      _module.find('.section-label').attr('data-active', 'no');
      _module.find('.section-panel').attr('data-active', 'no');

      _module.find('.section-label[data-index="' + index + '"]').attr('data-active', 'yes');
      _module.find('.section-panel[data-index="' + index + '"]').attr('data-active', 'yes');

    }

  })(jQuery);

</script>