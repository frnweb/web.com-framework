<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

if ( ! class_exists( 'SC_Hero_Module' ) ) {

	class SC_Hero_Module {

		private static $_data;
		private static $_slides;
		private static $_callouts;

		public static function get_slides() {

			if ( is_array( self::$_slides ) ) {

				return self::$_slides;

			}

			self::$_slides = array();

			if ( is_array( self::$_data['slides'] ) ) {

				foreach ( self::$_data['slides'] as $slide ) {

					$title_line_1      = $slide['title']['line_1'];
					$title_line_2      = $slide['title']['line_2'];
					$description_intro = $slide['description']['intro'];
					$description_body  = $slide['description']['body'];
					$text_alignment	   = $slide['text_alignment'];

					self::$_slides[] = (object) array(
						'title_line_1'      => $title_line_1,
						'title_line_2'      => $title_line_2,
						'description_intro' => $description_intro,
						'description_body'  => $description_body,
						'buttons'           => self::get_buttons( $slide ),
						'images'            => self::get_images( $slide ),
						'text_alignment'	=> $text_alignment
					);

				}
			}

			return self::$_slides;

		}

		public static function get_buttons( $slide ) {

			$buttons = array();

			foreach ( $slide['buttons'] as $button ) {

				if ( empty( $button['button_text'] ) ) {

					continue;

				}

				if ( empty( $button['button_link'] ) ) {

					 continue;

				}

				$buttons[] = (object) array(
					'text' => $button['button_text'],
					'link' => $button['button_link'],
				);

			}

			return $buttons;

		}

		public static function get_images( $slide ) {
			$sizes       = array( 'hg', 'lg', 'xl', 'md', 'sm', 'xs' );
			$sizes_total = count( $sizes );
			$images      = (object) array();
			$image       = false;
			$valign      = 'center';
			$halign      = 'center';

			// Loop over each size and check if it has an image or not. If it does
			// then break out of the loop and use that image as our default. In
			// addition if alignment options for the image are set then use them as
			// defaults.
			for ( $c = 0; $c < $sizes_total; $c++ ) {

				if ( ! empty( $slide[ $size . '_image' ] ) ) {

					$image = $slide[ $size . '_image' ];

					break;

				}

				if ( ! empty( $slide[ $size . '_alignment' ]['vertical'] ) ) {

					$valign = $slide[ $size . '_alignment' ]['vertical'];

				}

				if ( ! empty( $slide[ $size . '_alignment' ]['horizontal'] ) ) {

					 $halign = $slide[ $size . '_alignment' ]['horizontal'];

				}

			}

			for ( $c = 0; $c < $sizes_total; $c++ ) {

				$size  = $sizes[ $c ];

				if ( ! empty( $slide[ $size . '_image' ] ) ) {

					$image = $slide[ $size . '_image' ];

				}

				if ( ! empty( $slide[ $size . '_alignment' ]['vertical'] ) ) {

					$valign = $slide[ $size . '_alignment' ]['vertical'];

				}

				if ( ! empty( $slide[ $size . '_alignment' ]['horizontal'] ) ) {

					 $halign = $slide[ $size . '_alignment' ]['horizontal'];

				}

				$images->$size = (object) array(
					'image'  => $image ? $image['sizes'][ 'hero-' . $size . '-up' ] : '',
					'valign' => $valign,
					'halign' => $halign,
				);

			}

			return $images;

		}

		public static function get_callouts() {

			if ( is_array( self::$_callouts ) ) {

				return self::$_callouts;

			}

			self::$_callouts = array();

			if ( ! is_array( self::$_data['callouts'] ) ) {

				return self::$_callouts;

			}

			foreach ( self::$_data['callouts'] as $callout ) {

				$title        = trim( isset( $callout['title']       ) ? $callout['title']       : '' );
				$description  = trim( isset( $callout['description'] ) ? $callout['description'] : '' );
				$link         = trim( isset( $callout['link']        ) ? $callout['link']        : '' );
				$link_target  = ($callout['link_target']) ? '_blank' 		 	: '_self'  ;
				$icon         = trim( isset( $callout['icon']        ) ? $callout['icon']        : '' );

				if ( ! empty( $title ) && ! empty( $description ) && ! empty( $link ) ) {

					self::$_callouts[] = (object) array(
						'title'       => $title,
						'description' => $description,
						'link'        => $link,
						'link_target' => $link_target,
						'icon'        => $icon
					);

				}
			}

			return self::$_callouts;

		}

		public static function set_data( $data ) {
			self::$_data     = $data;
			self::$_slides   = null;
			self::$_callouts = null;

		}

	}

}

SC_Hero_Module::set_data( $module );

?>
<?php if ( count( SC_Hero_Module::get_slides() ) ) : ?>
	<section 
		class="sc-module sc-module-hero" 
		id="<?php echo esc_attr( $module['module_id'] ); ?>">
		<div class="slick-slider">
			<?php foreach ( SC_Hero_Module::get_slides() as $slide ) : ?>
				<div 
					class="slick-slide"
					data-image-xs-up="<?php echo esc_attr( $slide->images->xs->image ); ?>"
					data-image-sm-up="<?php echo esc_attr( $slide->images->sm->image ); ?>"
					data-image-md-up="<?php echo esc_attr( $slide->images->md->image ); ?>"
					data-image-lg-up="<?php echo esc_attr( $slide->images->lg->image ); ?>"
					data-image-xl-up="<?php echo esc_attr( $slide->images->xl->image ); ?>"
					data-image-hg-up="<?php echo esc_attr( $slide->images->hg->image ); ?>"
					data-valign-xs-up="<?php echo esc_attr( $slide->images->xs->valign ); ?>"
					data-valign-sm-up="<?php echo esc_attr( $slide->images->sm->valign ); ?>"
					data-valign-md-up="<?php echo esc_attr( $slide->images->md->valign ); ?>"
					data-valign-lg-up="<?php echo esc_attr( $slide->images->lg->valign ); ?>"
					data-valign-xl-up="<?php echo esc_attr( $slide->images->xl->valign ); ?>"
					data-valign-hg-up="<?php echo esc_attr( $slide->images->hg->valign ); ?>"
					data-halign-xs-up="<?php echo esc_attr( $slide->images->xs->halign ); ?>"
					data-halign-sm-up="<?php echo esc_attr( $slide->images->sm->halign ); ?>"
					data-halign-md-up="<?php echo esc_attr( $slide->images->md->halign ); ?>"
					data-halign-lg-up="<?php echo esc_attr( $slide->images->lg->halign ); ?>"
					data-halign-xl-up="<?php echo esc_attr( $slide->images->xl->halign ); ?>"
					data-halign-hg-up="<?php echo esc_attr( $slide->images->hg->halign ); ?>">
					<div class="container">
						<div class="row">
							<div class="col-xs-12<?php if($slide->text_alignment !== 'left'){echo ' '.$slide->text_alignment;} ?>">
								<div class="inner">
									<?php

									if ( ! empty( $slide->title_line_1 ) || ! empty( $slide->title_line_2 ) ) {

										echo '<div class="title">';

										if ( ! empty( $slide->title_line_1 ) ) {

											printf(
												'
												<h1 class="title-line-1">
													<span>%s</span>
												</h1>',
												esc_html( $slide->title_line_1 )
											);

										}

										if ( ! empty( $slide->title_line_2 ) ) {

											 printf(
												 '
												<div class="title-line-2">
													<span>%s</span>
												</div>',
												 wp_kses( 
													 $slide->title_line_2,
													 array(
														'a' => array(
															'href'   => array(),
															'title'  => array(),
															'target' => array()
														)
													)
												)
											 );

										}

										echo '</div>';

									}

									if ( ! empty( $slide->description_intro ) || ! empty( $slide->description_body ) ) {

										 echo '<div class="description">';

										if ( ! empty( $slide->description_intro ) ) {

											printf(
												'<div class="description-intro">
													<span>%s</span>
												</div>',
												esc_html( $slide->description_intro )
											);

										}

										if ( ! empty( $slide->description_body ) ) {

											printf(
												'<div class="description-body">
													<span>%s</span>
												</div>',
												esc_html( $slide->description_body )
											);

										}

										echo '</div>';

									}

									if ( count( $slide->buttons ) ) {

										echo '<div class="buttons">';

										$count = 1;

										foreach ( $slide->buttons as $button ) {

											printf(
												'<a href="%s" class="button-%d">%s</a>',
												esc_url( $button->link ),
												esc_attr( $count ),
												esc_html( $button->text )
											);

											$count++;

										}

										echo '</div>';

									}

									?>
								</div>
							</div>
						</div>
					</div>
					<div class="background"></div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if ( count( SC_Hero_Module::get_callouts() ) ) : ?>
			<div class="callouts callouts-total-<?php echo count( SC_Hero_Module::get_callouts() ); ?>">
				<div class="container">
					<div class="row">
						<?php

						$column_sizes = array(
							1 => array( 12 ),
							2 => array( 6, 6 ),
							3 => array( 4, 4, 4 ),
							4 => array( 6, 6, 6, 6 ),
							5 => array( 4, 4, 4, 4, 4 ),
							6 => array( 4, 4, 4, 4, 4, 4 ),
						);

						$callouts = SC_Hero_Module::get_callouts();
						$callouts_total = count( $callouts );

						for ( $c = 0; $c < $callouts_total; $c++ ) {

							$callout    = $callouts[ $c ];
							$icon_class = 'diamond';

							if( ! empty( $callout->icon ) ) {

								$icon_class = 'fa ' . $callout->icon;

							}

							printf(
								'<div class="col-xs-12 col-md-%d">
									<div class="callout">
										<div class="icon %s" aria-hidden="true"></div>
										<h6 class="title">%s</h6>
										<p class="description">%s</p>
										<a href="%s" target="%s" class="link" title="%s">Read More</a>
									</div>
								</div>',
								esc_attr( $column_sizes[ $callouts_total ][ $c ] ),
								esc_attr( $icon_class ),
								esc_html( $callout->title ),
								esc_html( $callout->description ),
								esc_url( $callout->link ),
								esc_html($callout->link_target),
								esc_attr( $callout->title )
							);

						}

						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
	<script type="text/javascript">

		(function($) {

			var module = $('#<?php echo esc_js( $module['module_id'] ); ?>');

			$(document).ready(function() {

				module.find('.slick-slider').slick({

					'dots'          : true,
					'arrows'        : false,
					'fade'          : true,
					'infinite'      : true,
					'autoplay'      : true,
					'autoplaySpeed' : 10000,
					'speed'         : 300,
					'accessibility' : true

				});

				update(0);
				updateCallouts();
					
				module.find('.callout').click(function(e) {
					var link = $(this).find('a');
					if(link.attr('target') === '_blank'){
						window.open(link.attr('href'));
					}else{
						window.location = $(this).find('a').attr('href');
					}

				});

				$(window).resize( _.throttle( onResize, 250 ) );

			});

			$(window).load(function() {

				updateAll();
				updateCallouts();

			});

			function onResize() {

				updateAll();
				updateCallouts();

			}

			function update( index ) {

				var image      = getMobileFirstProperty( 'image', index );
				var valign     = getMobileFirstProperty( 'valign', index );
				var halign     = getMobileFirstProperty( 'halign', index );
				var background = getSlide( index ).find( '.background' );

				if( image != '' ) {

					background
						.css( 'background-image', 'url(' + image + ')' )
						.css( 'background-position', halign + ' ' + valign );

				}

			}

			function updateAll() {

				module.find('.slick-slide').each(function( index, slide ) {

					update( index );

				});

			}

			function updateCallouts() {

				module.find('.callouts').css('opacity', 1);
				
				if( ! sc.mediaQueries.match('md_up') ) {

					module.find('.callouts').css( 'margin-top', '0' );
					module.find('.slick-slide').css( 'padding-bottom', '0' );

					return false;

				}

				module.find('.callout').matchHeight({});

				var totalCallouts  = module.find('.callout').length;
				var verticalOffset = 0;

				if( totalCallouts > 3) {

					verticalOffset = Math.round( module.find('.callouts').height() / 2 );

				} else {

					verticalOffset = module.find('.callout:first-child').height();

				}

				module.find('.callouts').css( 'margin-top', '-' + verticalOffset + 'px' );
				module.find('.slick-slide').css( 'padding-bottom', verticalOffset + 'px' );

			}

			function getMobileFirstProperty( property, index ) {

				var sizes = [ 'xs', 'sm', 'md', 'lg', 'xl', 'hg' ];
				var slide = getSlide( index );
				var value = slide.data( property + '-xs-up' );

				_.each( sizes, function(size) {

					if( sc.mediaQueries.match( size + '_up') ) {

						value = slide.data( property + '-' + size + '-up' );

					}

				});

				return value;

			}

			function getSlide( index ) {

				return module.find('.slick-slide:nth-child(' + ( index + 1 ) + ')');

			}

		})(jQuery);

	</script>
<?php endif; ?>
