<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and
 * make your changes there. Failure to do this will result in changes being
 * overwritten by an automatic update in the future.
 */
require_once( 'core/sc-customizer-stylesheet.php' );
require_once( 'core/sc-customizer-stylesheet-ovveride.php' );
require_once( 'customizers/branding-and-colors.php' );
require_once( 'customizers/header-and-footer.php' );
require_once( 'customizers/social-media.php' );
require_once( 'customizers/tracking-ids.php' );
require_once( 'customizers/constant-contact.php' );
require_once( 'customizers/settings.php' );

SC_Customizer_Stylesheet::singleton();
SC_Customizer_Stylesheet_Ovveride::singleton();

function sc_init_customizer( $wp_customize ) {

  if( class_exists('Kirki') ) {

    Kirki::add_config( 'uhs-modular-framework', array(
      'capability'  => 'edit_theme_options',
      'option_type' => 'theme_mod',
    ) );

  }

  SC_Customizer::enqueue_customizer( 'header-and-footer' );
  SC_Customizer::enqueue_customizer( 'branding-and-colors' );
  SC_Customizer::enqueue_customizer( 'social-media' );
  SC_Customizer::enqueue_customizer( 'tracking-ids' );
  SC_Customizer::enqueue_customizer( 'constant-contact' );
  SC_Customizer::enqueue_customizer( 'settings' );

  SC_Customizer::process_queue( $wp_customize );

  SC_Customizer::remove_defaults(
    $wp_customize,
    array(
      'themes',
      'colors',
      'header_image',
      'background_image',
      'static_front_page'
    )
  );

}
