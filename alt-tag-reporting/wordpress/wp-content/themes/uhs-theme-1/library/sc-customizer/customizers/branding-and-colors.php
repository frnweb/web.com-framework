<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and
 * make your changes there. Failure to do this will result in changes being
 * overwritten by an automatic update in the future.
 */
if( ! class_exists( 'SC_Customizer_Branding_And_Colors' ) ) {

	class SC_Customizer_Branding_And_Colors {

		public function register( $wp_customize, $priority ) {

			self::register_section( $wp_customize, $priority );

		}

		public function register_section( $wp_customize, $priority ) {

			$section = 'sc_branding_section';

			$wp_customize->add_section($section , array(
				'title'    => 'Branding & Colors',
				'priority' => $priority
			));

			// Core Colors
			// -----------------------------------------------------------------------

			SC_Customizer::register_image(
				$wp_customize,
				$section,
				'sc_header_logo',
				'Header Logo'
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_primary_color',
				'Primary Color',
				array(
					'default' => '#351a52'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_secondary_color',
				'Secondary Color',
				array(
					'default' => '#be3932'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_tertiary_color',
				'Tertiary Color',
				array(
					'default' => '#f4504c'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_text_color',
				'Text Color',
				array(
					'default' => '#33333c'
				)
			);

			// Main Navigation Override Colors
			// -----------------------------------------------------------------------

			SC_Customizer::register_checkbox(
				$wp_customize,
				$section,
				'sc_ovveride_main_navigation_styles',
				'Override Main Navigation Styles',
				array(
					'setting_options' => array(
						'transport' => 'refresh'
					)
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_main_navigation_background_color',
				'Main Navigation Background Color',
				array(
					'default' => '#000000'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_main_navigation_text_color',
				'Main Navigation Text Color',
				array(
					'default' => '#000000'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_main_navigation_divider_color',
				'Main Navigation Divider Color',
				array(
					'default' => '#000000'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_main_navigation_icon_color',
				'Main Navigation Icon Color',
				array(
					'default' => '#000000'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_main_navigation_hover_decoration_color',
				'Main Navigation Hover Decoration Color',
				array(
					'default' => '#000000'
				)
			);

			// Hero Callout Override Colors
			// -----------------------------------------------------------------------

			SC_Customizer::register_checkbox(
				$wp_customize,
				$section,
				'sc_ovveride_hero_styles',
				'Override Hero Styles'
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_hero_callout_background_color_odd',
				'Hero Callout Background Color (Odd)',
				array(
					'default' => '#000000'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_hero_callout_background_color_even',
				'Hero Callout Background Color (Even)',
				array(
					'default' => '#000000'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_hero_callout_text_color',
				'Hero Callout Text Color',
				array(
					'default' => '#000000'
				)
			);

			SC_Customizer::register_color(
				$wp_customize,
				$section,
				'sc_hero_callout_icon_color',
				'Hero Callout Icon Color',
				array(
					'default' => '#000000'
				)
			);

			// Typography
			// -----------------------------------------------------------------------

			SC_Customizer::register_radio(
				$wp_customize,
				$section,
				'sc_font_scheme',
				'Font Scheme',
				array(
					'font_scheme_1' => 'Font Scheme 1',
					'font_scheme_2' => 'Font Scheme 2',
					'font_scheme_3' => 'Font Scheme 3'
				),
				array(
					'setting_options' => array(
						'default'   => 'font_scheme_1',
						'transport' => 'refresh'
					)
				)
			);

			// Default Images
			// -----------------------------------------------------------------------

			SC_Customizer::register_cropped_image(
				$wp_customize,
				$section,
				'sc_default_interior_banner',
				'Default Interior Banner',
				array (
					'width'       => 1920,
					'height'      => 288,
					'flex_width'  => false,
					'flex_height' => false
				)
			);

			SC_Customizer::register_cropped_image(
				$wp_customize,
				$section,
				'sc_default_featured_image',
				'Default Featured Image',
				array (
					'width'       => 260,
					'height'      => 260,
					'flex_width'  => false,
					'flex_height' => false
				)
			);

		}

	}

}
