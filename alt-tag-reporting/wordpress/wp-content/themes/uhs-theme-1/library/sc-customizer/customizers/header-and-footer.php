<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */
if( ! class_exists( 'SC_Customizer_Header_And_Footer' ) ) {
	
	class SC_Customizer_Header_And_Footer {

		public function register( $wp_customize, $priority ) {

			self::register_section( $wp_customize, $priority );

		}

		public function register_section( $wp_customize, $priority ) {

			$section = 'sc_header_section';

			$wp_customize->add_section($section , array(
				'title'    => 'Header & Footer',
				'priority' => $priority
			));

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_header_phone_number',
				'(Header) Phone Number'
			);

			SC_Customizer::register_textarea(
				$wp_customize,
				$section,
				'sc_disclaimer',
				'(Footer) Disclaimer'
			);

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_copyright',
				'(Footer) Copyright'
			);

		}

	}

}

