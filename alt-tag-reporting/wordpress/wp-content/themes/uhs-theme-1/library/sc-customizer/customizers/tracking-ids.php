<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */
if( ! class_exists( 'SC_Customizer_Tracking_Ids' ) ) {
	
	class SC_Customizer_Tracking_Ids {

		public function register( $wp_customize, $priority ) {

			self::register_section( $wp_customize, $priority );

		}

		public function register_section( $wp_customize, $priority ) {

			$section = 'sc_tracking_ids';

			$wp_customize->add_section($section , array(
				'title'    => 'Tracking IDs',
				'priority' => $priority
			));

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_google_tag_manager_id',
				'Google Tag Manager Id'
			);

			if( class_exists('Kirki') ) {

				Kirki::add_field( 'uhs-modular-framework', [
					'type'        => 'repeater',
					'label'       => esc_html__( 'YoTrack', 'kirki' ),
					'section'     => $section,
					'priority'    => $priority,
					'row_label' => [
						'type'  => 'text',
						'value' => esc_html__('Tracking Settings', 'kirki' ),
					],
					'button_label' => esc_html__('Add New Tracking Line', 'kirki' ),
					'settings'     => 'sc_yotrack',
					'default'      => [
					],
					'fields' => [
						'sc_tracking_cid' => [
							'type'        => 'number',
							'label'       => esc_html__( 'Centermark ID', 'kirki' ),
							'default'     => '',
						],
						'sc_tracking_phone'  => [
							'type'        => 'text',
							'label'       => esc_html__( 'Phone Number', 'kirki' ),
							'default'     => '',
						],
					]
				] );

			}

		}

	}

}

