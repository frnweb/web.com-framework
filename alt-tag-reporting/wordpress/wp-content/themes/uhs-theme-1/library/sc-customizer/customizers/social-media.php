<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */
if( ! class_exists( 'SC_Customizer_Social_Media' ) ) {

	class SC_Customizer_Social_Media {

		public function register( $wp_customize, $priority ) {

			self::register_section( $wp_customize, $priority );

		}

		public function register_section( $wp_customize, $priority ) {

			$section = 'sc_social_media_section';

			$wp_customize->add_section($section , array(
				'title'    => 'Social Media & Sharing',
				'priority' => $priority
			));

			SC_Customizer::register_textarea(
				$wp_customize,
				$section,
				'sc_sharethis_code',
				'ShareThis Code',
				array (
          'setting_options' => array(
            'sanitize_callback' => null
          )
        )
			);

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_facebook_url',
				'Facebook URL'
			);

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_twitter_url',
				'Twitter URL'
			);

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_googleplus_url',
				'Google+ URL'
			);

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_linkedin_url',
				'LinkedIn URL'
			);

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_instagram_url',
				'Instagram URL'
			);

			SC_Customizer::register_text(
				$wp_customize,
				$section,
				'sc_youtube_url',
				'YouTube URL'
			);

		}

	}

}
