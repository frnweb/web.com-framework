<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and
 * make your changes there. Failure to do this will result in changes being
 * overwritten by an automatic update in the future.
 */
if( ! class_exists('SC_Customizer_Stylesheet_Ovveride') ) {

	/**
	 * Documentation please
	 */
	class SC_Customizer_Stylesheet_Ovveride {

		CONST CSS_OVERRIDE_PREVIEW    = 'sc-css-override-preview';
		CONST CSS_OVERRIDE_PRODUCTION = 'sc-css-override-production';

		protected static $_instance = null;

		function __construct() {

			add_action( 'customize_save_after', array( $this, 'customize_save_after' ), 99 );

		}

		/**
		 * Get the SC_Customizer_Stylesheet_Ovveride singleton
		 */
		public static function singleton() {

			if( null == self::$_instance ) {

				self::$_instance = new self;

			}

			return self::$_instance;

		}

		public function customize_save_after() {

			$this->generate_css_override();

		}

		/**
		 * Generate the CSS override.
		 */
		public function generate_css_override( $preview = false ) {

      $css  = $this->get_main_navigation_override();
      $css .= $this->get_hero_override();
      $css .= $this->get_global_font_scheme_override();

      set_theme_mod( $preview ? self::CSS_OVERRIDE_PREVIEW : self::CSS_OVERRIDE_PRODUCTION, $css );

		}

    private function get_main_navigation_override() {

      $ovveride               = get_theme_mod( 'sc_ovveride_main_navigation_styles',  false );
      $background_color       = get_theme_mod( 'sc_main_navigation_background_color', '#000000' );
      $text_color             = get_theme_mod( 'sc_main_navigation_text_color', '#000000' );
      $divider_color          = get_theme_mod( 'sc_main_navigation_divider_color', '#000000' );
      $icon_color             = get_theme_mod( 'sc_main_navigation_icon_color', '#000000' );
      $hover_decoration_color = get_theme_mod( 'sc_main_navigation_hover_decoration_color', '#000000' );

      if( false == $ovveride ) {
        return '';
      }

      $properties = array(
        '%background_color'       => $background_color,
        '%text_color'             => $text_color,
        '%divider_color'          => $divider_color,
        '%icon_color'             => $icon_color,
				'%hover_decoration_color' => $hover_decoration_color,
      );

      $css = '
        #header .header-bottom,
        #mobile-navigation .inner {
          background: %background_color;
        }
        #header .navigation .menu>.menu-item>a,
        #header .phone-link,
        #mobile-navigation .slider ul.sticky-links a,
        #mobile-navigation .sc-mobile-menu-link,
        #mobile-navigation .sc-mobile-menu-back-button {
          color: %text_color;
        }
        #header .navigation .menu>.menu-item>a::before {
          border-left: 1px solid %divider_color;
        }
        #header .phone-link .fa,
        #mobile-navigation .slider ul.sticky-links a .fa {
          color: %icon_color;
        }
        #header .navigation .menu>.menu-item:hover>a::after {
          border-bottom-color: %hover_decoration_color;
        }
        #header .navigation .mega-menu,
        #header .navigation .mega-menu::before,
        #header .navigation .mega-menu::after {
          border-top-color: %hover_decoration_color;
        }
        #mobile-navigation .sc-mobile-menu-close-button::before, 
        #mobile-navigation .sc-mobile-menu-close-button::after {
          background: %text_color;
        }
        #mobile-navigation .sc-mobile-menu-back-button.arrow-link:before, 
        #mobile-navigation .sc-mobile-menu-back-button.arrow-link:after {
          background: %icon_color;
        }
        #mobile-navigation .sc-mobile-menu-drill-down::after {
          border-left-color: %text_color;
        }
        #mobile-navigation .slider ul>li {
          border-bottom-color: %divider_color;
        }
      ';

      return str_replace(
        array_keys( $properties ), 
        array_values( $properties ), 
        $css
      );

    }

    private function get_hero_override() {

      $ovveride              = get_theme_mod( 'sc_ovveride_hero_styles',  false );
      $background_color_odd  = get_theme_mod( 'sc_hero_callout_background_color_odd', '#000000' );
      $background_color_even = get_theme_mod( 'sc_hero_callout_background_color_even', '#000000' );
      $text_color            = get_theme_mod( 'sc_hero_callout_text_color', '#000000' );
      $icon_color            = get_theme_mod( 'sc_hero_callout_icon_color', '#000000' );

      if( false == $ovveride ) {
        return '';
      }

      return sprintf('
        .sc-module-hero .callout {
          background: %1$s;
        }
        .sc-module-hero .callouts .col-xs-12:nth-child(even) .callout {
          background: %2$s;
        }
        .sc-module-hero .callout .title,
        .sc-module-hero .callout .description {
          color: %3$s;
        }
        .sc-module-hero .callout .icon {
					color: %4$s;
				}
				.sc-module-hero .callout .icon.diamond::before {
					border-bottom-color: %4$s;
				}
				.sc-module-hero .callout .icon.diamond::after {
					border-top-color: %4$s;
				}
				@media only screen and (min-width: 992px) {
					.sc-module-hero .callouts-total-4 .col-xs-12:nth-child(1) .callout, 
					.sc-module-hero .callouts-total-4 .col-xs-12:nth-child(4) .callout {
						background: %1$s;
					}
					.sc-module-hero .callouts-total-4 .col-xs-12:nth-child(2) .callout, 
					.sc-module-hero .callouts-total-4 .col-xs-12:nth-child(3) .callout {
						background: %2$s;
					}
				}
				',
        $background_color_odd,
        $background_color_even,
        $text_color,
        $icon_color
      );

    }

    public function get_global_font_scheme_override() {

      $font_scheme = get_theme_mod( 'sc_font_scheme' );

      switch( $font_scheme ) {

        case 'font_scheme_2': return $this->get_global_font_scheme_override_2();
        case 'font_scheme_3': return $this->get_global_font_scheme_override_3();

      }

      return '';

    }

    public function get_global_font_scheme_override_2() {
      
      $nunito     = "'Nunito',helvetica,arial,sans-serif";//400,400i,800
      $montserrat = "'Montserrat',helvetica,arial,sans-serif";//400,500,600,700

      return sprintf('
        html body {
          font-family: %1$s;
        }
        h1 {
          font-family: %2$s;
          font-weight: 700;
        }
        h2 {
          font-family: %2$s;
          font-weight: 500;
        }
        h3 {
          font-family: %2$s;
          font-weight: 600;
        }
        h4 {
          font-family: %1$s;
          font-weight: 400;
        }
        h5 {
          font-family: %2$s;
          font-weight: 600;
        }
        h6 {
          font-family: %1$s;
          font-weight: 800;
        }
        .button, 
        .sc-module-form div.gform_wrapper .gform_footer input.button, 
        .sc-module-form div.gform_wrapper .gform_footer input[type="submit"], 
        .sc-module-hero .slick-slider .button-1, 
        .sc-module-hero .slick-slider .button-2, 
        #page-content .sc-module-subscribe .ctct-form-button {
          font-family: %1$s;
          font-size: 13px;
          font-weight: 800;
        }
        #page-banner .title {
          font-family: %2$s;
          font-weight: 400;
        }
        .widget-sc-widget-sidebar-nav h4 {
          font-family: %2$s;
          font-weight: 500;
        }
        #page-sidebar .menu, 
        #page-sidebar .sub-menu {
          font-family: %1$s;
        }
        #page-sidebar .menu-item[data-level="2"]>a {
          font-weight: 400;
        }
        #header .small-navigation .menu .menu-item a {
          font-family: %1$s;
          font-weight: 800;
        }
        #header .navigation .menu>.menu-item>a {
          font-family: %2$s;
          font-weight: 600;
        }
        #header .navigation .mega-menu .sub-menu>.menu-item a[data-depth="2"] {
          font-family: %2$s;
          font-weight: 600;
        }
        #header .navigation .mega-menu .sub-menu>.menu-item a[data-depth="3"] {
          font-family: %2$s;
          font-weight: 400;
        }
        #header .navigation .mega-menu .sub-menu>.menu-item a[data-depth="4"] {
          font-family: %2$s;
          font-weight: 400;
        }
        #header .phone-link {
          font-family: %1$s;
          font-weight: 800;
        }
        .sc-module-hero .slick-slider .title-line-1 {
          font-family: %2$s;
          font-weight: 400;
        }
        .sc-module-hero .slick-slider .title-line-2 {
          font-family: %2$s;
          font-weight: 700;
        }
        .sc-module-hero .slick-slider .description-intro {
          font-family: %2$s;
          font-weight: 600;
        }
        .sc-module-hero .slick-slider .description-body {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-hero .callout .title {
          font-family: %2$s;
          font-weight: 600;
        }
        .sc-module-hero .callout .description {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-tabs h4,
        .sc-module-stories h4,
        .sc-module-video h4,
        .sc-module-gallery h4,
        .sc-module-social-feed h4,
        .sc-module-subscribe h4,
        .sc-module-banner h4 {
          font-family: %2$s;
          font-weight: 600;
        }
        .sc-module-tabs .section-labels .section-label a,
        .sc-module-accordion .section-label a,
        .sc-module-social-feed .section-label a {
          font-family: %2$s;
          font-weight: 600;
        }
        .sc-module-stories .story-info .story-headline,
        .sc-module-video .headline,
        .sc-module-subscribe .headline {
          font-family: %2$s;
          font-weight: 400;
        }
        .sc-module-stories .story-info .story-content {
          font-family: %1$s;
          font-style: italic;
          font-weight: 400;
        }
        .sc-module-stories .story-info .story-name {
          font-family: %2$s;
          font-weight: 600;
        }
        .sc-module-stories .story-info .story-relation {
          font-family: %2$s;
          font-weight: 400;
        }
        .sc-module-banner .banner-content-wrapper p {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-card .card-content h5 {
          font-family: %2$s;
          font-weight: 600;
        }
        .sc-module-card .card-content p {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-subscribe .description {
          font-family: %1$s;
          font-style: italic;
          font-weight: 400;
        }
        #cff .cff-author a, 
        #cff-lightbox-wrapper .cff-author a {
          font-family: %1$s;
          font-weight: 800;
        }
        .sc-module-location .location-info .details .title {
          font-family: %2$s;
          font-weight: 600;
        }
        .sc-module-location .location-info .details .address,
        .sc-module-location .location-info .details .phone_numbers,
        .sc-module-location .location-info .details .phone_numbers .phone_number a {
          font-family: %1$s;
          font-weight: 400;
        }
        .arrow-link {
          font-family: %1$s;
          font-weight: 800;
        }
        #mobile-navigation .slider ul.sticky-links a,
        #mobile-navigation .sc-mobile-menu-link {
          font-family: %1$s;
        }',
        $nunito,
        $montserrat
      ); 

    }

    public function get_global_font_scheme_override_3() {

      $dosis = "'Dosis',helvetica,arial,sans-serif";//400,500,600,700
      $libre = "'Libre Baskerville',helvetica,arial,sans-serif";//400,400i,700

      return sprintf('
        html body {
          font-family: %1$s;
        }
        h1 {
          font-family: %2$s;
          font-weight: 700;
        }
        h2 {
          font-family: %1$s;
          font-weight: 500;
        }
        h3 {
          font-family: %2$s;
          font-style: italic;
          font-weight: 400;
        }
        h4 {
          font-family: %1$s;
          font-weight: 500;
        }
        h5 {
          font-family: %1$s;
          font-weight: 500;
        }
        h6 {
          font-family: %1$s;
          font-weight: 500;
        }
        .button, 
        .sc-module-form div.gform_wrapper .gform_footer input.button, 
        .sc-module-form div.gform_wrapper .gform_footer input[type="submit"], 
        .sc-module-hero .slick-slider .button-1, 
        .sc-module-hero .slick-slider .button-2, 
        #page-content .sc-module-subscribe .ctct-form-button {
          font-family: %1$s;
          font-size: 14px;
          font-weight: 500;
        }
        #page-banner .title {
          font-family: %1$s;
          font-weight: 400;
        }
        .widget-sc-widget-sidebar-nav h4 {
          font-family: %2$s;
          font-style: italic;
          font-weight: 400;
        }
        #page-sidebar .menu, 
        #page-sidebar .sub-menu {
          font-family: %1$s;
        }
        #page-sidebar .menu-item[data-level="2"]>a {
          font-weight: 400;
        }
        #header .small-navigation .menu .menu-item a {
          font-family: %1$s;
          font-weight: 500;
        }
        #header .navigation .menu>.menu-item>a {
          font-family: %1$s;
          font-weight: 500;
        }
        #header .navigation .mega-menu .sub-menu>.menu-item a[data-depth="2"] {
          font-family: %1$s;
          font-weight: 500;
        }
        #header .navigation .mega-menu .sub-menu>.menu-item a[data-depth="3"] {
          font-family: %1$s;
          font-weight: 400;
        }
        #header .navigation .mega-menu .sub-menu>.menu-item a[data-depth="4"] {
          font-family: %1$s;
          font-weight: 400;
        }
        #header .phone-link {
          font-family: %2$s;
          font-weight: 700;
        }
        .sc-module-hero .slick-slider .title-line-1 {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-hero .slick-slider .title-line-2 {
          font-family: %2$s;
          font-size: 45px;
          font-weight: 700;
          line-height: 45px;
        }
        .sc-module-hero .slick-slider .description-intro {
          font-family: %2$s;
          font-style: italic;
          font-weight: 400;
        }
        .sc-module-hero .slick-slider .description-body {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-hero .callout .title {
          font-family: %2$s;
          font-size: 18px;
          font-weight: 400;
        }
        .sc-module-hero .callout .description {
          font-family: %1$s;
          font-size: 16px;
        }
        .sc-module-tabs h4,
        .sc-module-stories h4,
        .sc-module-video h4,
        .sc-module-gallery h4,
        .sc-module-social-feed h4,
        .sc-module-subscribe h4,
        .sc-module-banner h4 {
          font-family: %2$s;
          font-size: 21px;
          font-style: italic;
          font-weight: 400;
        }
        .sc-module-tabs .section-labels .section-label a,
        .sc-module-accordion .section-label a,
        .sc-module-social-feed .section-label a {
          font-family: %1$s;
          font-weight: 500;
        }
        .sc-module-stories .story-info .story-headline,
        .sc-module-video .headline,
        .sc-module-subscribe .headline {
          font-family: %1$s;
          font-weight: 500;
        }
        .sc-module-stories .story-info .story-content {
          font-family: %1$s;
          font-style: initial;
          font-weight: 400;
        }
        .sc-module-stories .story-info .story-name {
          font-family: %1$s;
          font-weight: 600;
        }
        .sc-module-stories .story-info .story-relation {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-banner .banner-content-wrapper p {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-card .card-content h5 {
          font-family: %2$s;
          font-size: 17px;
          font-style: italic;
          font-weight: 400;
        }
        .sc-module-card .card-content p {
          font-family: %1$s;
          font-weight: 400;
        }
        .sc-module-subscribe .description {
          font-family: %1$s;
          font-style: initial;
          font-weight: 400;
        }
        #cff .cff-author a, 
        #cff-lightbox-wrapper .cff-author a {
          font-family: %1$s;
          font-weight: 700;
        }
        .sc-module-location .location-info .details .title {
          font-family: %2$s;
          font-size: 21px;
          font-style: italic;
          font-weight: 400;
        }
        .sc-module-location .location-info .details .address,
        .sc-module-location .location-info .details .phone_numbers,
        .sc-module-location .location-info .details .phone_numbers .phone_number a {
          font-family: %1$s;
          font-size: 18px;
          font-weight: 400;
        }
        .arrow-link {
          font-family: %1$s;
          font-size: 13px;
          font-weight: 700;
        }
        #mobile-navigation .slider ul.sticky-links a,
        #mobile-navigation .sc-mobile-menu-link {
          font-family: %1$s;
        }',
        $dosis,
        $libre
      ); 

    }

    public function get_css_ovverride( $preview = false ) {

      return get_theme_mod( $preview ? self::CSS_OVERRIDE_PREVIEW : self::CSS_OVERRIDE_PRODUCTION, '' );

    }

	}

}
