<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */
if( ! class_exists('SC_Customizer_Stylesheet') ) {

	/**
	 * Documentation please
	 */
	class SC_Customizer_Stylesheet {

		CONST ORIGINAL_STYLESHEET_MODIFIED_ON = 'sc-original-stylesheet-modified-on';
		CONST STYLESHEET_GENERATED_ON         = 'sc-stylesheet-generated-on';

		protected static $_instance    = null;
		protected static $_color_cache = array();

		function __construct() {

			if( $this->original_stylesheet_was_updated() ) {

				$this->generate_stylesheet();

			}

			add_action( 'customize_save_after', array( $this, 'customize_save_after' ), 99 );

		}

		/**
		 * Get the SC_Vertical singleton instance
		 */
		public static function singleton() {

			if( null == self::$_instance ) {

				self::$_instance = new self;

			}

			return self::$_instance;

		}

		public function customize_save_after() {

			$this->generate_stylesheet();

		}

		public function get_original_stylesheet_file() {

			$theme_directory = get_stylesheet_directory();

			return $theme_directory . '/style.css';

		}

		public function get_generated_stylesheet_file( $preview = false, $generate_new = false ) {

			$directory = $this->get_generated_stylesheet_dir();

			if( false === $directory ) {

				return false;

			}

			$generated_on = get_option( self::STYLESHEET_GENERATED_ON, '' );

			if( $generate_new && ! $preview ) {

				$generated_on = time();

				update_option( self::STYLESHEET_GENERATED_ON, $generated_on );

			}

			$filename = $preview ? 'style-preview.css' : 'style' . $generated_on . '.css';

			return $directory . '/' . $filename;

		}

		public function get_generated_stylesheet_file_url( $preview = false ) {

			$upload_dir = wp_upload_dir();

			if( ! isset( $upload_dir['baseurl'] ) ) {

				return false;
			
			}

			$generated_on = get_option( self::STYLESHEET_GENERATED_ON, '' );
			
			$filename = $preview ? 'style-preview.css' : 'style' . $generated_on . '.css';

			return $upload_dir['baseurl'] . '/sc-customizer-stylesheet/' . $filename;

		}

		public function get_generated_stylesheet_dir() {

			$upload_dir = wp_upload_dir();

			if( ! isset( $upload_dir['basedir'] ) ) {

				return false;
			
			}

			$directory = $upload_dir['basedir'] . '/sc-customizer-stylesheet';

			if( is_dir( $directory ) ) {

				return $directory;

			}

			if ( ! mkdir( $directory, 0775, true ) ) {

				return false;

			}

			return $directory;

		}

		/**
		 * Generate the stylesheet for a given vertical.
		 */
		public function generate_stylesheet( $preview = false ) {

			$original_stylesheet     = $this->get_original_stylesheet_file();
			$generated_stylesheet    = $this->get_generated_stylesheet_file( $preview, true );
			$replacement_color_pairs = $this->get_replacement_color_pairs();

			$css = file_get_contents( $original_stylesheet );

			foreach( $replacement_color_pairs as $original_color => $replacement_color ) {
				
				$css = str_replace( $original_color, $replacement_color, $css );
				
			}

			$css  = str_replace( 'assets/', get_stylesheet_directory_uri() . '/assets/', $css );

			$file = fopen( $generated_stylesheet, 'w' ) or die( 'Unable to open file!' );
			
			fwrite($file, $css);
			fclose($file);

			$modified_on_date = (int) filemtime( $original_stylesheet );
	
			update_option( self::ORIGINAL_STYLESHEET_MODIFIED_ON, $modified_on_date, true );

		}

		/*
		 * Checks the actual last modified time of the stylsheet file vs the last 
		 * record time of modification.
		 */
		public function original_stylesheet_was_updated() {

			$original_stylesheet = $this->get_original_stylesheet_file();

			if( file_exists( $original_stylesheet ) ) {

				$actual_modified_on_date = (int) filemtime( $original_stylesheet );
				$cached_modified_on_date = (int) get_option( self::ORIGINAL_STYLESHEET_MODIFIED_ON, 0 );

				if( $actual_modified_on_date != $cached_modified_on_date ) {

					return true;

				}

				return false;

			}

		}

		public function get_replacement_color_pairs() {

			$default_primary_color   = '#351A52';
			$default_secondary_color = '#BE3932';
			$default_tertiary_color  = '#F4504C';
			$default_text_color      = '#33333C';

			$user_primary_color   = get_theme_mod( 'sc_primary_color',   $default_primary_color );
			$user_secondary_color = get_theme_mod( 'sc_secondary_color', $default_secondary_color );
			$user_tertiary_color  = get_theme_mod( 'sc_tertiary_color',  $default_tertiary_color );
			$user_text_color      = get_theme_mod( 'sc_text_color',      $default_text_color );

			$default_and_user_colors_pairs = array(
				$default_primary_color   => $user_primary_color,
				$default_secondary_color => $user_secondary_color,
				$default_tertiary_color  => $user_tertiary_color,
				$default_text_color      => $user_text_color
			);

			$replacement_color_pairs = array();

			foreach ($default_and_user_colors_pairs as $default_color => $user_color) {

				for( $opacity = 100; $opacity > 0; $opacity -= 5 ) {

					$blended_default_color = $this->color_blend_by_opacity( $default_color, $opacity );
					$blended_user_color    = $this->color_blend_by_opacity( $user_color, $opacity );
					$darkened_default_color = $this->color_blend_by_opacity( $default_color, $opacity, '000000' );
					$darkened_user_color    = $this->color_blend_by_opacity( $user_color, $opacity, '000000' );
					$replacement_color_pairs[$blended_default_color] = $blended_user_color;
					$replacement_color_pairs[$darkened_default_color] = $darkened_user_color;
				}

			}

			return $replacement_color_pairs;

		}

		public function color_blend_by_opacity( $foreground, $opacity, $background = null ) {

			static $colors_rgb = array(); // stores colour values already passed through the hexdec() functions below.
			
			$foreground = str_replace('#','',$foreground);
			
			if( is_null ($background ) ) {

				$background = 'FFFFFF'; // default background.

			}

			$pattern = '~^[a-f0-9]{6,6}$~i'; // accept only valid hexadecimal colour values.

			if( ! @preg_match( $pattern, $foreground ) || ! @preg_match( $pattern, $background ) ) {

				trigger_error( "Invalid hexadecimal color value(s) found", E_USER_WARNING );
				return false;

			}

			$opacity = intval( $opacity ); // validate opacity data/number.

			if( $opacity > 100  || $opacity < 0 ) {

				trigger_error( "Opacity percentage error, valid numbers are between 0 - 100", E_USER_WARNING );
				return false;

			}

			if( $opacity == 100 ) {

				return strtoupper( $foreground ); // $transparency == 0
			
			}  

			if( $opacity == 0 ) {

				return strtoupper( $background ); // $transparency == 100
			}

			// calculate $transparency value.
			$transparency = 100 - $opacity;

			if( ! isset( self::$_color_cache[$foreground] ) ) { 

				$f = array(
					'r' => hexdec( $foreground[0] . $foreground[1] ),
					'g' => hexdec( $foreground[2] . $foreground[3] ),
					'b' => hexdec( $foreground[4] . $foreground[5] )
				);

				self::$_color_cache[$foreground] = $f;
			
			} else { 

				$f = self::$_color_cache[$foreground];

			}

			if( ! isset( self::$_color_cache[$background] ) ) {

				$b = array( 
					'r' => hexdec( $background[0] . $background[1] ),
					'g' => hexdec( $background[2] . $background[3] ),
					'b' => hexdec( $background[4] . $background[5] )
				);

				self::$_color_cache[$background] = $b;

			} else {

				$b = self::$_color_cache[$background];

			}

			$add = array(
				'r' => ( $b['r'] - $f['r'] ) / 100,
				'g' => ( $b['g'] - $f['g'] ) / 100,
				'b' => ( $b['b'] - $f['b'] ) / 100
			);

			$f['r'] += intval( $add['r'] * $transparency );
			$f['g'] += intval( $add['g'] * $transparency );
			$f['b'] += intval( $add['b'] * $transparency );

			return sprintf( '%02X%02X%02X', $f['r'], $f['g'], $f['b'] );

		}

	}

}
