<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and
 * make your changes there. Failure to do this will result in changes being
 * overwritten by an automatic update in the future.
 */

require_once( 'library/plugin-update-checker/plugin-update-checker.php' );
require_once( 'library/sc-customizer/bootstrap.php' );
require_once( 'library/sc-widgets/bootstrap.php' );

CONST SC_GOOGLE_MAPS_API_KEY = 'AIzaSyCPCdnxQ6y_RwP1jy7teKfUTZZa5POU7sI';
CONST FACEBOOK_FEED_LICENSE  = 'cdb9433ef9dc133f71f97a38dc872de1';
CONST TWITTER_FEED_LICENSE   = '93265a453ce518c629c0252be057911b';

Puc_v4_Factory::buildUpdateChecker(
	'https://uhsrepo.eskycity.cloud/themes/uhs-theme-1/theme.json',
	__FILE__,
	'uhs-theme-1'
);

//------------------------------------------------------------------------------

function sc_acf_google_map_api( $api ) {

	$api['key'] = SC_GOOGLE_MAPS_API_KEY;

	return $api;

}

add_filter( 'acf/fields/google_map/api', 'sc_acf_google_map_api' );

//------------------------------------------------------------------------------

function sc_acf_init() {

	acf_update_setting( 'google_api_key', SC_GOOGLE_MAPS_API_KEY );

}

add_action( 'acf/init', 'sc_acf_init' );

//------------------------------------------------------------------------------

add_filter( 'acf/settings/load_json',  function ( $paths ) {

	$load_point = get_stylesheet_directory() . '/library/advanced-custom-fields/json';

	$paths[] = $load_point;

	return $paths;

}, 99);

//------------------------------------------------------------------------------

add_action( 'init',                      'sc_check_static_theme_files'      );
add_action( 'after_switch_theme',        'sc_update_static_theme_files', 10 );
add_action( 'upgrader_process_complete', 'sc_update_static_theme_files', 10 );

function sc_check_static_theme_files() {

	if( isset( $_REQUEST['sc-update-static-theme-files'] ) ) {

		sc_update_static_theme_files();
		return true;

	}

	$last_theme_version    = get_option( 'sc-last-theme-version', false );
	$current_theme         = wp_get_theme();
	$current_theme_version = $current_theme->get( 'Version' );

	if( $last_theme_version !== $current_theme_version) {

		sc_update_static_theme_files();
		update_option( 'sc-last-theme-version', $current_theme_version );

	}

}

function sc_update_static_theme_files() {

	SC_Customizer_Stylesheet::singleton()->generate_stylesheet();
	SC_Customizer_Stylesheet::singleton()->generate_stylesheet( true );
	SC_Customizer_Stylesheet_Ovveride::singleton()->generate_css_override();
	SC_Customizer_Stylesheet_Ovveride::singleton()->generate_css_override( true );
	SC_Modules::json_builder()->generate_acf_json();

}

//------------------------------------------------------------------------------

function sc_load_scripts() {

	$preview = is_customize_preview();
	$child   = get_stylesheet_directory_uri();

	wp_enqueue_script( 
		'yotrack', 
		'//yotrack.cdn.ybn.io/yotrack.min.js'
	);

	wp_enqueue_script( 
		'scripts', 
		get_stylesheet_directory_uri() . '/assets/js/scripts.js', 
		array( 'jquery', 'underscore', 'yotrack' )
	);

	wp_enqueue_script( 'scripts', $child . '/assets/js/scripts.js', array( 'jquery', 'underscore' ) );

	if ( SC_Customizer_Stylesheet::singleton()->original_stylesheet_was_updated() ) {

		SC_Customizer_Stylesheet::singleton()->generate_stylesheet();

	}

	if( $preview ) {

		SC_Customizer_Stylesheet::singleton()->generate_stylesheet( $preview );
		SC_Customizer_Stylesheet_Ovveride::singleton()->generate_css_override( $preview );

	}

	switch( get_theme_mod( 'sc_font_scheme' ) ) {

		case 'font_scheme_3':
			$font_scheme = array(
				'Dosis:400,500,600,700',
				'Libre+Baskerville:400,400i,700'
			);
			break;

		case 'font_scheme_2': 
			$font_scheme = array(				
				'Montserrat:400,500,600,700',
				'Nunito:400,400i,800'
			);
			break;

		case 'font_scheme_1':
		default:
			$font_scheme = array(
				'Lato:400,400i,700,700i',
				'Encode+Sans:300,400,500,700',
				'Rokkitt:500'
			);
			break;

	}

	foreach( $font_scheme as $font_family) {

		wp_enqueue_style( sanitize_title( $font_family ), 'https://fonts.googleapis.com/css?family=' . $font_family );

	}

	wp_enqueue_style( 'style', SC_Customizer_Stylesheet::singleton()->get_generated_stylesheet_file_url( $preview ) );

	wp_add_inline_style( 'style', SC_Customizer_Stylesheet_Ovveride::singleton()->get_css_ovverride( $preview ) );

}

//------------------------------------------------------------------------------

function sc_init_image_sizes() {

	// Featured Image
	//
	add_image_size( 'featured-image', 260, 260, true );

	// Hero Module
	//
	add_image_size( 'hero-hg-up', 1920, 575,  true );
	add_image_size( 'hero-xl-up', 1599, 575,  true );
	add_image_size( 'hero-lg-up', 1399, 575,  true );
	add_image_size( 'hero-md-up', 1199, 575,  true );
	add_image_size( 'hero-sm-up', 991,  234,  true );
	add_image_size( 'hero-xs-up', 767,  234,  true );

	// Interior Page Banner
	//
	add_image_size( 'interior-banner-hg-up', 1920, 288,  true );
	add_image_size( 'interior-banner-xl-up', 1599, 288,  true );
	add_image_size( 'interior-banner-lg-up', 1399, 288,  true );
	add_image_size( 'interior-banner-md-up', 1199, 288,  true );
	add_image_size( 'interior-banner-sm-up', 991,  160,  true );
	add_image_size( 'interior-banner-xs-up', 767,  160,  true );

	// Stories Module
	//
	add_image_size( 'story-xs-up',             488, 469, true ); // <-- old square aspect ratio
	add_image_size( 'story-xs-up-recommended', 720, 469, true ); // <-- preferred landscape aspect ratio
	add_image_size( 'story-md-up',             866, 564, true );

	// Gallery Module
	//
	add_image_size( 'gallery-thumb',   101,  63,  true );
	add_image_size( 'gallery-display', 1140, 641, true );

}

//------------------------------------------------------------------------------

function sc_init_menus() {

	// Register nav menus
	//
	register_nav_menus(
		array(
			'header_menu'       => 'Header Menu - Main',
			'header_menu_small' => 'Header Menu - Small',
			'footer_menu'       => 'Footer Menu'
		)
	);

}

//------------------------------------------------------------------------------

function sc_init_modules() {

	if ( ! class_exists('SC_Modules') ) {

		return false;

	}

	$sizes = (object) array(

		'all' => array(
			'One of twelve columns'    => 1,
			'Two of twelve columns'    => 2,
			'Three of twelve columns'  => 3,
			'Four of twelve columns'   => 4,
			'Five of twelve columns'   => 5,
			'Six of twelve columns'    => 6,
			'Seven of twelve columns'  => 7,
			'Eight of twelve columns'  => 8,
			'Nine of twelve columns'   => 9,
			'Ten of twelve columns'    => 10,
			'Eleven of twelve columns' => 11,
			'Twelve of twelve columns' => 12,
		),

		'one_quarter_up' => array(
			'Three of twelve columns'  => 3,
			'Four of twelve columns'   => 4,
			'Five of twelve columns'   => 5,
			'Six of twelve columns'    => 6,
			'Seven of twelve columns'  => 7,
			'Eight of twelve columns'  => 8,
			'Nine of twelve columns'   => 9,
			'Ten of twelve columns'    => 10,
			'Eleven of twelve columns' => 11,
			'Twelve of twelve columns' => 12,
		),

		'one_half_up' => array(
			'Six of twelve columns'    => 6,
			'Seven of twelve columns'  => 7,
			'Eight of twelve columns'  => 8,
			'Nine of twelve columns'   => 9,
			'Ten of twelve columns'    => 10,
			'Eleven of twelve columns' => 11,
			'Twelve of twelve columns' => 12,
		),

		'three_quarters_up' => array(
			'Eight of twelve columns'  => 8,
			'Nine of twelve columns'   => 9,
			'Ten of twelve columns'    => 10,
			'Eleven of twelve columns' => 11,
			'Twelve of twelve columns' => 12,
		),

		'full' => array(
			'Twelve of twelve columns' => 12,
		),

	);

	SC_Modules::singleton()->register_layouts(
		array(
			'Front Page',
			'Interior Page',
			'Interior Page With Sidebar',
			'Interior Page With No Banner',
		)
	);

	SC_Modules::singleton()->register_module_types(array(

		'Text' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Title' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Image' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Form' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Tabs' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Banner' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With No Banner'
			)
		),

		'Card' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Gallery' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->one_half_up,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Location' => array(
			'fluid' => true,
			'size'  => 12,
			'sizes' => $sizes->full,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With No Banner'
			)
		),

		'Stories' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->full,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Video' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),

		'Social Feed' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
		),
		
		'Subscribe' => array(
			'fluid' => false,
			'size'  => 12,
			'sizes' => $sizes->all,
			'layouts' => array(
				'Front Page',
				'Interior Page',
				'Interior Page With Sidebar',
				'Interior Page With No Banner'
			)
    ),

		'Hero' => array(
			'fluid' => true,
			'size'  => 12,
			'sizes' => $sizes->full,
			'layouts' => array(
				'Front Page',
				'Interior Page With No Banner'
			)
		)

	));

	SC_Modules::json_builder()->generate_acf_json();
	SC_Modules::json_builder()->add_acf_load_point();

}

//------------------------------------------------------------------------------

add_filter( 'style_loader_src',  'sc_remove_querystring_from_assets', 10, 2 );
add_filter( 'script_loader_src', 'sc_remove_querystring_from_assets', 10, 2 );

function sc_remove_querystring_from_assets( $src ) {

	if( is_customize_preview() ) {

			$src = add_query_arg( 'nocache', time(), $src );

	} else {

		if( strpos( $src, '?ver=' ) ) {

			$src = remove_query_arg( 'ver', $src );

		}

	}


	return $src;

}

//------------------------------------------------------------------------------

function sc_init_provisioner() {

	if( ! class_exists( 'SC_Provisioner' ) ) {

		return false;

	}

	SC_Provisioner::singleton();

}

//------------------------------------------------------------------------------

add_filter('wp_nav_menu_objects', 'sc_add_header_menu_icons', 10, 2);

function sc_add_header_menu_icons( $items, $args ) {

	if( 'header_menu_small' !== $args->theme_location ) {

		return $items;

	}

	foreach( $items as &$item ) {

		$mobile_icon  = get_field( 'sc_mobile_icon',  $item );
		$desktop_icon = get_field( 'sc_desktop_icon', $item );
		$classes      = array();

		if( $mobile_icon ) {

			$mobile_icon = sprintf(
				'<i class="fa %s hidden-md hidden-lg" aria-hidden="true"></i>',
				esc_attr( $mobile_icon )
			);

		} else {

			$mobile_icon = '<i class="fa fa-info-circle hidden-md hidden-lg" aria-hidden="true"></i>';

		}

		if( $desktop_icon ) {

			$desktop_icon = sprintf(
				'<i class="fa %s hidden-xs hidden-sm" aria-hidden="true"></i>',
				esc_attr( $desktop_icon )
			);

			$classes[] = 'has-desktop-icon';

		} else {

			$desktop_icon = '';

		}

		$item->title = sprintf(
			'%s%s <span class="%s">%s</span>',
			$mobile_icon,
			$desktop_icon,
			implode( ' ', $classes ),
			esc_html( $item->title )
		);

	}

	return $items;

}

//------------------------------------------------------------------------------

function sc_render_page_banner_open() {

	do_action( 'sc_before_page_banner' );

	$default_image = get_theme_mod( 'sc_default_interior_banner', '' );
	$images        = get_interior_banner_images();

	if( is_int( $default_image ) ) {

		$default_image = wp_get_attachment_image_src(
			$default_image,
			'interior-banner-hg-up'
		);

		if( isset( $default_image[0] ) ) {

			$default_image = $default_image[0];

		}

	}

	if( ! empty( $images->hg->image ) ) {
		$default_image = $images->hg->image;
	}

	if( empty( $default_image ) ) {
		$default_image = sc_get_image( 'default-interior-banner.jpg', false );
	}

	echo '
		<div
			id="page-banner"
			style="background-image: url(\'' . $default_image . '\');"
			data-image-xs-up="' . esc_attr( $images->xs->image ) . '"
			data-image-sm-up="' . esc_attr( $images->sm->image ) . '"
			data-image-md-up="' . esc_attr( $images->md->image ) . '"
			data-image-lg-up="' . esc_attr( $images->lg->image ) . '"
			data-image-xl-up="' . esc_attr( $images->xl->image ) . '"
			data-image-hg-up="' . esc_attr( $images->hg->image ) . '"
			data-valign-xs-up="' . esc_attr( $images->xs->valign ) . '"
			data-valign-sm-up="' . esc_attr( $images->sm->valign ) . '"
			data-valign-md-up="' . esc_attr( $images->md->valign ) . '"
			data-valign-lg-up="' . esc_attr( $images->lg->valign ) . '"
			data-valign-xl-up="' . esc_attr( $images->xl->valign ) . '"
			data-valign-hg-up="' . esc_attr( $images->hg->valign ) . '"
			data-halign-xs-up="' . esc_attr( $images->xs->halign ) . '"
			data-halign-sm-up="' . esc_attr( $images->sm->halign ) . '"
			data-halign-md-up="' . esc_attr( $images->md->halign ) . '"
			data-halign-lg-up="' . esc_attr( $images->lg->halign ) . '"
			data-halign-xl-up="' . esc_attr( $images->xl->halign ) . '"
			data-halign-hg-up="' . esc_attr( $images->hg->halign ) . '">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">';

	do_action( 'sc_after_page_banner_open' );

}

function get_interior_banner_images() {

	$sizes   = array( 'hg', 'lg', 'xl', 'md', 'sm', 'xs' );
	$images  = (object) array();
	$image   = '';
	$valign  = 'center';
	$halign  = 'center';
  $post_id = get_the_ID();

  if( is_home() && 'page' == get_option( 'show_on_front' ) ) {

    $blog_page_id = (int) get_option( 'page_for_posts' );

    if( 0 !== $blog_page_id ) {

      $post_id = $blog_page_id;

    }

  }

	// Loop over each size and check if it has an image or not. If it does
	// then break out of the loop and use that image as our default. In
	// addition if alignment options for the image are set then use them as
	// defaults.
	//
	for( $c = 0; $c < count( $sizes ); $c++ ) {

		if( ! empty( get_field( $size . '_image', $post_id ) ) ) {

			$image = get_field( $size . '_image', $post_id );

			break;

		}

		if( ! empty( get_field( $size . '_alignment', $post_id )['vertical'] ) ) {

			$valign = get_field( $size . '_alignment', $post_id )['vertical'];

		}

		if( ! empty( get_field( $size . '_alignment', $post_id )['horizontal'] ) ) {

			$halign = get_field( $size . '_alignment', $post_id )['horizontal'];

		}

	}

	for( $c = 0; $c < count( $sizes ); $c++ ) {

		$size = $sizes[$c];

		if( ! empty( get_field( $size . '_image', $post_id ) ) ) {

			$image = get_field( $size . '_image', $post_id );

		}

		if( ! empty( get_field( $size . '_alignment', $post_id )['vertical'] ) ) {

			$valign = get_field( $size . '_alignment', $post_id )['vertical'];

		}

		if( ! empty( get_field( $size . '_alignment', $post_id )['horizontal'] ) ) {

			$halign = get_field( $size . '_alignment', $post_id )['horizontal'];

		}

		$images->$size = (object) array(
			'image'  => $image ? $image['sizes']['hero-' . $size . '-up'] : '',
			'valign' => $valign,
			'halign' => $halign
		);

	}

	return $images;

}

//------------------------------------------------------------------------------

function sc_render_page_banner( $title = null ) {

	sc_render_page_banner_open();

	echo '<h1 class="title">';

	if ( is_null( $title ) ) {

		$title_override = get_field( 'title_override' );

		if( $title_override ) {

			echo esc_html( $title_override );

		} else {

			the_title();

		}

	} else {

		echo $title;

	}

	echo '</h1>';

	sc_render_page_banner_close();

}

//------------------------------------------------------------------------------

add_action( 'sc_before_page', 'sc_open_sidebar_if_needed' );

function sc_open_sidebar_if_needed() {

	if( is_search() ) {

    return;

  }

	if( 'page-with-sidebar.php' == basename( get_page_template() ) ) {

		SC_Modules::singleton()->container_open = '<div class="container-fluid sc-module-container"><div class="row">';

		ob_start();

		echo '
			<div id="page-sidebar">
				<div class="widgets clearfix">';

		if(is_active_sidebar('page-sidebar')) dynamic_sidebar('page-sidebar');

		echo '
				</div>
			</div>';

		$sidebar = ob_get_clean();

		echo '
			<div class="container">
				<div class="row">
					<div class="col-md-3 hidden-xs hidden-sm">' . $sidebar . '</div>
					<div class="col-xs-12 col-md-9">';

	}

}

//------------------------------------------------------------------------------

add_action( 'sc_after_page', 'sc_close_sidebar_if_needed' );

function sc_close_sidebar_if_needed() {
	
	if( is_search() ) {

		return;

	}

	if( 'page-with-sidebar.php' == basename( get_page_template() ) ) {

		echo '
					</div>
				</div>
			</div>';

	}

}

//------------------------------------------------------------------------------

function sc_render_non_modular_page_content_open() {

	$content_before_posts = '';

  if( is_home() && 'page' == get_option( 'show_on_front' ) ) {

    $blog_page_id = (int) get_option( 'page_for_posts' );

    if( 0 !== $blog_page_id ) {

			if( get_field( 'modules', $blog_page_id ) ) {

				ob_start();
	
				SC_Modules::singleton()->set_modules( get_field( 'modules', $blog_page_id ) );
				SC_Modules::singleton()->display_modules();
	
				$content_before_posts = ob_get_clean();
				
			}

    }

  }

  do_action( 'sc_before_page' );

	echo '<div id="page-content">
					<div>' . $content_before_posts . '</div>
					<div class="container sc-non-modular-container">
						<div class="row">
							<div class="col-xs-12">';

  do_action( 'sc_after_page_open' );

}

//------------------------------------------------------------------------------

function sc_add_menu_item_depth( $atts, $item, $args, $depth ) {

	$atts['data-depth'] = $depth;

	return $atts;

}

add_filter( 'nav_menu_link_attributes', 'sc_add_menu_item_depth', 10, 4 );

//------------------------------------------------------------------------------

add_action( 'init', 'sc_color_swatches' );

function sc_color_swatches( ) {

	if( ! isset( $_REQUEST['sc-color-swatches'] ) ) {

		return false;

	}

	$colors = array(
		'primary'    => get_theme_mod( 'sc_primary_color',   '#351A52' ),
		'primary-dark' => get_theme_mod( 'sc_primary_color',   '#351A52' ),
		'accent'     => get_theme_mod( 'sc_secondary_color', '#BE3932' ),
		'alt-accent' => get_theme_mod( 'sc_tertiary_color',  '#F4504C' ),
		'secondary'  => get_theme_mod( 'sc_text_color',      '#33333C' ),
	);

	echo '<p>NOTE: SASS variables do not match customizer labels.</p>';

	foreach( $colors as $name => $color ) {
		$blend = ($name === 'primary-dark') ? '000000' : null;

		echo '<div style="float:left;">';

		for( $opacity = 100; $opacity > 0; $opacity -= 5 ) {

			$modified_color = SC_Customizer_Stylesheet::singleton()->color_blend_by_opacity( $color, $opacity, $blend );

			echo '<div style="display:flex;align-items:center;padding:10px;width:200px;height:50px;background:#' . $modified_color . '">#' . $modified_color . ' - ' . $opacity . '</div>';

		}

		echo '</div>';

	}

	echo '<pre style="float:left;margin:0 0 0 30px;">';

	foreach( $colors as $name => $color ) {
		$blend = ($name === 'primary-dark') ? '000000' : null;
		for( $opacity = 100; $opacity > 0; $opacity -= 5 ) {

			$modified_color = SC_Customizer_Stylesheet::singleton()->color_blend_by_opacity( $color, $opacity, $blend );

			echo "\n" . '$' . $name . '-color-' . $opacity . ': #' . $modified_color . ';';

		}

		echo "\n";

	}

	echo '</pre>';

	exit;

}

//------------------------------------------------------------------------------

function sc_logo( $theme_mod = 'sc_header_logo' ) {

	$header_logo = $image = get_theme_mod( $theme_mod, false );
	$blog_name   = get_bloginfo( 'name' );

	if( $header_logo ) {

		sc_image( $header_logo, array( 'class' => 'image', 'alt' => $blog_name ) );

	} else {

		echo '<span class="text">' . esc_html( $blog_name ) . '</span>';

	}

}

//------------------------------------------------------------------------------

function sc_google_tag_manager_id() {

	$id = get_theme_mod( 'sc_google_tag_manager_id', false );

	if( false === $id ) {

		return false;

	}

	$id = trim( $id );

	if( empty( $id ) ) {

		return false;

	}

	return $id;

}

//------------------------------------------------------------------------------

function sc_use_desktop_nav_always() {

  $use_page_based_mobile_nav = get_theme_mod( 'sc_use_page_based_mobile_nav' );

	return ! $use_page_based_mobile_nav;

}

//------------------------------------------------------------------------------

function sc_yotrack_swap() {

	$yotrack_settings = get_theme_mod( 'sc_yotrack' );

	if( is_array( $yotrack_settings ) && count( $yotrack_settings ) > 0 ) { ?>

		<script type="text/javascript">
		var phones = [];
		<?php 

		$settings_grouped = array();
		$counter		  = 1;

		array_walk( $yotrack_settings, function($v) use ( &$settings_grouped ){

			if ( array_key_exists( $v["sc_tracking_cid"], $settings_grouped ) ) {

				$settings_grouped[$v["sc_tracking_cid"]]["sc_tracking_phone"][] = $v["sc_tracking_phone"];

			} else {

				$v["sc_tracking_phone"] = [$v["sc_tracking_phone"]];
				$settings_grouped[$v["sc_tracking_cid"]] = $v;

			}

		});

		foreach( $settings_grouped as $setting ) { 
			
			if( ! empty($setting['sc_tracking_cid'] ) ) { ?>
			
				YoTrack('universalhealthservices', '<?php echo esc_js( $setting['sc_tracking_cid']); ?>', function(err, api) {

					<?php if( ! empty( $setting['sc_tracking_phone'] ) ) { ?>
					
						var lines = <?php echo json_encode( $setting['sc_tracking_phone'] ); ?>;

						api.swapPhones(lines, function(err, line) {

							phones.push( {'orginal_lines': lines, 'swapped_line': line, 'cid': '<?php echo esc_js( $setting['sc_tracking_cid']); ?>' } );

							<?php if( $counter == count($settings_grouped) ): ?>

								if (typeof render_map == 'function') { 

									render_map(); 
									
								}

							<?php endif; ?>

						});

					<?php } ?>
				}); 
				
			<?php }
		$counter++;
		}
		?>

		</script>
		<?php 

	} else {  ?>

			<script type="text/javascript">
				(function($) {
					$( document ).ready(function() {
						if (typeof render_map == 'function') { 
							render_map(); 
						}
					});
				})(jQuery);
			</script>

		<?php

	}

}

