<div class="post-content">
	<div class="post-date">
		<span><?php echo get_the_date(); ?></span>
	</div>
	<?php 
	
	$modules = false;

	if( class_exists('SC_Modules') ) {

		if( is_array( SC_Modules::singleton()->get_modules() ) ) {

			$modules = true;

		}

	}

	if( $modules ) {

		SC_Modules::singleton()->container_open  = '<div class="row">';
		SC_Modules::singleton()->container_close = '</div>';
		
		SC_Modules::singleton()->display_modules();

	} else {

		the_content();
	
	}
	
	?>
	<?php if( get_field( 'enable_sharethis' ) ): ?> 
		<div class="post-sharethis">
			<div class="sharethis-inline-share-buttons"></div>
		</div>
	<?php endif; ?>
</div>