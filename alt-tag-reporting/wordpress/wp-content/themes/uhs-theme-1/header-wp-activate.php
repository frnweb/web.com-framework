<?php
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 *
 * Header WP Activate
 *
 * Used on the WordPress account activation page
 *
 * @package WordPress
 * @author Web.com
 */

// Inlcude the main child theme functions file.
require_once( 'functions.php' );

// Inlcude the main parent theme functions file.
require_once( get_template_directory() . '/functions.php' );

/**
 * Starts an output buffer. footer_wp_activate() located in
 * footer-wp-activate.php will stop, capture, and echo the buffered content.
 *
 * @return void
 */
function header_wp_activate() {

	ob_start();

}

header_wp_activate();
