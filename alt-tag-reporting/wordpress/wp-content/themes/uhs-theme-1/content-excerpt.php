<?php if (! defined('ABSPATH')) die('No direct access allowed'); ?>
<div class="post-content post-excerpt">
	<?php 
	
	$modules = false;

	if( class_exists('SC_Modules') ) {

		if( is_array( SC_Modules::singleton()->get_modules() ) ) {

			$modules = true;

		}

	}

	if( $modules ) {

		$search = new SC_Search( $post_id );

		echo '<p>' . sc_trim_words( $search->get_plain_text_content(), 250 ) . ' [...]</p>';

		echo '<a href="' . esc_url( get_permalink() ) . '" class="arrow-link">Read More</a>';

	} else {

		the_excerpt();
	
	}
	
	?>
</div>