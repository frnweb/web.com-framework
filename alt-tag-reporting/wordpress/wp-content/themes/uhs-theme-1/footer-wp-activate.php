<?php
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 *
 * Footer WP Activate
 *
 * Used on the WordPress account activation page.
 *
 * @package WordPress
 * @author Web.com
 */

/**
 * Capture the output buffer started by header_wp_activate() located in
 * header-wp-activate.php. Render out the entire page.
 *
 * @return void
 */
function footer_wp_activate() {

	$activation_content = '<div class="sc-module sc-module-text">' . ob_get_clean() . '</div>';

	get_header();

	sc_render_page( 'Activation', $activation_content );

	get_footer();

}

footer_wp_activate();
