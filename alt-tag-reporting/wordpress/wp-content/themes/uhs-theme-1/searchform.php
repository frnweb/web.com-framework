<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */
?>
<form method="get" class="search-form" action="<?php bloginfo('url'); ?>/">
	<label for="search_input" class="hidden">Search Website</label>
	<input id="search_input" type="text" class="search-input" name="s" placeholder="Search Website" />
	<label for="search_submit" class="hidden">Submit Search</label>
	<input id="search_submit" type="submit" class="search-button button" value="Search" />
</form>