<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE OR THEME
 * --------------------------------------------------
 * If you need to make direct changes to this file or any file in this theme
 * you should make a full copy the entire theme, re-name it, activate it, and 
 * make your changes there. Failure to do this will result in changes being 
 * overwritten by an automatic update in the future.
 */

function sc_get_search_title( $post_id ) {

  if( function_exists( 'relevanssi_get_the_title') ) {

    return relevanssi_get_the_title( $post_id );

  }

  return get_the_title();

}

function sc_get_search_excerpt( $post_id, $term ) {

  $search         = new SC_Search( $post_id );
  $content        = str_replace( '&nbsp;', '', $search->get_plain_text_content() );
  $max_length     = 250;
  $term_position  = stripos( $content, $term );
  $start_position = floor( $term_position - $max_length / 2 );

  if( $start_position < 0 ) {
    $start_position = 0;
  }

  $excerpt = trim(
    substr( 
      $content, 
      $start_position, 
      $max_length
    )
  );

  if( function_exists( 'relevanssi_highlight_terms' ) ) {

    $excerpt = relevanssi_highlight_terms( $excerpt, $term );
  
  }

  if( ! empty( $excerpt ) ) {

    $excerpt = '...' . $excerpt . '...';

  }

  return $excerpt;

}

get_header();

sc_render_page_banner( 'Search Results' );
sc_render_non_modular_page_content_open();

?>
<form method="get" class="search-form" action="<?php bloginfo('url'); ?>/">
  <label for="search-form-input" class="hidden">Search Website</label>
  <input type="text" id="search-form-input" class="search-form-input" name="s" value="<?php if( isset( $_REQUEST['s'] ) ) echo esc_attr( (string) $_REQUEST['s'] ); ?>" placeholder="Search Website" />
  <label for="search-form-button" class="hidden">Submit Search</label>
  <button title="Search Website for Text Entered" type="submit" id="search-form-button" class="search-form-button">
    <svg width="16" height="16" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
      <path d="M1216 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/>
    </svg>
  </button>
</form>
<?php

if ( have_posts() ) {

  while ( have_posts() ) {

    the_post();

    echo '<div class="post clearfix">';

    printf(
      '<div class="post-title">
        <h2 class="title">
          <a href="%s">
            %s
          </a>
        </h2>
      </div>',
      esc_url( get_permalink() ),
      sc_get_search_title( get_the_ID() )
    );

    printf(
      '<div class="post-content post-excerpt">
        %s
      </div>',
      sc_get_search_excerpt( get_the_ID(), get_search_query() )
    );
    
    echo '</div>';

  }

  sc_render_pagination();

} else {

  if( ! empty( $_REQUEST['s'] ) ) {
    echo '<p>Sorry, no posts matched your criteria.</p>';
  }

}

sc_render_non_modular_page_content_close();
get_footer();
  