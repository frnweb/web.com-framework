<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */
?>
<div class="post clearfix">
	<?php if( is_single() ): ?>

		<?php get_template_part( 'content', 'content' ); ?>

		<div class="post-navigation">
			<?php previous_post_link( '%link' ); ?>
			<?php next_post_link( '%link' ); ?>
		</div>

	<?php else: ?>

		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<div class="post-featured-image">
					<a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
						<?php

						if( empty( get_the_post_thumbnail( null, 'featured-image' ) ) ) {

							$thumbnail = get_theme_mod( 'sc_default_featured_image' ); 

							if( is_int( $thumbnail ) ) {

								$thumbnail_img = wp_get_attachment_image ( $thumbnail, 'featured-image' );

							}

							echo $thumbnail_img;

						} else {

							the_post_thumbnail( 'featured-image' );

						}

						?>
					</a>
	      </div>
	    </div>
			<div class="col-xs-12 col-sm-8">
				<?php get_template_part( 'content', 'title'   ); ?>
				<div class="post-date">
					<span><?php echo get_the_date(); ?></span>
				</div>
				<?php get_template_part( 'content', 'excerpt' ); ?>
				<?php get_template_part( 'content', 'meta'    ); ?>
			</div>
		</div>
		
	<?php endif; ?>
</div>