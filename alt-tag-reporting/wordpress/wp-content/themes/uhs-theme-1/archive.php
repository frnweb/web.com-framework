<?php if (! defined('ABSPATH')) die('No direct access allowed');

get_header();

if ( have_posts() ) {

	if ( is_home() ) {

		$title = single_post_title('', false);

	}	else {

		$title = get_the_archive_title();
		
	}

	sc_render_page_banner( $title );
	sc_render_non_modular_page_content_open();
	
	echo '<div class="row"><div class="col-xs-12 col-md-9">';

	while ( have_posts() ) {

		the_post();

		get_template_part( 'content', get_post_type() );

	}

	echo '</div><div class="col-xs-12 col-md-3"><div id="default-sidebar" class="sc-sidebar">';

	$default_sidebar = apply_filters( 'sc_default_sidebar', 'default-sidebar' );

	if( is_active_sidebar( $default_sidebar ) ) {

		dynamic_sidebar( $default_sidebar );

	}

	echo '</div></div></div>';

	sc_render_pagination();
	sc_render_non_modular_page_content_close();

} else {

	$title   = 'No Posts Found';
	$content = '<p>Sorry, no posts were found. Thank You.</p>';

	sc_render_page( $title, $content );

}

get_footer();