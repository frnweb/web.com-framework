<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */
?>
<form method="get" class="search-form" action="<?php bloginfo('url'); ?>/">
	<input type="text" class="search-input" name="s" placeholder="Search Website" />
	<input type="submit" class="search-button" value="Search" />
</form>