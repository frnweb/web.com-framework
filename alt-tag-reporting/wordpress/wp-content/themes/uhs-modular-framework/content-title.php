<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */
?>
<div class="post-title">
	<?php if( is_single() ): ?>
    <h1 class="title">
  		<span>
  			<?php the_title(); ?>
  		</span>
  	</h1>
  <?php else: ?>
    <h2 class="title">
      <a href="<?php echo esc_url( get_permalink() ); ?>">
        <?php the_title(); ?>
      </a>
    </h2>
  <?php endif; ?>
</div>