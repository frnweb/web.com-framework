<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */
?>
<div class="post clearfix">
	<?php 

	if(is_single()) {

		get_template_part( 'content', 'content' );

	} else {

		get_template_part( 'content', 'title'   );
		get_template_part( 'content', 'excerpt' );
		get_template_part( 'content', 'meta'    );

	}

	?>
</div>