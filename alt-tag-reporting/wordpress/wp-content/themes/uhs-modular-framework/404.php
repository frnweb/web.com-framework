<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */
get_header();

$title   = 'Page Not Found';
$content = <<<EOD
<p>We're sorry, but the page you are looking for cannot be found.</p>
<p>If you are still having a problem, please feel free to contact us.</p>
EOD;

sc_render_page( $title, $content );

get_footer();