<?php if (! defined('ABSPATH')) die('No direct access allowed');

  $defaults = array(
    'name' => '',
  );

  $module = array_merge( $defaults, $module );

  if( ! empty( $module['name'] ) ) {

    printf( '<a name="%s"></a>', esc_attr( $module['name'] ) );

  }
