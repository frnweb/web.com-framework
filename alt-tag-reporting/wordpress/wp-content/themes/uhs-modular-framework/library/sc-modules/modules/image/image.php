<?php if (! defined('ABSPATH')) die('No direct access allowed'); 

if( ! class_exists( 'SC_Image_Module' ) ) {

  class SC_Image_Module {

    private static $_data;
    private static $_image;

    public static function get_image() {

      if( ! isset( self::$_image['url'] ) ) {

        return false;

      }

      return self::$_image['url'];

    }

    public static function get_alt() {

      if( ! isset( self::$_image['alt'] ) ) {

        return '';

      }

      return trim( self::$_image['alt'] );

    }

    public static function get_caption() {

      if( ! self::show_caption() ) {

        return false;

      }

      if( ! isset( self::$_image['caption'] ) ) {

        return false;

      }

      $caption = trim( self::$_image['caption'] );

      if( empty( $caption ) ) {

        return false;

      }

      return $caption;

    }
    
    public static function get_classes() {

      $classes = array( 'image' );

      if( self::get_caption() ) {

        $classes[] = 'image-with-caption';

      }

      if( self::show_border() ) {

        $classes[] = 'image-with-border';

      }

      return implode( ' ', $classes );

    }

    public static function show_caption() {

      if( 'yes' !== self::$_data['show_caption'] ) {

        return false;

      }

      return true;

    }

    public static function show_border() {

      if( 'yes' !== self::$_data['show_border'] ) {

        return false;

      }

      return true;

    }

    public static function set_data( $data ) {

      self::$_data  = $data;
      self::$_image = isset( $data['image'] ) ? $data['image'] : false;

    }

  } 

}

SC_Image_Module::set_data( $module );

?>
<section class="sc-module sc-module-image" id="<?php echo esc_attr( $module['module_id'] ); ?>">
  <div class="row">
    <div class="col-xs-12">
      <?php

        if( false !== SC_Image_Module::get_image() ) {

          sc_image( SC_Image_Module::get_image(), array(

            'class' => esc_attr( SC_Image_Module::get_classes() ),
            'alt'   => esc_attr( SC_Image_Module::get_alt() )

          ));

          $caption = SC_Image_Module::get_caption();

          if( false !== $caption ) {

            printf( '<span class="caption">%s</span>', esc_html( $caption ) );

          }
          
        } else {

          echo '<p class="no-image-found">Oops, No image found.</p>';

        }

      ?>
    </div>
  </div>
</section>