<?php if (! defined('ABSPATH')) die('No direct access allowed'); ?>

<section class="sc-module sc-module-text" id="<?php echo esc_attr( $module['module_id'] ); ?>">
  <div class="row">
    <div class="col-xs-12">
      <?php echo $module['body']; ?>
    </div>
  </div>
</section>
<script type="text/javascript">

  (function($) {

    var moduleId = '#<?php echo esc_js( $module['module_id'] ); ?>';

    $( document ).ready( function() {

      sc.responsiveTable.init(moduleId);

    });

  })(jQuery);

</script>
