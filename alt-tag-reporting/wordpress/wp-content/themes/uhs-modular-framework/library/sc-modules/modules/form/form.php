<?php if (! defined('ABSPATH')) die('No direct access allowed');

  global $post;

?>
<section class="sc-module sc-module-form" id="<?php echo esc_attr( $module['module_id'] ); ?>">
  <div class="row">
    <div class="col-xs-12">
      <?php

        $form_plugin  = $module['form_plugin'];
        $ninja_form   = $module['ninja_form'];
        $gravity_form = $module['gravity_form'];

        switch( $form_plugin ) {

          case 'Gravity Forms':

            if( class_exists( 'RGFormsModel' ) && is_array( $gravity_form ) ) {

              $display_title       = false;
              $display_description = false;
              $display_inactive    = false;
              $field_values        = array();
              $ajax                = true;

              gravity_form_enqueue_scripts( $gravity_form['id'], true );
              gravity_form( 
                $gravity_form['id'], 
                $display_title,     
                $display_description,
                $display_inactive,
                $field_values,
                $ajax
              );

            }

            break;

          case 'Ninja Forms':

            if( class_exists( 'Ninja_Forms' ) && is_array( $ninja_form ) ) { 
              
              Ninja_Forms()->display( $ninja_form['id'] );

            }

            break;

        }

      ?>
    </div>
  </div>
</section>