<?php if (! defined('ABSPATH')) die('No direct access allowed');

  global $post;

  $defaults = array(
    'title'     => '',
	'type'		=> '2',
    'sub_title' => ''
  );

  $module = array_merge($defaults, $module);
  
  $classes = array(
	"sc-module",
	"sc-module-title",
	"sc-module-title-h".$module['type']
  );

?>
<?php if( ! empty( $module['title'] ) ): ?>
  <section class="<?php echo implode(' ', $classes); ?>" id="<?php echo esc_attr( $module['module_id'] ); ?>">
    <div class="row">
      <div class="col-xs-12">
        <div class="inner">
          <h<?php echo $module['type']; ?> class="title">
            <?php echo esc_html( $module['title'] ); ?>
          </h<?php echo $module['type']; ?>>
          <?php if( ! empty( $module['sub_title'] ) ): ?>
            <span class="sub-title">
              <?php echo esc_html( $module['sub_title'] ); ?>
            </span>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>