<?php if (! defined('ABSPATH')) die('No direct access allowed');

if( ! class_exists('SC_Modules_Json_Builder') ) {

	/*
	 * Documentation please
	 */
	class SC_Modules_Json_Builder {

		function __construct() {

			//

		}

		/*
		 * Documentation please
		 */
		public function add_acf_load_point() {

			add_filter( 'acf/settings/load_json',  function ( $paths ) {

				unset($paths[0]);

				$paths[] = SC_Modules::singleton()->get_acf_load_point();
				
				return $paths;

			});

		}

		/*
		 * Documentation please
		 */
		public function generate_acf_json() {

			$layouts = SC_Modules::singleton()->get_layouts();

			if( ! count( $layouts ) ) {

				SC_Modules::singleton()->register_layout( 'Page' );

				$layouts = SC_Modules::singleton()->get_layouts();
			
			}

			foreach ( $layouts as $layout_slug => $layout_options ) {

				$this->generate_layout( $layout_options );

			}

		}

		public function generate_layout( $layout_options ) {

			$module_types = SC_Modules::singleton()->get_module_types();
			$page_layout  = $this->get_layout_json( $layout_options );

			if( false === $page_layout ) {

				return false;

			}

			foreach ( $module_types as $module_name => $module_type ) {

				$module = $this->get_module_field( $module_type );

				if( is_null( $module ) ) {

					continue;

				}

				if( isset( $module_type['layouts'] ) && is_array( $module_type['layouts'] ) ) {
					
					if( in_array( $layout_options['name'], $module_type['layouts'] ) ) {

						$page_layout->fields[0]->layouts[] = $module;

					}

				} else {

					$page_layout->fields[0]->layouts[] = $module;
					
				}

			}

			$page_layout_json = json_encode( $page_layout );
			$acf_load_point   = SC_Modules::singleton()->get_acf_load_point();
			$acf_json_path    = $acf_load_point . '/' . $layout_options['slug'] . '.json';
			$acf_json_file    = fopen( $acf_json_path, 'w' );

			fwrite( $acf_json_file, $page_layout_json );
			fclose( $acf_json_file );

		}

		/*
		 * Documentation please
		 */
		private function get_layout_json( $layout_options ) {

			$json   = @file_get_contents( $layout_options['json'] );
			$layout = json_decode( $json );

			if( ! is_null( $layout ) ) {

				return $layout;

			}

			return false;

		}

		/*
		 * $module_type = object
		 */
		private function get_module_field( $module_type ) {

			$module_json  = @file_get_contents( $module_type['json'] );
			$module_field = json_decode( $module_json );
			$sizes_field  = $this->get_sizes_field( $module_field, $module_type );

			if( false !== $sizes_field ) {

				$module_field->sub_fields[] = $sizes_field;
			
			}

			$module_field->sub_fields[] = $this->get_module_id_field( $module_field );

			return $module_field;

		}

		/*
		 * Documentation please
		 */
		private function get_sizes_field( $module, $options ) {

      if ( ! isset($options['sizes']) ) {

      	return false;

      }

      $choices = array();

			foreach ($options['sizes'] as $label => $size) {
				
				$choices[$size] = $label;

			}

			$wrapper = array(
				'width' => '',
				'class' => '',
				'id'    => ''
			);

			$default_value = '';

			if( isset($options['size']) ) {

				$default_value = $size;

			}

			$sizes = (object) array(

				'key'               => $module->key . '_size',
				'label'             => 'Module Size',
				'name'              => 'size',
				'type'              => 'select',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => $wrapper,
				'choices'           => $choices,
				'other_choice'      => 0,
				'save_other_choice' => 0,
				'default_value'     => $default_value,
				'layout'            => 'vertical'

			);

			return $sizes;

		}

		/*
		 * Documentation please
		 */
		public function get_module_id_field( $module ) {

			$wrapper = array(
				'width' => '',
				'class' => '',
				'id'    => ''
			);

			$module_id_field = (object) array(

				'key'               => $module->key . '_module_id',
				'label'             => 'Module ID',
				'name'              => 'module_id',
				'type'              => 'unique_id_field',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => $wrapper

			);

			return $module_id_field;

		}

	}

}
