<?php if (! defined('ABSPATH')) die('No direct access allowed');

if( ! class_exists('SC_Modules') ) {

	/**
	 * Documentation please
	 */
	class SC_Modules {

		protected $_layouts        = array();
		protected $_module_types   = array();
		protected $_modules        = null;

		protected static $_instance     = null;
		protected static $_admin        = null;
		protected static $_json_builder = null;

		public $container_open  = '<div class="container sc-module-container"><div class="row">';
		public $container_close = '</div></div>';

		function __construct() {

			add_action( 'loop_start', array( $this, 'get_modules' ) );

			self::admin();

		}

		/**
		 * Documentation please
		 */
		public static function singleton() {

			if( null == self::$_instance ) {

				self::$_instance = new self;

			}

			return self::$_instance;

		}

		/**
		 * Documentation please
		 */
		public static function admin() {

			if( null == self::$_admin ) {

				self::$_admin = new SC_Modules_Admin;
			
			}

			return self::$_admin;

		}

		/**
		 * Documentation please
		 */
		public static function json_builder() {

			if( null == self::$_json_builder ) {

				self::$_json_builder = new SC_Modules_Json_Builder;
			
			}

			return self::$_json_builder;

		}

		/**
		 * Documentation please
		 */
		public function register_layouts( $layouts ) {

			foreach( $layouts as $layout_name ) {

				$this->register_layout( $layout_name );

			}

		}

		/**
		 * Documentation please
		 */
		public function register_layout( $name ) {

			$slug    = sanitize_title( $name );
			$dir     = $this->get_layouts_load_point();
			$json    = sprintf( '%1$s/%2$s.json', $dir, $slug );
			$options = array(
				'name' => $name,
				'slug' => $slug,
				'json' => $json
			);

			$this->_layouts[$options['slug']] = $options;

		}

		/**
		 * Documentation please
		 */
		public function register_module_types( $module_types ) {

			foreach($module_types as $module_name => $module_options) {

				$this->register_module_type( $module_name, $module_options );

			}

		}

		/**
		 * Documentation please
		 */
		public function get_layouts() {

			if( ! is_array($this->_layouts) ) {

				return array();

			}

			return $this->_layouts;
		}


		/**
		 * Documentation please
		 */
		public function register_module_type( $name, $options ) {

			if( ! is_array($options) ) {

				$options = array();

			}

			$slug  = sanitize_title( $name );
			$dir   = $this->get_module_load_point( $name );
			$file  = sprintf( '%1$s/%2$s.php',  $dir, $slug );
			$json  = sprintf( '%1$s/%2$s.json', $dir, $slug );

			$defaults = array(

				'fluid' => false,
				'name'  => $name,
				'slug'  => $slug,
				'file'  => $file,
				'json'  => $json

			);

			$options = array_merge( $defaults, $options );

			$this->_module_types[$options['slug']] = $options;

		}

		/**
		 * Documentation please
		 */
		public function get_modules() {

			if( ! is_array($this->_modules) ) {

				$this->set_modules( get_field('modules') );

			}

			return $this->_modules;
		}

		/**
		 * Documentation please
		 */
		public function set_modules( $modules = array() ) {

			if( is_array($modules) ) {

				foreach($modules as &$module) {

					$defaults = array( 
						'module_id' => mt_rand()
					);

					$module = array_merge(
						$defaults,
						$module
					);

					$module['module_id'] = 'sc-module-' . $module['module_id'];

				}

				$this->_modules = $modules;
			
			} else {

				$this->_modules = array();

			}

		}

		/**
		 * Documentation please
		 */
		public function get_module_types() {

			return $this->_module_types;

		}

		/**
		 * Documentation please
		 */
		public function get_module_type($module) {

			$module_name = $module['acf_fc_layout'];

			$slug = str_replace('_', '-', $module_name);

			if(isset($this->_module_types[$slug])) {

				return $this->_module_types[$slug];

			}

			return false;

		}

		/**
		 * Documentation please
		 */
		public function get_module_size($module) {

			$size = (object) array(
				'xs' => 12,
				'sm' => 12,
				'md' => 12,
				'lg' => 12,
			);

			if( isset( $module['size'] ) ) {

				$size->md = (int) $module['size'];
				$size->lg = (int) $module['size'];

			}

			if( has_filter( 'sc_module_size' ) ) {

				$size = apply_filters( 'sc_module_size', $module, $size );
				
			}

			return $size;

		}

		/**
		 * Documentation please
		 */
		public function display_modules() {

			$modules         = $this->get_modules();
			$container_open  = $this->container_open;
			$container_close = $this->container_close;
			$row_fill        = 0;
			$open            = false;

			foreach( $modules as $module ) {

				$markup = $this->get_module_markup( $module );
				$type   = $this->get_module_type( $module );
				$size   = $this->get_module_size( $module );

				if ( true === $type['fluid'] ) {

					$row_fill = 0;

					if( $open ) {

						echo $container_close;
						$open = false;

					}

					printf('
						<div class="container-fluid sc-module-container-fluid">
							<div class="row">
								%s
							</div>
						</div>',
						$markup
					);

				} else {

					if( 0 === $row_fill ) {

						if( $open ) {

							echo $container_close;
							$open = false;

						}

						echo $container_open;
						$open = true;
						$row_fill = $size->md;

					} else {

						$row_fill += $size->md;

					}

					echo $markup;

					if( 12 <= $row_fill ) {

						echo $container_close;
						$open = false;
						$row_fill = 0;

					}

				}

			}

			if( $open ) {

				echo $container_close;

			}

		}

		/**
		 * Documentation please
		 */
		public function get_module_markup($module) {

			$module_type = $this->get_module_type($module);
			$module_size = $this->get_module_size($module);

			if( ! isset($module_type['file']) ) {

				return false; // <-- maybe throw a custom error here?

			}

			ob_start();

			include $module_type['file'];

			$module_markup = ob_get_clean();

			return sprintf('
				<div class="col-xs-%d col-sm-%d col-md-%d col-lg-%d">
					%s
				</div>',
				$module_size->xs,
				$module_size->sm,
				$module_size->md,
				$module_size->lg,
				$module_markup
			);

		}

		/**
		 * Get the directory to save and load ACF json from. If the directory does
		 * not exist it will be created.
		 * 
		 * @return string|boolean The directory or false on failure.
		 */
		public function get_acf_load_point() {

			$upload_dir = wp_upload_dir();

			if( ! isset($upload_dir['basedir']) ) {

				return false;
			
			}

			$acf_load_point = $upload_dir['basedir'] . '/sc-modules';

			if( is_dir( $acf_load_point ) ) {

				return $acf_load_point;

			}

			if ( ! mkdir( $acf_load_point, 0775, true ) ) {

				return false;

			}

			return $acf_load_point;

		}

		public function get_layouts_load_point() {

			$theme_dir    = get_stylesheet_directory();
			$layouts_dirs = array(
				$theme_dir . '/library/sc-modules/layouts',
				SC_MODULES_DIR_LAYOUTS
			);

			foreach( $layouts_dirs as $layouts_dir ) {

				if( is_dir( $layouts_dir ) ) {

					return $layouts_dir;

				}
				
			}

			return false;

		}

		public function get_module_load_point( $module_name ) {

			$theme_dir   = get_stylesheet_directory();
			$module_slug = sanitize_title($module_name);
			$module_dirs = array(
				$theme_dir . '/library/sc-modules/modules/' . $module_slug,
				SC_MODULES_DIR_MODULES . '/' . $module_slug
			);

			foreach( $module_dirs as $module_dir ) {

				if( is_dir( $module_dir ) ) {

					return $module_dir;

				}
				
			}

			return false;

		}

	}

}
