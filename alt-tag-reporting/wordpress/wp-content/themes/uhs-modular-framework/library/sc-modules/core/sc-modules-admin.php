<?php if (! defined('ABSPATH')) die('No direct access allowed');

if( ! class_exists('SC_Modules_Admin') ) {

	class SC_Modules_Admin {

		function __construct() {

			if( is_admin() ) {

				add_action( 
					'admin_enqueue_scripts', 
					array(
						$this,
						'enqueue_admin_scripts'
					)
				);
				
				add_action(
					'add_meta_boxes',
					array(
						$this,
						'add_module_browser'
					)
				);

				add_filter(
					'acf/load_value/name=modules',
					array(
						$this,
						'add_default_text_module'
					),
					10,
					3
				);

			}

		}

		public function enqueue_admin_scripts() {

			$template_directory_uri = get_template_directory_uri();
			$assets = $template_directory_uri . '/library/sc-modules/assets/admin';

			// enqueue css
			//
			wp_enqueue_style(
				'sc-modules-browser', 
				$assets . '/css/sc-modules-browser.css'
			);

			// enqueue js
			//
			wp_enqueue_script(
				'sc-modules-browser', 
				$assets . '/js/sc-modules-browser.js',
				array( 'jquery' ), 
				'1.0.0',
				true
			);

		}

		public function add_module_browser() {

			add_meta_box( 
				'sc-module-browser', 
				'Page Layout Preview', 
				function() {}, 
				'page', 
				'side', 
				'default'
			);

		}

		public function add_default_text_module( $value, $post_id, $field ) { 

			if( is_array( $value ) ) {

				return $value;

			}

			$value = array(
				array(
					'acf_fc_layout'         => 'text',
					'field_text_layout_001' => '',
					'text_layout_000_size'  => 12,
				)
			);

			return $value;

		}

	}

}
