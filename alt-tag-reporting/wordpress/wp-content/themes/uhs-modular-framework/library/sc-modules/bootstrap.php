<?php if (! defined('ABSPATH')) die('No direct access allowed');
/*
*   Plugin Name: SC Modules
*   Plugin URI: http://www.solidcactus.com
*   Description: Dynamic module layout for pages.
*   Author: Solid Cactus
*   Version: 1.0.0
*   Author URI: http://www.solidcactus.com
*   Contributing Author: Richard Stanley
*/

include('core/sc-modules.php');
include('core/sc-modules-admin.php');
include('core/sc-modules-json-builder.php');

define( 'SC_MODULES_DIR_LAYOUTS', __DIR__ . '/layouts' );
define( 'SC_MODULES_DIR_MODULES', __DIR__ . '/modules' );
define( 'SC_MODULES_DIR_JSON',    __DIR__ . '/json'    );
