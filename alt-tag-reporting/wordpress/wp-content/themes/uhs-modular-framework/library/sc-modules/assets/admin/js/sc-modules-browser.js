
(function($) {
	
	$(document).ready(function() {

		if($('div[data-name="modules"]').length) {

			setupColumnEvents();
			updatePreview();
			setInterval(updatePreview, 1000);

			/*
			// https://www.advancedcustomfields.com/resources/adding-custom-javascript-fields
			// The remove action seems to have a bug so resorting to hackish setInterval 
			// approach seen above for updating the module preview
			//
			acf.add_action('load', function( $el ){
				console.log('update');
				updatePreview();
			});

			acf.add_action('remove', function( $el ){
				console.log('remove');
				updatePreview();
			});

			acf.add_action('append', function( $el ){
				console.log('append');
				updatePreview();
			});

			acf.add_action('sortstop', function( $el ){
				console.log('sortstop');
				updatePreview();
			});
			//*/

		} else {

			$('#sc-module-browser').remove();

		}

	});

	function setupColumnEvents() {
		
		$('.sc-module-browser-column').live('click', function(e) {
			
			var index = parseInt($(this).find('span').html()) - 1;

			getLayouts().addClass('-collapsed closed');

			$(getLayouts()[index]).removeClass('-collapsed closed');

			getLayouts()[index].scrollIntoView(true);

		});

	}

	function updatePreview() {

		var rows = getModuleRows();
		var preview, row, i, ii, index;

		$('#sc-module-browser .sc-module-browser-preview').remove();

		preview = $('<div class="sc-module-browser-preview"></div>');

		index = 1;

		for(i = 0; i < rows.length; i++) {

			row = $('<div class="sc-module-browser-row"></div>');

			for(ii = 0; ii < rows[i].length; ii++) {

				row.append(
					$('<div></div>')
						.addClass('sc-module-browser-column')
						.addClass('sc-module-browser-column-' + rows[i][ii])
						.html('<span>' + index + '</span>')
				);
				
				index++;

			}

			preview.append(row);

		}

		$('#sc-module-browser .inside').append(preview);

	}

	function getModuleSizes() {

		var sizes = [];

		getLayouts().each(function(i, layout) {

			var input, size;

			input = $(layout).find('div[data-name="size"] select');
			size  = $(input).val();

			sizes.push(parseInt(size));

		});

		return sizes;

	}

	function getModuleRows() {

		var sizes = getModuleSizes();
		var rows  = [];
		var fill  = 0;
		var size;

		for(var c = 0; c < sizes.length; c++) {

			size  = sizes[c];
			fill += size;

			if(rows.length == 0 || fill > 12) {

				fill = size;
				rows.push([]);

			}

			rows[rows.length - 1].push(size);

		}

		return rows;

	}

	function getLayouts() {

		return $('div[data-name="modules"] .values .layout');

	}

})(jQuery);
