<?php if (! defined('ABSPATH')) die('No direct access allowed');

if( ! class_exists( 'SC_Relevanssi_Updater' ) ) {

	class SC_Relevanssi_Updater {

		public $non_indexed_pages; 

		function __construct( $post_id ) {

			$this->non_indexed_pages = [
				'thank-you',
				'thankyou',
				'thanks',
				'site-map',
				'sitemap'
			];

      add_action( 'sc_relevanssi_updater_cron', array( $this, 'update_relevanssi') );

      if ( ! wp_next_scheduled( 'sc_relevanssi_updater_cron' ) ) {
        wp_schedule_event( time(), 'hourly', 'sc_relevanssi_updater_cron' );
      }

		}

		public function update_relevanssi() {

			$this->update_relevanssi_settings();
			$this->exclude_page_from_relevansssi_index();
			$this->build_relevansssi_index();

		}
    
    public function update_relevanssi_settings() {

			$file          = SC_SEARCH_DIR_JSON . '/relevanssi-settings.json';
			$settings      = @file_get_contents( $file );
			$import_exists = function_exists( 'relevanssi_import_options' );

			if( false === $settings || ! $import_exists ) {
				return;
			}

			relevanssi_import_options( $settings );

		}
		
		public function exclude_page_from_relevansssi_index() {

			global $post;

			$query = new WP_Query(
				array(
					'post_type'     => 'page',
					'post_name__in' =>  $this->non_indexed_pages
				)
			);

			if( ! $query->have_posts() ) {
				return;
			}

			while( $query->have_posts() ) {
				
				$query->the_post();

				update_post_meta( get_the_ID(), '_relevanssi_hide_post', 'on' );
			}

			wp_reset_postdata();

		}

		public function build_relevansssi_index() {

			if( ! function_exists( 'relevanssi_build_index' ) ) {
				return;
			}

			relevanssi_build_index();

		}

	}

}
