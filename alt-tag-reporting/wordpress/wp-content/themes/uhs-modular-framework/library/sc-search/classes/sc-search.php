<?php if (! defined('ABSPATH')) die('No direct access allowed');

if( ! class_exists( 'SC_Search' ) ) {

	class SC_Search {

    CONST META_PLAIN_TEXT_CONTENT = 'sc-search-plain-text-content';
    CONST META_LAST_SEARCH_UPDATE = 'sc-search-last-search-update';

    protected $_post_id;
    protected $_plain_text_segments;

		function __construct( $post_id ) {

      $this->set_post_id( $post_id );
      
		}

    public function get_plain_text_content() {
      
      $content = get_post_meta( $this->_post_id, self::META_PLAIN_TEXT_CONTENT, true );

      if( empty( $content ) ) {

        $content = $this->build_plain_text_content();

      }

      return $content;

    }

    /**
     * 
     * 
     * @return void
     */
    public function build_plain_text_content() {
      
      $this->_plain_text_segments = array();
    
      if( have_rows( 'modules', $this->_post_id ) ) {
    
        while( have_rows( 'modules', $this->_post_id ) ) {
    
          the_row();
    
          $row = get_row();

          if( is_array( $row ) ) {

            foreach( $row as $sub_key => $sub_value ) {
    
              $this->build_plain_text_field_content( $key, $value );
      
            }

          }
    
        }
    
      }
    
      $content = implode( '...', $this->_plain_text_segments );

      if( empty( $content ) ) {
    
        $post    = get_post( $this->_post_id );
        $content = $post->post_content;
    
      }

      $content = apply_filters( 'the_content', $content );
      $content = trim( strip_tags( $content ) );

      update_post_meta( $this->_post_id, self::META_PLAIN_TEXT_CONTENT, $content );
      update_post_meta( $this->_post_id, self::META_LAST_SEARCH_UPDATE, time() );
  
      return $content;

    }

    private function build_plain_text_field_content( $key, $value ) {
    
      $text_types   = ['text', 'textarea', 'wysiwyg'];
      $field_object = get_field_object( $key );
      $field_type   = $field_object['type'];
    
      if( in_array( $field_type, $text_types ) ) {
    
        $text_segment = trim( strip_tags( $value ) );
    
        if( ! empty( $text_segment ) ) {
    
          $this->_plain_text_segments[] = $text_segment;
        
        }
    
      }
    
      if( have_rows( $key, $this->_post_id ) ) {
        
        while( have_rows( $key, $this->_post_id  ) ) {
    
          the_row();
          
          $row = get_row();

          if( is_array( $row ) ) {

            foreach( $row as $sub_key => $sub_value ) {
    
              $this->build_plain_text_field_content( $sub_key, $sub_value );
      
            }

          }
    
        }
    
      }
    
    }

    public function set_post_id( $post_id ) {
      
      $this->_post_id = $post_id;

    }

	}

}
