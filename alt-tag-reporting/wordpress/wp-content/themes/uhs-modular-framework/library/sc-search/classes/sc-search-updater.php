<?php if (! defined('ABSPATH')) die('No direct access allowed');

if( ! class_exists( 'SC_Search_Updater' ) ) {

	class SC_Search_Updater {

		function __construct() {

      add_action( 'save_post', array( $this, 'update_search_content' ), 10, 1 );
      add_action( 'sc_search_updater_cron', array( $this, 'update_search_content_batch') );

      if ( ! wp_next_scheduled( 'sc_search_updater_cron' ) ) {
        wp_schedule_event( time(), 'hourly', 'sc_search_updater_cron' );
      }

    }
    
    public function update_search_content( $post_id ) {

      $search = new SC_Search( $post_id );
      $search->build_plain_text_content();

    }

    public function update_search_content_batch() {

      global $post;

      $batch_size = 50;

      $post_types = [ 'post', 'page' ];

      // Query posts that have not had search content generated yet
      $query = new WP_Query(
        array(
          'posts_per_page' => $batch_size,
          'post_type'      => $post_types,
          'meta_query'     => array(
            'relation' => 'OR',
            array(
              'key'     => SC_Search::META_LAST_SEARCH_UPDATE,
              'value'   => '',
              'compare' => 'NOT EXISTS'
            )
          )
        )
      );

      // Generate search content for queried posts
      if( $query->have_posts() ) {
        
        while( $query->have_posts() ) {
  
          $query->the_post();

          $this->update_search_content( $post->ID );

          $batch_size--;
  
        }

        wp_reset_postdata();

      }

      // Only continue if batch size hasn't been reduced to 0
      if( 1 <= $batch_size ) {
  
        // Query posts by last date that their search content was generated
        $query = new WP_Query(
          array(
            'posts_per_page' => $batch_size,
            'post_type'      => $post_types,
            'order_by'       => 'meta_value_num',
            'order'          => 'ASC',
            'meta_key'       => SC_Search::META_LAST_SEARCH_UPDATE,
          )
        );
  
        // Generate search content for queried posts
        if( $query->have_posts() ) {
          
          while( $query->have_posts() ) {
    
            $query->the_post();

            $this->update_search_content( $post->ID );
    
          }
  
          wp_reset_postdata();
  
        }

      }

    }

	}

}
