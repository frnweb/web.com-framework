<?php if (! defined('ABSPATH')) die('No direct access allowed');

define( 'SC_SEARCH_DIR',      __DIR__           );
define( 'SC_SEARCH_DIR_JSON', __DIR__ . '/json' );

include('classes/sc-search.php');
include('classes/sc-search-updater.php');
include('classes/sc-relevanssi-updater.php');

new SC_Search_Updater;
new SC_Relevanssi_Updater( get_the_id() );