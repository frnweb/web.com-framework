<?php

class SC_Provisioner {

	CONST STATUS_OPTION   = 'sc-provisioner';
	CONST STATUS_COMPLETE = 'complete';

	public $pages;
	public $menus;
	public $settings;

	private static $_instance = null;

	function __construct() {

		// Default pages
		//
		$this->pages = array(

			'home' => array(
				'title' => 'Home'
			),

		);

		// Default menus
		//
		$this->menus = array(

			'header_menu' => array(
				'title' => 'Header Menu',
				'items' => array(),
			),

			'footer_menu' => array(
				'title' => 'Footer Menu',
				'items' => array(),
			),

		);

		// Default options
		// Option Reference https://codex.wordpress.org/Option_Reference
		//
		$this->settings = array(

			'blogdescription'              => 'Official Website',
			'default_pingback_flag'        => 0,
			'default_ping_status'          => 'closed',
			'default_comment_status'       => 'closed',
			'require_name_email'           => 1,
			'comment_registration'         => 1,
			'close_comments_for_old_posts' => 1,
			'close_comments_days_old'      => 0,
			'comments_notify'              => 1,
			'moderation_notify'            => 1,
			'comment_moderation'           => 1,
			'comment_whitelist'            => 1,
			'blog_public'                  => 0,

		);

		if( $this->safe_to_provision() ) {

			add_action('init', array($this, 'provision'));
		}

	}

	public static function singleton() {

		if( self::$_instance == null ) {

			self::$_instance = new self;

		}

		return self::$_instance;

	}

	public function safe_to_provision() {

		if( ! is_admin() ) {

			return false;
		}

		$status = get_option( self::STATUS_OPTION );

		if( $status == self::STATUS_COMPLETE ) {

			return false;
		}

		return true;

	}

	public function provision() {

		if( ! $this->safe_to_provision() ) {

			return false;
		}

		update_option( self::STATUS_OPTION, self::STATUS_COMPLETE );

		$this->delete_sample_post();
		$this->delete_sample_page();
		$this->add_pages();
		$this->add_menus();
		$this->update_settings();

	}

	private function delete_sample_post() {

		$posts = get_posts(array('post_type' => 'post'));

		foreach($posts as $post) {

			if($post->post_title == 'Hello world!') {

				wp_delete_post($post->ID, true);

			}

		}

		wp_reset_postdata();
	}

	private function delete_sample_page() {

		$pages = get_posts(array('post_type' => 'page'));

		foreach($pages as $page) {

			if($page->post_title == 'Sample Page') {

				wp_delete_post($page->ID, true);

			}

		}

		wp_reset_postdata();

	}

	private function add_pages() {

		if( ! is_array( $this->pages ) ) {

			return false;
		}

		$all_pages   = get_posts( array('post_type' => 'page') );
		$page_titles = array();

		foreach( $all_pages as $page ) {

			$page_titles[ $page->post_title ] = true;

		}

		foreach( $this->pages as $page ) {

			if( isset( $page_titles[ $page[ 'title' ] ] ) ) {

				continue;

			}

			$page = (object) $page;

			$post = array(
				'post_title'  => $page->title,
				'post_status' => 'publish',
				'post_type'   => 'page'
			);

			wp_insert_post($post);

		}

	}

	private function add_menus() {

		if( ! is_array( $this->menus ) ) {

			return false;
		}

		$locations = get_theme_mod('nav_menu_locations');

		foreach( $this->menus as $menu_location => $menu_options ) {

			$menu_options = (object) $menu_options;
			$menu_exists  = wp_get_nav_menu_object( $menu_options->title );

			if( ! $menu_exists ) {

				$menu_id = wp_create_nav_menu( $menu_options->title );

				if( isset( $menu_options->items ) ) {

					$this->add_menu_items( $menu_id, $menu_options->items );
				
				}

				wp_update_nav_menu_object( $menu_id );

				$locations[$menu_location] = $menu_id;

			}

		}

		set_theme_mod( 'nav_menu_locations', $locations );

	}

	private function add_menu_items( $menu_id, $items ) {

		if( ! is_array( $items ) ) {

			return false;

		}

    $defaults = array(
      'menu-item-db-id'       => 0,
      'menu-item-object-id'   => 0,
      'menu-item-object'      => '',
      'menu-item-parent-id'   => 0,
      'menu-item-position'    => 0,
      'menu-item-type'        => 'custom',
      'menu-item-title'       => '',
      'menu-item-url'         => '',
      'menu-item-description' => '',
      'menu-item-attr-title'  => '',
      'menu-item-target'      => '',
      'menu-item-classes'     => '',
      'menu-item-xfn'         => '',
      'menu-item-status'      => 'publish',
    );

		foreach($items as $item) {

			$item_options = array_merge($defaults, array(

				'menu-item-title' => $item['title'], 
				'menu-item-url'   => $item['url']

			));

			wp_update_nav_menu_item( $menu_id, 0, $item_options );

		}

	}

	private function update_settings() {

		foreach ($this->settings as $key => $value) {

			update_option( $key, $value );
		}

		$home_page = $this->get_home_page();

		if( $home_page !== false ) {

			update_option( 'show_on_front', 'page' );
			update_option( 'page_on_front', $home_page->ID );

		}

	}

	private function get_home_page() {

		$pages = get_posts( array('post_type' => 'page') );

		$home_page = false;

		foreach( $pages as $page ) {

			if( $page->post_title == 'Home' ) {

				$home_page = $page;

				break;

			}

		}

		wp_reset_postdata();

		return $home_page;

	}

}
