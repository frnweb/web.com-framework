<?php if (! defined('ABSPATH')) die('No direct access allowed');
/*
*   Plugin Name: SC Provisioner
*   Plugin URI: http://www.solidcactus.com
*   Description: Dynamicly populate initial site content.
*   Author: Solid Cactus
*   Version: 1.0.0
*   Author URI: http://www.solidcactus.com
*   Contributing Author: Richard Stanley
*/

include('core/sc-provisioner.php');