<?php if (! defined('ABSPATH')) die('No direct access allowed');

if( ! wp_next_scheduled( 'sc_gravity_forms_cleanup' ) ) {

	wp_schedule_event( time(), 'hourly', 'sc_gravity_forms_cleanup' );
	
}

add_action( 'sc_gravity_forms_cleanup', 'sc_gravity_forms_cleanup' );

function sc_gravity_forms_cleanup() {

	if( ! is_callable( 'GFAPI::get_entries' ) ) {
		return;
	}

	$entries = GFAPI::get_entries( 0, array(
		'start_date' => date( 'Y-m-d H:i:s', 0 ),
		'end_date'   => date( 'Y-m-d H:i:s', strtotime( '-30 days' ) )
	));

	foreach( $entries as $entry ) {
		
		GFAPI::delete_entry( $entry['id'] );

	}

}
