<?php if (! defined('ABSPATH')) die('No direct access allowed');

if(! class_exists('SC_Customizer'))
{
  class SC_Customizer {

    protected static $_customizers = [];

    function __construct() {

      //

    }

    /**
     *
     */
    public static function enqueue_customizer( $customizer ) {

      self::$_customizers[] = $customizer;

    }

    /**
     *
     */
    public static function process_queue( $wp_customizer ) {

      $priority = 0;

      foreach( self::$_customizers as $customizer ) {

        $customizer = self::get_customizer_instance( $customizer );

        if( false !== $customizer ) {

          $customizer->register( $wp_customizer, $priority++ );

        }

      }

    }

    /**
     *
     */
    public static function get_customizer_instance( $name ) {

      $name = ucwords(str_replace(array('-', '_'), array(' '), $name));
      $name = str_replace(' ', '_', 'SC_Customizer_' . $name);

      if( class_exists($name) ) {

        return new $name;

      }

      return false;

    }

    /**
     *
     */
    public static function remove_defaults( $wp_customize, $sections = array() ) {

      foreach ($sections as $section) {

        $wp_customize->remove_section( $section );
        $wp_customize->remove_panel( $section );

      }

    }

    /**
     *
     */
    public static function register_text( 
      $wp_customize, 
      $section, 
      $option_name, 
      $label, 
      $options = array() ) {

      $setting_options = isset( $options['setting_options'] ) ? $options['setting_options'] : array();
      $control_options = isset( $options['control_options'] ) ? $options['control_options'] : array();

      $wp_customize->add_setting (
        $option_name, 
        array_merge(
          array (
            'default'   => '', 
            'type'      => 'theme_mod',
            'transport' => 'postMessage',
            'sanitize_callback' => 'sanitize_text_field'
          ),
          $setting_options
        )
      );

      $wp_customize->add_control (
        $option_name,
        array_merge(
          array (
            'label'   => $label,
            'section' => $section,
            'type'    => 'text',
          ),
          $control_options
        )
      );

    }

    /**
     *
     */
    public static function register_textarea( 
      $wp_customize, 
      $section, 
      $option_name, 
      $label, 
      $options = array() ) {

      $setting_options = isset( $options['setting_options'] ) ? $options['setting_options'] : array();
      $control_options = isset( $options['control_options'] ) ? $options['control_options'] : array();

      $wp_customize->add_setting (
        $option_name, 
        array_merge(
          array (
            'default'   => '', 
            'type'      => 'theme_mod',
            'transport' => 'postMessage',
            'sanitize_callback' => 'sanitize_textarea_field'
          ),
          $setting_options
        )
      );

      $wp_customize->add_control (
        $option_name,
        array_merge(          
          array (
            'label'   => $label,
            'section' => $section,
            'type'    => 'textarea',
          ),
          $control_options
        )
      );

    }

    /**
     *
     */
    public static function register_checkbox( 
      $wp_customize, 
      $section, 
      $option_name, 
      $label, 
      $options = array() ) {

      $setting_options = isset( $options['setting_options'] ) ? $options['setting_options'] : array();
      $control_options = isset( $options['control_options'] ) ? $options['control_options'] : array();

      $wp_customize->add_setting (
        $option_name,
        array_merge(
          array(
            'default'   => '',
            'type'      => 'theme_mod',
            'transport' => 'postMessage'
          ),
          $setting_options
        )
      );

      $wp_customize->add_control (
        $option_name,
        array_merge(
          array(
            'label'   => $label,
            'section' => $section,
            'type'    => 'checkbox',
          ),
          $control_options
        )
      );

    }

    /**
     *
     */
    public static function register_radio( 
      $wp_customize, 
      $section, 
      $option_name, 
      $label, 
      $choices,
      $options = array() ) {

      $setting_options = isset( $options['setting_options'] ) ? $options['setting_options'] : array();
      $control_options = isset( $options['control_options'] ) ? $options['control_options'] : array();

      $wp_customize->add_setting (
        $option_name,
        array_merge(
          array(
            'default'   => '',
            'type'      => 'theme_mod',
            'transport' => 'postMessage'
          ),
          $setting_options
        )
      );

      $wp_customize->add_control (
        $option_name,
        array_merge(
          array(
            'label'   => $label,
            'section' => $section,
            'choices' => $choices,
            'type'    => 'radio',
          ),
          $control_options
        )
      );

    }

    /**
     *
     */
    public static function register_image(
      $wp_customize,
      $section,
      $option_name,
      $label,
      $options = array() ) {

      $defaults = array(

      );

      $options = array_merge($defaults, $options);

      $options['section'] = $section;
      $options['label']   = $label;

      $wp_customize->add_setting(
        $option_name,
        array(
          'default'   => '',
          'type'      => 'theme_mod',
          'transport' => 'postMessage'
          )
      );

      $wp_customize->add_control(
        new WP_Customize_Image_Control(
          $wp_customize,
          $option_name,
          $options
        )
      );

    }

    /**
     *
     */
    public static function register_cropped_image(
      $wp_customize,
      $section,
      $option_name,
      $label,
      $options = array() ) {

      $defaults = array(
        'flex_width'  => true,
        'flex_height' => false,
        'width'       => 300,
        'height'      => 300
      );

      $options = array_merge($defaults, $options);

      $options['section'] = $section;
      $options['label']   = $label;

      $wp_customize->add_setting(
        $option_name,
        array(
          'default'   => '',
          'type'      => 'theme_mod',
          'transport' => 'postMessage'
          )
      );

      $wp_customize->add_control(
        new WP_Customize_Cropped_Image_Control(
          $wp_customize,
          $option_name,
          $options
        )
      );

    }

    /**
     *
     */
    public static function register_color(
      $wp_customize,
      $section,
      $option_name,
      $label,
      $options = array() ) {

      $defaults = array(

      );

      $options = array_merge( $defaults, $options );

      $options['section'] = $section;
      $options['label']   = $label;

      $wp_customize->add_setting( $option_name, $options );

      $wp_customize->add_control(
        new WP_Customize_Color_Control(
          $wp_customize,
          $option_name,
          array(
            'label'    => $label,
            'section'  => $section,
            'settings' => $option_name
          )
        )
      );

    }

  }

}
