<?php if (! defined('ABSPATH')) die('No direct access allowed');

if(! class_exists('SC_Customizer_Settings'))
{
	class SC_Customizer_Settings {

    public function register($wp_customize, $priority) {

      self::register_section($wp_customize, $priority);

    }

		public function register_section($wp_customize, $priority) {

			$section = 'sc_settings_section';

			$wp_customize->add_section($section , array(
				'title'    => 'Settings',
				'priority' => $priority
			));

			SC_Customizer::register_checkbox($wp_customize, $section, 'sc_enable_author_pages', 'Enable Author Pages');
			SC_Customizer::register_checkbox($wp_customize, $section, 'sc_enable_url_guessing', 'Enable URL Guessing');
			SC_Customizer::register_checkbox($wp_customize, $section, 'sc_enable_emojis',       'Enable Emojis');

		}

	}

}

