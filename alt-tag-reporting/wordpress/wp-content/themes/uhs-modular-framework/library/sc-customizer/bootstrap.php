<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */

// Include the core customizer class
//
include('core/sc-customizer.php');

// Include individual customizer panel classes
//
include('customizers/notices.php');
include('customizers/settings.php');

if(! function_exists('sc_init_customizer')) {

	function sc_init_customizer( $wp_customize ) {

		// Enqueue customizer panel classes. They will be displayed in the order 
		// they are enqueued.
		//
		SC_Customizer::enqueue_customizer( 'notices' );
		SC_Customizer::enqueue_customizer( 'settings' );
		
		// Process the enqueued customizer panel classes.
		//
		SC_Customizer::process_queue( $wp_customize );

		// Remove default wordpress customizer panels that we don't need.
		//
		SC_Customizer::remove_defaults( $wp_customize, array(
			'themes',
			'title_tagline',
			'colors',
			'header_image',
			'background_image',
			'static_front_page',
			'custom_css'
		) );

	}

}

add_action('customize_register', 'sc_init_customizer', 20);
