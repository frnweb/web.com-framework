<?php if (! defined('ABSPATH')) die('No direct access allowed');

add_action( 'acf/include_field_types', 'sc_include_custom_acf_fields' );

if( ! function_exists( 'sc_include_custom_acf_fields' ) ) {

  function sc_include_custom_acf_fields( $version ) {

    include_once('fields/acf-field-gravity-forms.php');
    include_once('fields/acf-field-ninja-forms.php');
    include_once('fields/acf-field-unique-id.php');

  }

}

//------------------------------------------------------------------------------

add_filter( 'acf/load_field/name=form_plugin', 'sc_form_module_choices_filter' );

if( ! function_exists( 'sc_form_module_choices_filter' ) ) {

  function sc_form_module_choices_filter($field)
  {
    
    $field['choices'] = array();

    if( class_exists( 'Ninja_Forms' ) ) {

      $field['choices']['Ninja Forms'] = 'Ninja Forms';

    }

    if( class_exists( 'RGFormsModel' ) ) {

      $field['choices']['Gravity Forms'] = 'Gravity Forms';

    }

    return $field;

  }

}
