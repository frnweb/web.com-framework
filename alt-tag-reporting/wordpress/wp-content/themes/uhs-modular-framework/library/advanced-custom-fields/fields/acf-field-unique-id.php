<?php
/**
 * Advanced Custom Fields: Unique Id Field
 *
 */

class acf_field_unique_id extends acf_field {

  function __construct( ) {

    $this->name     = 'unique_id_field';
    $this->label    = 'Unique Id';
    $this->category = 'basic';

    parent::__construct();

  }

  /*
   * render_field_settings()
   *
   * Create extra field settings. These are visible when editing a field.
   *
   * @type  action
   * @since 3.6
   * @date  23/01/13
   *
   * @param  $field (array) the $field being edited
   * @return n/a
   */
  function render_field_settings( $field ) {

    //

  }

  /*
   * render_field()
   *
   * Create the HTML interface for your field
   *
   * @param $field (array) the $field being rendered
   *
   * @type  action
   * @since 3.6
   * @date  23/01/13
   *
   * @param $field (array) the $field being edited
   * @return  n/a
   */
  function render_field( $field ) {

    $name  = $field['name'];
    $value = $field['value'];

    printf('
      <input type="text" name="%s" value="%s" />
      <style type="text/css">
        .acf-field-unique-id-field {
          display:none;
        }
      </style>',
      esc_attr($name),
      esc_attr($value)
    );

  }

  function update_value( $value, $post_id, $field ) {
    
    if( empty($value) ) {

      $value = mt_rand(1000, mt_getrandmax());

    }

    return $value;
    
  }

  /*
   * load_value()
   *
   * This filter is applied to the $value after it is loaded from the db.
   *
   * @type  filter
   * @since 3.6
   * @date  23/01/13
   *
   * @param $value (mixed) the value found in the database
   * @param $post_id (mixed) the $post_id from which the value was loaded
   * @param $field (array) the field array holding all the field options
   * @return  $value
   */
  function load_value( $value, $post_id, $field ) {
    
    if( ! $value ) {

      return false;

    }

    if( 'null' == $value ) {

      return false;

    }

    return $value;
    
  }

  /*
   * format_value()
   *
   * This filter is appied to the $value after it is loaded from the db and 
   * before it is returned to the template
   *
   * @type  filter
   * @since 3.6
   * @date  23/01/13
   *
   * @param $value (mixed) the value which was loaded from the database
   * @param $post_id (mixed) the $post_id from which the value was loaded
   * @param $field (array) the field array holding all the field options
   *
   * @return  $value (mixed) the modified value
   */
  function format_value( $value, $post_id, $field ) {

    if( ! $value ) {

      return false;

    }

    if( 'null' == $value ) {

      return false;

    }

    return $value;

  }

}

new acf_field_unique_id();
