(function($) {

	// Close all sidebar admin menus
	$('.wp-has-current-submenu').removeClass('wp-has-current-submenu');

	// Open the sidebar appearance admin menu
	$('#adminmenu .menu-top a[href="themes.php"]')
		.parent()
		.removeClass('wp-not-current-submenu')
		.addClass('wp-has-current-submenu wp-menu-open');

	// Highlight the snippet submenu item
	$('#adminmenu .wp-submenu a[href="edit.php?post_type=sc-snippet"')
		.addClass('current')
		.parent()
		.addClass('current');

}(jQuery));