<?php if (! defined('ABSPATH')) die('No direct access allowed');

if( ! class_exists( 'SC_Snippet' ) ) {

  class SC_Snippet {

    CONST POST_TYPE = 'sc-snippet';

    function __construct() {
      
			add_action( 'init', array( $this, 'register_post_type' ) );

			add_action( 'admin_init', array( $this, 'add_submenu_page' ) );
			add_action( 'admin_head', array( $this, 'open_admin_menu' ) );
			
			add_filter( 'manage_edit-sc-snippet_columns', array( $this, 'setup_admin_columns' ) ) ;
			add_action( 'manage_sc-snippet_posts_custom_column', array( $this, 'populate_admin_columns' ), 10, 2 );

			add_shortcode( 'sc-snippet', array( $this, 'render_snippet' ) );

		}

    public function register_post_type() {

      $singular = 'Snippet';
			$plural   = 'Snippets';

			$labels   = array(
				'name'               => $plural,
				'singular_name'      => $singular,
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add'     . ' ' .  $singular,
				'edit_item'          => 'Edit'    . ' ' .  $singular,
				'new_item'           => 'New'     . ' ' .  $singular,
				'all_items'          => 'All'     . ' ' .  $plural,
				'view_item'          => 'View'    . ' ' .  $singular,
				'search_items'       => 'Search'  . ' ' .  $plural,
				'not_found'          => sprintf('No %s found',          $plural),
				'not_found_in_trash' => sprintf('No %s found in trash', $plural),
				'parent_item_colon'  => '',
				'menu_name'          => $plural
			);

      $args = array(
        'hierarchical'        => false,
        'description'         => 'Custom post type for snippets.',
        'supports'            => array( 'title', 'editor' ),
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => false,
        'show_in_nav_menus'   => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'has_archive'         => false,
        'query_var'           => false,
        'can_export'          => true,
        'rewrite' 			      => null,
        'capability_type'     => 'post',
        'menu_position'       => 22,
        'menu_icon'           => 'data:image/svg+xml;base64,' . base64_encode( '<svg width="20" height="20" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path fill="#a0a5aa" d="M960 896q26 0 45 19t19 45-19 45-45 19-45-19-19-45 19-45 45-19zm300 64l507 398q28 20 25 56-5 35-35 51l-128 64q-13 7-29 7-17 0-31-8l-690-387-110 66q-8 4-12 5 14 49 10 97-7 77-56 147.5t-132 123.5q-132 84-277 84-136 0-222-78-90-84-79-207 7-76 56-147t131-124q132-84 278-84 83 0 151 31 9-13 22-22l122-73-122-73q-13-9-22-22-68 31-151 31-146 0-278-84-82-53-131-124t-56-147q-5-59 15.5-113t63.5-93q85-79 222-79 145 0 277 84 83 52 132 123t56 148q4 48-10 97 4 1 12 5l110 66 690-387q14-8 31-8 16 0 29 7l128 64q30 16 35 51 3 36-25 56zm-681-260q46-42 21-108t-106-117q-92-59-192-59-74 0-113 36-46 42-21 108t106 117q92 59 192 59 74 0 113-36zm-85 745q81-51 106-117t-21-108q-39-36-113-36-100 0-192 59-81 51-106 117t21 108q39 36 113 36 100 0 192-59zm178-613l96 58v-11q0-36 33-56l14-8-79-47-26 26q-3 3-10 11t-12 12q-2 2-4 3.5t-3 2.5zm224 224l96 32 736-576-128-64-768 431v113l-160 96 9 8q2 2 7 6 4 4 11 12t11 12l26 26zm704 416l128-64-520-408-177 138q-2 3-13 7z"/></svg>' ),
        'labels'              => $labels
      );

      register_post_type( SC_Snippet::POST_TYPE, $args );
      
		}

		public function add_submenu_page() {

			add_submenu_page(
				'themes.php',
				'Snippets',
				'Snippets',
				'manage_options',
				'edit.php?post_type=sc-snippet'
			);

		}

		public function open_admin_menu() {

			$screen = get_current_screen();

			if( SC_Snippet::POST_TYPE === $screen->post_type ) {

				$admin_menu_url = get_template_directory_uri() . '/library/sc-snippet/assets/admin/js/admin-menu.js';

				wp_enqueue_script( 
					'sc-snippet-admin-menu', 
					$admin_menu_url, 
					array( 
						'jquery'
					)
				);

			}

		}

		public function setup_admin_columns( $columns ) {

			$columns = array(
				'cb'        => '<input type="checkbox" />',
				'title'     => 'Title',
				'shortcode' => 'Shortcode',
				'date'      => 'Date'
			);

			return $columns;
		}

		public function populate_admin_columns( $column, $post_id ) {

			global $post;

			if( 'shortcode' === $column ) {

				echo '[sc-snippet name=' . $post->post_name . '] OR [sc-snippet id=' . $post->ID . ']';

			}

		}
		
		public function render_snippet( $atts ) {

			$snippet = null;
			$options = shortcode_atts(array(
				'id'   => null,
				'name' => null
			), $atts);

			if( ! is_null( $options['id'] ) ) {

				$snippet = get_post( $options['id'], OBJECT, 'raw' );

			}

			if( ! is_null( $options['name'] ) && is_null( $snippet ) ) {

				$snippet = get_page_by_path( $options['name'], OBJECT, SC_Snippet::POST_TYPE );

			}

			if( ! is_null( $snippet ) ) {

				return apply_filters( 'the_content', $snippet->post_content );

			}

			return '';

		}

  }

}
