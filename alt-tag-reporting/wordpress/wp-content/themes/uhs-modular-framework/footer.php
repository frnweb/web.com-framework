<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */
?>
			<footer id="footer">
				<div id="footer-top">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="navigation clearfix">
									<?php wp_nav_menu(array(
										'theme_location' => 'footer_menu',
										'container' => 'div',
										'depth' => '1'
									)); ?>
								</div>
							</div><!-- /.col-xs-12 -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</div><!-- /#footer-top -->
				<div id="footer-bottom">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<p class="copyright pull-left"><?php echo get_theme_mod('sc_copyright'); ?></p>
								<p class="byline pull-right"><a target="_blank" href="http://www.solidcactus.com">Responsive Website Design</a> by <a target="_blank" href="http://www.solidcactus.com">Solid Cactus</a>.</p>
							</div><!-- /.col-xs-12 -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</div><!-- /#footer-bottom -->
			</footer><!-- /#footer -->
		</div><!-- /#wrapper -->
		<?php wp_footer(); ?>
	</body>
</html>