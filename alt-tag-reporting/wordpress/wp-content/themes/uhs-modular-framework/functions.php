<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to a function in this file then you should  
 * override it in your child theme functions.php file.
 */

require_once('library/advanced-custom-fields/bootstrap.php');
require_once('library/plugin-update-checker/plugin-update-checker.php');
require_once('library/gravity-forms/bootstrap.php');
require_once('library/sc-customizer/bootstrap.php');
require_once('library/sc-modules/bootstrap.php');
require_once('library/sc-search/bootstrap.php');
require_once('library/sc-provisioner/bootstrap.php');
require_once('library/sc-snippet/bootstrap.php');

Puc_v4_Factory::buildUpdateChecker(
	'https://uhsrepo.eskycity.cloud/themes/uhs-modular-framework/theme.json',
	__FILE__,
	'uhs-modular-framework'
);

//------------------------------------------------------------------------------

add_action('init', 'sc_init');

if( ! function_exists('sc_init') ) {

	/**
	 * Basic theme initialization settings:
	 * 	1. Remove the generator meta tag
	 * 	2. Disable XMLRPC
	 * 	3. Enable excerpt, thumbnail, and menu support
	 * 	4. Disable URL guessing when the option is not turned on in the 'Settings'
	 * 	   customizer panel. It is off by default.
	 */
	function sc_init() {

		// Remove the generator meta tag
		//
		add_filter('the_generator', '__return_false');

		// Disable XML remote procedure calls
		//
		add_filter('xmlrpc_enabled', '__return_false');

		// Add excerpt support to pages
		//
		add_post_type_support('page', 'excerpt');

		// Setup theme supports
		//
		add_theme_support('post-thumbnails');
		add_theme_support('menus');

		// Optionally disable URL guessing
		//
		if( ! get_theme_mod('sc_enable_url_guessing') ) {

			remove_filter('template_redirect', 'redirect_canonical');

		}

	}

}

//------------------------------------------------------------------------------

add_action('init', 'sc_init_image_sizes');

if( ! function_exists('sc_init_image_sizes') ) {

	/**
	 * Initialize images sizes for the theme.
	 */
	function sc_init_image_sizes() {

		// Add image sizes
		//
		add_image_size('featured', 175, 100,  true);
	
	}

}

//------------------------------------------------------------------------------

add_action('init', 'sc_init_menus');

if( ! function_exists('sc_init_menus') ) {

	/**
	 * Initialize menus for the theme.
	 */
	function sc_init_menus() {

		// Register nav menus
		//
		register_nav_menus(
			array(
				'header_menu'  => 'Header Menu',
				'footer_menu'  => 'Footer Menu'
			)
		);

	}

}

//------------------------------------------------------------------------------

if( ! function_exists('sc_init_modules') ) {

	/**
	 * Initialize modules for the theme.
	 */
	function sc_init_modules() {

		if ( ! class_exists('SC_Modules') ) {

			return false;

		}

		/*-------------------------------------------------------------------------
		 * 
		 * Module options
		 * 
		 * fluid (boolean) 
		 * Fluid modules will be place in a full-screen .container-fluid class 
		 * rather than a fixed .container class. Fluid modules are required to have 
		 * a size of 12.
		 * 
		 * size (int)
		 * The deafult number of columns for the module. Max of 12.
		 * 
		 * sizes (array)
		 * An array of available sizes for the module. The array key will be used as
		 * a label while the value is the number of columns.
		 * 
		 * -----------------------------------------------------------------------*/

		SC_Modules::singleton()->register_module_types(array(

			'Text' => array(
				'fluid' => false,
				'size'  => 12,
				'sizes' => array(
					'1/4 Screen'  => 3,
					'1/3 Screen'  => 4,
					'1/2 Screen'  => 6,
					'Full Screen' => 12
				)
			),

			'Tabs' => array(
				'fluid' => false,
				'size'  => 12,
				'sizes' => array(
					'1/2 Screen'  => 6,
					'Full Screen' => 12
				)
			),

			'Form' => array(
				'fluid' => false,
				'size'  => 12,
				'sizes' => array(
					'1/2 Screen'  => 6,
					'Full Screen' => 12
				)
			),

			'Title' => array(
				'fluid' => false,
				'size'  => 12,
				'sizes' => array(
					'1/2 Screen'  => 6,
					'Full Screen' => 12
				)
			),

			'Callouts' => array(
				'fluid' => false,
				'size'  => 12,
				'sizes' => array(
					'1/2 Screen'  => 6,
					'Full Screen' => 12
				)
			),

			'Anchor' => array(
				'fluid' => false,
				'size'  => 12,
				'sizes' => array(
					'Full Screen' => 12
				)
			),

		));

		SC_Modules::json_builder()->generate_acf_json();
		SC_Modules::json_builder()->add_acf_load_point();

	}

}

if( function_exists( 'sc_init_modules' ) ) {

	sc_init_modules();

}

//------------------------------------------------------------------------------

if( ! function_exists('sc_init_provisioner') ) {

	/**
	 * Initialize the provisioner. The provisioner removes default content, 
	 * configures default settings, and creates default pages and menus.
	 */
	function sc_init_provisioner() {

		if ( ! class_exists('SC_Provisioner') ) {

			return false;

		}

		SC_Provisioner::singleton()->pages = array(
			'home'     => array('title' => 'Home'),
			'about-us' => array('title' => 'About'),
			'contact'  => array('title' => 'Contact')
		);

		SC_Provisioner::singleton()->menus = array(
			'header_menu' => array('title' => 'Header Menu'),
			'footer_menu' => array('title' => 'Footer Menu')
		);

	}

}

if( function_exists( 'sc_init_provisioner' ) ) {

	sc_init_provisioner();

}

//------------------------------------------------------------------------------

add_action('wp_enqueue_scripts', 'sc_load_scripts');

if( ! function_exists('sc_load_scripts') ) {

	/**
	 * Enqueue theme styles and scripts.
	 */
	function sc_load_scripts() {

		$parent = get_template_directory_uri();
		$child  = get_stylesheet_directory_uri();

		wp_enqueue_script( 'modernizr', $parent . '/vendor/modernizr/modernizr-custom.min.js', array( 'jquery' ) );
		wp_enqueue_script( 'detectizr', $parent . '/vendor/detectizr/detectizr.min.js', array( 'jquery' ) );
		wp_enqueue_script( 'scripts',   $child  . '/assets/js/scripts.js', array( 'underscore', 'modernizr', 'detectizr' ) );
			
		wp_enqueue_style( 'bootstrap', $parent . '/vendor/bootstrap/css/bootstrap.min.css' );
		wp_enqueue_style( 'style',     $child  . '/style.css' );

	}

}

//------------------------------------------------------------------------------

add_action('init', 'sc_disable_emojis');

if( ! function_exists('sc_disable_emojis') ) {

	/**
	 * This function will disable emojis and emoticons when the option to enable
	 * them is not selected in the 'Settings' customizer panel. This option is 
	 * turned off by default.
	 */
	function sc_disable_emojis() {

		if( ! get_theme_mod('sc_enable_emojis') ) {

			// Remove all actions related to emojis
			//
			remove_action( 'admin_print_styles', 'print_emoji_styles' );
			remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
			remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
			remove_action( 'wp_print_styles', 'print_emoji_styles' );
			remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
			remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
			remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

			// Filter to remove emoji DNS prefetching
			//
			add_filter( 'emoji_svg_url', '__return_false' );

			// Filter to keep smilies as plain text (disables older style emoticons)
			//
			add_filter( 'option_use_smilies', '__return_false' );

			// Filter to remove from TinyMCE
			//
			add_filter( 'tiny_mce_plugins', function($plugins) {

				if(is_array($plugins)) {

					return array_diff($plugins, array('wpemoji'));

				} 

				return array();

			});

		}

	}

}

//------------------------------------------------------------------------------

add_action('after_setup_theme', 'sc_after_setup_theme');

if( ! function_exists('sc_after_setup_theme') ) {

	/**
	 * Documentation needed.
	 */
	function sc_after_setup_theme() {

		add_theme_support('title-tag');
	
	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_trim_words' ) ) {
  
  /**
   * Truncate text based on a maximum charachter count. The text will be 
   * truncated at the closest word break and not in the middle of a word.
   *
   * @param string [$text] The text to trim.
   * @param int [$num_chars] The maximum charachter count. (Default 200)
   * @param mixed [$more] A string to append or null for nothing. (Default null)
   */
  function sc_trim_words( $text, $num_chars = 200, $more = null ) {
    
    if( $num_chars >= strlen( $text ) ) {

      return $text;

    }

    $parts       = preg_split( '/([\s\n\r]+)/u', $text, null, PREG_SPLIT_DELIM_CAPTURE);
    $parts_count = count( $parts );
    $length      = 0;
    
    for( $last_part = 0; $last_part < $parts_count; ++$last_part ) {
  
      $length += strlen( $parts[$last_part] );
  
      if( $length > $num_chars ) {
  
        break;
  
      }
      
    }
  
    $trimmed_text = implode( array_slice( $parts, 0, $last_part ) );
  
    if( ! is_string( $more ) ) {
  
      $more = '';
  
    }

    return $trimmed_text . $more;
  
  }

}

//------------------------------------------------------------------------------

if( ! function_exists('get_the_excerpt_here') ) {

	/**
	 * Get the excerpt for a given post ID.
	 *
	 * @param  int [$post_id] The post ID to lookup.
	 * @return string A post excerpt.
	 */
	function get_the_excerpt_here($post_id) {

		global $wpdb;

		$query =
			sprintf(
				"SELECT post_excerpt FROM $wpdb->posts WHERE ID = %d LIMIT 1", 
				(int) $post_id
			);

		$result = $wpdb->get_results($query, ARRAY_A);

		return $result[0]['post_excerpt'];

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_get_excerpt_max_length' ) ) {

	/**
	 * Get the excerpt with a limit on its length and ability to
	 * customize the readmore text.
	 *
	 * @param  int [$max_length] The excerpt max length.
	 * @param  string [$read_more] The read more text.
	 */
	function sc_get_excerpt_max_length( $max_length, $read_more = false ) {

		$excerpt = get_the_excerpt();

		$max_length++;

		if( mb_strlen( $excerpt ) > $max_length ) {

			$excerpt = mb_substr($excerpt, 0, $max_length - 5);
			$exwords = explode(' ', $excerpt);
			$excut   = -(mb_strlen($exwords[count($exwords) - 1]));

			if( $excut < 0 ) {

				$excerpt = mb_substr($excerpt, 0, $excut);

			}

			$excerpt = trim($excerpt);

			if( $read_more ) {

				$excerpt .= '<a href="'. get_permalink() .'">'. $read_more .'</a>';

			}

		}

		return $excerpt;

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_excerpt_max_length' ) ) {

	/**
	 * Print the excerpt with a limit on its length and ability to
	 * customize the readmore text.
	 *
	 * @param  int [$max_length] The excerpt max length.
	 * @param  string [$read_more] The read more text.
	 */
	function sc_excerpt_max_length( $max_length, $read_more = false ) {

		echo sc_get_excerpt_max_length( $max_length, $read_more );

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_get_image' ) ) {

	/**
	 * Documentation needed
	 */
	function sc_get_image( $name, $options = array() ) {

		// if name is numeric then we'll assume that it's a post id and
		// and delegate out to the sc_get_featured_image function
		//
		if( is_numeric( $name ) ) {

			$size = 'full';

			if( isset( $options['size'] ) ) {

				$size = 'full';

				unset( $options['size'] );

			}

			return sc_get_featured_image( $name, $size, $options );

		}

		// get our themes default image directory
		//
		$image_directory = get_stylesheet_directory_uri() . '/assets/images/';

		// check if our image is using a relative or absolute url.
		//
		foreach( array('/', 'http://', 'https://') as $needle ) {

			if( stripos($name, $needle) === 0 ) {

				// it looks like our image is using a relative or absolute url
				// so lets not prepend our themes image directory to it.
				//
				$image_directory = '';

			}

		}

		// build the full path to our image
		//
		$image = $image_directory . $name;

		// if $options === false then only return the path
		//
		if( false === $options ) {

			return $image;

		}

		// if $options is a string then convert $options into an array conatining
		// the old string as a value. The old string will become alt text.
		//
		if( is_string( $options ) ) {

			$options = array('alt' => $options);

		}

		if( is_array( $options ) ) {

			$options['src'] = $image; // add image src to our options array

			$attributes = array(); // create an attributes array

			// loop over our options array and for each one add
			// a marked up attribute to the $attributes array.
			//
			foreach( $options as $attribute => $value ) {

				$attributes[] = $attribute . '="' . $value . '"';

			}

			// return our image markup
			//
			return '<img ' . implode( ' ', $attributes ) . ' />';

		}

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_image' ) ) {

	/**
	 * Documentation needed
	 */
	function sc_image( $name, $options = array() ) {

		echo sc_get_image($name, $options);

	}
	
}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_get_featured_image' ) ) {

	/**
	 * Get the featured image for a given post and return its image tag. Works
	 * very similar to sc_image except that you are able to pass a post id.
	 *
	 * @param int [$post_id] The post id.
	 * @param string [$size] The image size name to use.
	 * @param array [$options] An options array
	 * @return string An image element.
	 */
	function sc_get_featured_image( 
		$post_id, 
		$size = 'full', 
		$options = array() ) {

		$thumbnail_id = get_post_thumbnail_id($post_id);

		if( ! empty($thumbnail_id) && false !== $thumbnail_id ) {

			$image = wp_get_attachment_image_src( $thumbnail_id, $size );

			return sc_get_image( $image[0], $options );

		}

		return '';

	}

}

//------------------------------------------------------------------------------

if( ! function_exists('sc_featured_image') ) {

	/**
	 * Identical functionality to sc_get_featured_image except that it echos out
	 * to the browser rather than return a value.
	 *
	 * @param int [$post_id] The post id.
	 * @param string [$size] The image size name to use.
	 * @param array [$options] An options array
	 */
	function sc_featured_image( $post_id, $size = 'full', $options = array() ) {

		echo sc_get_featured_image( $post_id, $size, $options );

	}

}

//------------------------------------------------------------------------------

if( ! function_exists('sc_dynamic_year') ) {

	/**
	 * Find occurances of a the most recent past year within a string and replace 
	 * them with the current year. The search will not go backwards past 2015.
	 * 
	 * Example assuming the current year is 2017: 
	 * The string "Copyright 2015-2016" would update to "Copyright 2015-2017"
	 */
	function sc_dynamic_year( $text ) {

		$current_year = (int) date( 'Y', time() );
		$past_year    = $current_year;
		$min_year     = 2015;

		while( $min_year <= $past_year ) {

			$past_year = (string) $past_year;

			if( strpos( $text, $past_year ) ) {

				return str_replace( $past_year , $current_year, $text );

			}

			$past_year = (int) $past_year;
			$past_year--;

		}

		return $text;

	}

}

//------------------------------------------------------------------------------

add_filter( 'tiny_mce_before_init', 'sc_customize_tiny_mce' );

if( ! function_exists( 'sc_customize_tiny_mce' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_customize_tiny_mce($arr) {

		$available_block_formats = array(
			'Paragraph'    => 'p',
			'Heading 2'    => 'h2',
			'Heading 3'    => 'h3',
			'Heading 4'    => 'h4',
			'Heading 5'    => 'h5',
			'Heading 6'    => 'h6',
			'Preformatted' => 'pre'
		);

		$block_formats = '';

		foreach ($available_block_formats as $label => $value) {
			
			$block_formats .= "$label=$value;";

		}

		$arr['block_formats'] = $block_formats;

		return $arr;

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_get_archive_title' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_get_archive_title() {

		if ( is_category() ) { 

			return single_cat_title( '', false );

		}

		if ( is_tax() ) { 

			$term_slug     = get_query_var( 'term' );
			$taxonomy_slug = get_query_var( 'taxonomy' );
			$term          = get_term_by( 'slug', $term_slug, $taxonomy_slug );

			if( false !== $term ) {

				return $term->name;

			}
			 
		}

		if ( is_day() ) { 

			return 'Archive for ' . get_the_time( 'F jS, Y' );
		}

		if ( is_month() ) { 

			return 'Archive for ' . get_the_time( 'F Y' );
		}

		if ( is_year() ) { 

			return 'Archive for ' . get_the_time( 'Y' ); 
		}

		if ( is_search() ) { 

			return 'Search Results';
		}

		if ( is_author() ) { 

			return 'Author Archive'; 
		}

		if ( isset( $_GET['paged'] ) && ! empty( $_GET['paged'] ) ) { 

			return 'Blog Archives'; 
		}
		
		return post_type_archive_title( '', false );

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_page_banner_open' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_page_banner_open() {

		do_action( 'sc_before_page_banner' );

			echo '
				<div id="page-banner">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">';

		do_action( 'sc_after_page_banner_open' );

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_page_banner_close' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_page_banner_close() {

		do_action( 'sc_before_page_banner_close' );

			echo '
							</div>
						</div>
					</div>
				</div>';

		do_action('sc_after_page_banner');

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_page_banner' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_page_banner( $title = null ) {

		sc_render_page_banner_open();

		echo '<h1 class="title">';

		if ( is_null( $title ) ) {

			the_title();

		} else {

			echo $title;

		}

		echo '</h1>';

		sc_render_page_banner_close();
		
	}
	
}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_page_content_open' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_page_content_open() {

		do_action( 'sc_before_page' );

			echo '<div id="page-content">';

				do_action( 'sc_after_page_open' );

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_page_content_close' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_page_content_close() {

				do_action( 'sc_before_page_close' );

			echo '</div>';

		do_action( 'sc_after_page' );

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_non_modular_page_content_open' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_non_modular_page_content_open() {

		do_action( 'sc_before_page' );

			echo '<div id="page-content">
							<div class="container sc-non-modular-container">
								<div class="row">
									<div class="col-xs-12">';

		do_action( 'sc_after_page_open' );

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_non_modular_page_content_close' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_non_modular_page_content_close() {

		do_action( 'sc_before_page_close' );

			echo '
							</div>
						</div>
					</div>
				</div>';

		do_action( 'sc_after_page' );

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_page_content' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_page_content( $content = null ) {

		if ( is_null( $content ) ) {

			$modules = false;

			if ( class_exists('SC_Modules') ) {

				if( is_array( SC_Modules::singleton()->get_modules() ) ) {

					$modules = true;

				}

			}

			if( $modules ) {

				sc_render_page_content_open();

				SC_Modules::singleton()->display_modules();

				sc_render_page_content_close();

			} else {

				sc_render_non_modular_page_content_open();

				the_content();
				
				sc_render_non_modular_page_content_close();

			}

		} else {

			sc_render_non_modular_page_content_open();

			echo $content;

			sc_render_non_modular_page_content_close();

		}
		
	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_page' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_page( $title = null, $content = null ) {

		sc_render_page_banner($title);
		sc_render_page_content($content);
		
	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_render_pagination' ) ) {

	/**
	 * Documentation needed.
	 */
	function sc_render_pagination() {

		echo '<div class="post-navigation">';

		posts_nav_link( ' ', '&laquo; Previous', 'Next &raquo;' );
				 
		echo '</div>';
		
	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_mce_buttons' ) ) {

	function sc_mce_buttons( $buttons ) {

		array_push( $buttons, 'separator', 'table' );

		return $buttons;

	}

}

add_filter( 'mce_buttons', 'sc_mce_buttons' );

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_mce_external_plugins' ) ) {

	function sc_mce_external_plugins( $plugins ) {

	  $theme  = get_template_directory_uri();
	  $vendor = $theme . '/vendor';

	  $plugins['table'] = $vendor . '/tinymceplugins/table/plugin.min.js';

	  return $plugins;

	}

}

add_filter( 'mce_external_plugins', 'sc_mce_external_plugins' );

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_tiny_mce_before_init' ) ) {

	function sc_tiny_mce_before_init( $settings ) {

	  $styles = array(
	      array(
	          'title' => 'None',
	          'value' => ''
	      ),
	      array(
	          'title' => 'Responsive Table',
	          'value' => 'responsive-table',
	      ),
	  );

	  $settings['table_class_list'] = json_encode( $styles );

	  return $settings;

	}

}
 
add_filter( 'tiny_mce_before_init', 'sc_tiny_mce_before_init' );

//------------------------------------------------------------------------------

function sc_disable_attachment_pages() {

  global $post;

  if( is_attachment() ) {

    global $wp_query;

    $wp_query->set_404();

    status_header( 404 );

  }

}

add_action( 'wp', 'sc_disable_attachment_pages' );

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_debug' ) ) {

	/**
	 * This function is a simple wrapper for print_r so that you don't have to
	 * worry about printing out <pre> tags. In addition it will vary it's output
	 * based on the number and type of arguments passed into it.
	 * 
	 * - If a single argument is passed it will be run through print_r.
	 * - If two or more arguments are passed and the first argument is a string
	 *   it will be used as a label. All remaining arguments will be run though 
	 *   print_r.
	 * - If two or more arguments are passed and the first argument is not a 
	 *   string then all arguments will be run through print_r.
	 */
	function sc_debug() {

		$arguments      = func_get_args();
		$argument_count = func_num_args();

		echo "<pre>"; 

		if( $argument_count === 1 ) {

			print_r( $arguments[0] );

		} else if( $argument_count > 1 ) {

			$start = 0;

			if( is_string( $arguments[0] ) ) {

				echo $arguments[0] . "\n";

				$start = 1;

			}

			for( $c = $start; $c < $argument_count; $c++ ) {

				print_r( $arguments[$c] );

			}

		}

		echo "</pre>";

	}

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_debug_post' ) ) {

  function sc_debug_post( $post_id, $load_meta = true ) {
    
    $post = get_post( $post_id );
    
    if( is_null( $post ) ) {
      
      sc_debug( $post );
      return null;
      
    }
    
    if( $load_meta ) {
      
      $meta = get_post_meta( $post_id );
      
      $post->meta = $meta;
      
    }
    
    sc_debug( $post );
    
  }

}

//------------------------------------------------------------------------------

if( ! function_exists( 'sc_debug_user' ) ) {

  function sc_debug_user( $user_id, $load_meta = true ) {
    
    $user = get_user_by( 'ID', (int) $user_id );
    
    if( false === $user ) {
      
      sc_debug( $user );
      return null;
      
    }
    
    if( $load_meta ) {
      
      $meta = get_user_meta( $user_id );
      
      $user->meta = $meta;
      
    }
    
    sc_debug( $user );
    
  }

}
