<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */
get_header();

if ( have_posts() ) {

	if( is_front_page() ) {

		$title = get_bloginfo('name', 'display');

	} else {

		$title = single_post_title('', false);

	}    

	sc_render_page_banner($title);
	sc_render_non_modular_page_content_open();

	while ( have_posts() ) {

		the_post();

		get_template_part( 'content', get_post_type() );

	}

	sc_render_pagination();
	sc_render_non_modular_page_content_close();

} else {

	$title   = 'No Posts Found';
	$content = '<p>Sorry, there are currently no posts available. Thank You.</p>';

	sc_render_page( $title, $content );

}

get_footer();