<?php if (! defined('ABSPATH')) die('No direct access allowed');
/* DO NOT MODIFY THIS FILE
 * -----------------------
 * If you need to make changes to this file create a copy of it in
 * your child theme and perform any updates there.
 */
get_header();

sc_render_page_banner( 'Search Results' );

if ( have_posts() ) {

	sc_render_non_modular_page_content_open();
	
	while ( have_posts() ) {

		the_post();

		get_template_part( 'content', get_post_type() );

	}

	sc_render_pagination();
	sc_render_non_modular_page_content_close();

} else {

	sc_render_page_content( 
		'<p>Sorry, no posts matched your criteria.</p>'
	);

}

get_footer();