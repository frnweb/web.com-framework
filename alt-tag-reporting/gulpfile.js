
var _           = require('lodash');
var gulp        = require('gulp');
var prefixer    = require('gulp-autoprefixer');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var rename      = require('gulp-rename');
var uglify      = require('gulp-uglify');
var gutil       = require('gulp-util');
var plumber     = require('gulp-plumber');
var postcss     = require('gulp-postcss');
var scssLint    = require('gulp-scss-lint');
var watch       = require('gulp-watch');
var phpcs       = require('gulp-phpcs');
var mergeRules  = require('postcss-merge-rules');
var mqPacker    = require('css-mqpacker');
var runSequence = require('run-sequence');
var fs          = require('fs');
var config      = JSON.parse(fs.readFileSync('./config/config.json'));
var args        = require('args');

args.option('dev', 'Enable dev mode. CSS and JavaScript will not be compressed.');

const flags = args.parse(process.argv);

function errorHandler(error) {

    gutil.log(gutil.colors.red(error.message));
    this.emit('end');

}

function lintReporter(file) {

    if (! file.scsslint.success) {

        gutil.log(file.scsslint.issues.length + ' issues found in ' + file.path);

    }
    
}

function parseConfig() {

    _.each(config.themes, function(theme) {

        theme.sass    = parseThemeSlug(theme, theme.sass);
        theme.scripts = parseThemeScripts(theme);

    });

}

function parseThemeScripts(theme) {

  return _.reduce(theme.scripts, function(object, item) {
    
    var name = _.isString(item) ? 'scripts.js' : item.name;

    object[name] = object[name] || [];
    
    if(_.isString(item)) {

        object[name].push(parseThemeSlug(theme, item));

    } else {

        object[name] = _.map(item.source, function(script) { 
            return parseThemeSlug(theme, script);
        });

    }
    
    return object;
    
  }, {});

}

function parseThemeSlug(theme, value) {

    return value.split('{{slug}}').join(theme.slug);

}

gulp.task('lintSass', function() {

    // TODO: Add lint

});

gulp.task('compileSass', function() {

    var processors = [
        mergeRules,
        mqPacker
    ];

    var outputStyle = flags.dev ? 'expanded' : 'compressed';
    var dest, stream;

    _.each(config.themes, function(theme) {

        dest   = './wordpress/wp-content/themes/' + theme.slug;
        stream = gulp
            .src(theme.sass)
            .pipe(plumber(errorHandler))
            .pipe(sass({
                    includePaths:['./source'],
                    omitSourceMapUrl:true, 
                    outputStyle:outputStyle
                }))
            .pipe(prefixer({browsers:['last 3 versions', 'ie >= 9']}))
            .pipe(postcss(processors))
            .pipe(rename('style.css'))
            .pipe(gulp.dest(dest));

    });

    return stream;

});

gulp.task('lintPHP', function() {

    var dir, dest, source, stream;

    _.each(config.themes, function(theme) {

        dir  = './lint/' + theme.slug;
        dest = dir + '/php.lint.txt';

        if( ! fs.existsSync(dir) ) {

            fs.mkdirSync(dir);

        }

        source = ['./wordpress/wp-content/themes/' + theme.slug + '/**/*.php'];

        if( ! _.isUndefined( theme.lint.php.source ) ) {

            source = theme.lint.php.source.map(function(value) {
    
                return parseThemeSlug(theme, value);
    
            });

        }

        stream = gulp.src(source)
            .pipe(phpcs({
                bin:'/xampp/wpcs/vendor/bin/phpcs',
                standard:'WordPress',
                warningSeverity:0
            }))
            .pipe(phpcs.reporter('file', {
                path:dest
            }));

    });

    return stream;

});

gulp.task('lintScripts', function() {

    // TODO: Add lint

});

gulp.task('compileScripts', function() {

    var dest, stream;

    _.each(config.themes, function(theme) {

        dest = './wordpress/wp-content/themes/' + theme.slug + '/assets/js';

        _.each(theme.scripts, function(source, name) {

            if(source.length) {

                if(flags.dev) {

                    stream = gulp.src(source)
                        .pipe(plumber(errorHandler))
                        .pipe(concat(name))
                        .pipe(gulp.dest(dest));

                } else {

                    stream = gulp.src(source)
                        .pipe(plumber(errorHandler))
                        .pipe(concat(name))
                        .pipe(uglify())
                        .pipe(gulp.dest(dest));

                }

            }

        });

    });

    return stream;

});

gulp.task('watchSass', function() {

    gulp.watch('./source/**/*.scss', function() {

        runSequence('compileSass');
    });

});

gulp.task('watchScripts', function() {

    gulp.watch('./source/**/*.js', function() {

        runSequence('compileScripts');
    });

});

gulp.task('default', function() {

    parseConfig();

    runSequence('watchSass', 'watchScripts');

});
