
/* Header Search
------------------------------------------------------------------------------*/

(function($) {

	var openedAt;

	$(document).ready(function() {

		$('#header .search-link').click(function(e) {
			
			e.preventDefault();
			
			openSearchForm();
			
		});
		
	});

	$(window).resize( _.throttle( onResize, 500 ) );

	function onResize() {

		if( new Date().getMilliseconds() - openedAt > 250 ) {

			closeSearchForm();

		} 

	}

	function openSearchForm() {

		openedAt = new Date().getMilliseconds();

		if( ! $('#header-search-form').length ) {

			$('#header .header-col-right').append( $('#header-search-template').text() );

		}

		$('#header-search-form .header-search-input').focus();

		$('#header-search-form .header-search-input, #header-search-form .header-search-button').blur(function() {

			setTimeout(function() {

				var close = true;

				if( $('#header-search-form .header-search-input').is(':focus') ) {

					close = false;

				}

				if( $('#header-search-form .header-search-button').is(':focus') ) {

					close = false;

				}

				if( close ) {

					closeSearchForm();

				}

			}, 750);

		});

	}

	function closeSearchForm() {

		$('#header-search-form').remove();

	}

})(jQuery);


/* Header Navigation
------------------------------------------------------------------------------*/

(function($) {

	$(document).ready(function() {

		// Remove column links
		$('#site-navigation a[data-depth="1"]').remove();

		// Add mega menu classes
		$('#site-navigation .menu > .menu-item-has-children').each(function(i, menuItem) {

			if($(menuItem).find('ul').length) {

				$(menuItem).addClass('has-mega-menu');

				$(menuItem).find('> ul').addClass('mega-menu');

			}

		});

		// Add mega menu column classes
		$('#site-navigation .mega-menu').each(function(i, megaMenu) {

			var megaMenu    = $(megaMenu);
			var columnCount = megaMenu.find('> li').length;
			var columnWidth = 25;

			switch(columnCount) {
				case 1:
					columnWidth = 100; 
					break;
				case 2:
					columnWidth = 50; 
					break;
				case 3:
					columnWidth = 33; 
					break;
				default:
					columnWidth = 25; 
					break;
			}

			megaMenu.find('> li').addClass('menu-item-width-' + columnWidth);

		});

		// Init menu
		$('#site-navigation .menu').smartmenus({
			hideDuration:0,
			showDuration:0,
			showTimeout:100,
			subIndicators:false
		});

	});

})(jQuery);


/* Mobile Navigation
------------------------------------------------------------------------------*/

(function($) {

	$(document).ready(function() {

	  $('#mobile-navigation .desktop-menu-mobile').find($('a[data-depth="1"]')).each(function() {

			$(this).siblings('ul.sub-menu').children().unwrap();
			$(this).siblings('ul.sub-menu').unwrap();
			$(this).unwrap();
			$(this).remove();	
			
		}); 

		var menuToCopyId = '#mobile-navigation';
		var mobileMenuId = '#mobile-navigation';
		var menuOptions  = {

			onOpen: function() {
				$('#wrapper').addClass('mobile-navigation-is-open');
			},

			onClose: function() {
				$('#wrapper').removeClass('mobile-navigation-is-open');
			},

			slideMarkup:       '<div>' + getStickyLinks() + '{{ slide }}</div>',
			backButtonMarkup:  '<a class="sc-mobile-menu-back-button arrow-link arrow-link-left">Go Back</a>',
			closeButtonMarkup: '<a class="sc-mobile-menu-close-button">Close</a>',

			containerMarkup: getContainerMarkup()

		};

		var mobileMenu = sc.mobileMenu.init(
			menuToCopyId,
			mobileMenuId,
			menuOptions
		);

		$('#mobile-menu-button').click(function(event) {

			mobileMenu.toggle();

		});

	});
	
	function getStickyLinks() {

		return '<ul class="sticky-links">' + $('#header .small-navigation .menu').html() + '</ul>';
		
	}

	function getContainerMarkup() {

		var logo        = '';
		var socialIcons = '';
		var markup      = [
			'<div class="inner">',
			'  <div class="container">',
			'    <div class="row">',
			'      <div class="col-xs-12">',
			'        {{ slick }}',
			'        <div class="footer">',
			'          <div class="logo">' + logo + '</div>',
			'          <div class="social-icons">' + socialIcons + '</div>',
			'        </div>',
			'      </div>',
			'    </div>',
			'  </div>',
			'</div>'
		];

		return markup.join('');

	} 

})(jQuery);


/* Interior Banner
------------------------------------------------------------------------------*/

(function($) {

	var banner;

	$(document).ready(function() {

		banner = $('#page-banner');

		update();

		$(window).resize( _.throttle( onResize, 250 ) );

	});

	$(window).load(function() {

		update();

	});

	function onResize() {

		update();

	}

	function update() {

		var image  = getMobileFirstProperty( 'image'  );
		var valign = getMobileFirstProperty( 'valign' );
		var halign = getMobileFirstProperty( 'halign' );

		if( image != '' ) {

			banner
				.css( 'background-image', 'url(' + image + ')' )
				.css( 'background-position', halign + ' ' + valign );

		}

	}

	function getMobileFirstProperty( property ) {

		var sizes = [ 'xs', 'sm', 'md', 'lg', 'xl', 'hg' ];
		var value = banner.data( property + '-xs-up' );

		_.each( sizes, function(size) {

			if( sc.mediaQueries.match( size + '_up') ) {

				value = banner.data( property + '-' + size + '-up' );

			}

		});

		return value;

	}

})(jQuery);

/* Sidebar Menu
------------------------------------------------------------------------------*/

(function($) {

	$(document).ready(function() {

		$('#page-sidebar .menu-item[data-level="2"].current-menu-item > a').on('click', function(e){
			
			e.preventDefault();

		});

	});

})(jQuery);
